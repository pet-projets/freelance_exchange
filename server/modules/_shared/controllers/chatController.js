const { File, User, Chat, Message } = require("../db")

function hasAccess(userId, chat) {
  return !(!chat.users.includes(userId) && userId !== process.env.ADMIN_ID)
}

class ChatController {

    async createChat(req, res, next) {
      const {userId} = req.body
      const userModel = await User.findOne({_id: userId})
      if (!userModel) return res.status(404).json({message: 'User does not exist'})
      if (userId === req.user.id) return res.status(403).json({message: 'You can not create this chat'})
      const ids = [userId, req.user.id]
      const existedChat = await Chat.findOne({users: {$all: ids}, isFreelance: false})
      if (existedChat) return res.status(403).json({message: 'Chat has already exits'})
      const chat = new Chat({users: [req.user.id, userId]})
      await chat.save()
      return res.status(200).json({chat})
    }

    async sendMessage(req, res, next) {
        const {text, files, chatId, related} = req.body
        const chat = await Chat.findOne({_id: chatId})
        if (!chat) return res.status(404).json({message: 'Chat does not exits'})
        if (!hasAccess(req.user.id, chat)) {
            return res.status(403).json({message: 'You have no access to this chat'})
        }
        const message = new Message({chatId, senderId: req.user.id, text, files, timestamp: Date.now(), relatedMessage: related})
        await message.save()
        const filesModel = await File.find({_id: {$in: files}})
        return res.status(200).json({message: {...message.toObject(), files: filesModel}})
    }

    async getChat(req, res, next) {
        const {chatId} = req.query
        const chat = await Chat.findOne({_id: chatId})
        if (!chat) return res.status(404).json({message: 'Chat does not exits'})
        await Message.updateMany({chatId, isRead: false, senderId: {$ne: req.user.id}}, {isRead: true})
        if (!hasAccess(req.user.id, chat)) {
          return res.status(403).json({message: 'You have no access to this chat'})
        }
        const messages = await Message.find({chatId})
        const filesIds = messages.reduce((a, m) => [...a, ...m.files], [])
        const files = await File.find({_id: {$in: filesIds}})
        const messagesWithFiles = messages.map(message => {
          const messageFiles = files.filter(file => message.files.includes(file._id))
          return { ...message.toObject(), files: messageFiles }
        })
        return res.status(200).json({messages: messagesWithFiles})
    }

    async getChatByUser(req, res, next) {
      const {userId} = req.query
      const ids = [userId, req.user.id]
      const chat = await Chat.findOne({users: {$all: ids}, isFreelance: false})
      return res.status(200).json({id: chat?.id})
    }

    async getChats(req, res, next) {
      const chats = await Chat.aggregate([
          {
            $match: { users: req.user.id }
          },
          {
            $lookup: {
              from: 'messages',
              localField: '_id',
              foreignField: 'chatId',
              as: 'messages'
            }
          },
          {
            $addFields: {
              latestMessage: {
                  $cond: {
                    if: { $gt: [{ $size: '$messages' }, 0] },
                    then: { $arrayElemAt: ['$messages', -1] },
                    else: null
                  }
              },
              unreadCount: {
                  $size: {
                    $filter: {
                      input: '$messages',
                      as: 'message',
                      cond: { $eq: ['$$message.isRead', false] }
                    }
                  }
              }
            }
          },
          {
            $project: {
              messages: 0
            }
          }
      ])
      const chatersIds = chats.map((chat) => {
          return chat.users[0] === req.user.id ? chat.users[1] : chat.users[0]
      })
      const chaters = await User.find({_id: { $in: chatersIds }})
      const filesIds = chaters.map(chater => chater.avatar)
      const files = await File.find({_id: {$in: filesIds} })
      const chatsWithUsers = chats.map( (chat) => {
          const chater = chaters.find(chater => chater._id.toString() === chat.users[0] || chater._id.toString() === chat.users[1])
          const file = files.find(file => file._id.toString() === chater?.avatar)
          return Object.assign({}, chat, {
              chater: { id: chater?._id, username: chater?.username, avatar: file || null }
          })
      })
      return res.status(200).json({chats: chatsWithUsers})
    }

    // async getSupportChat(req, res, next) {
    //   const chat = await Chat.findOne({supportUserId: req.user.id})
    //   await Message.updateMany({chatId: chat.id, isRead: false, senderId: {$ne: req.user.id}}, {isRead: true})
    //   if (req.user.id !== process.env.ADMIN_ID && req.user.id !== chat.supportUserId) return res.status(403).json({message: 'You have no access to this chat'})
    //   const messages = await Message.find({chatId: chat.id})
    //   const filesIds = messages.reduce((a, m) => [...a, ...m.files], [])
    //   const files = await File.find({_id: {$in: filesIds}})
    //   const messagesWithFiles = messages.map(message => {
    //     const messageFiles = files.filter(file => message.files.includes(file._id))
    //     return { ...message.toObject(), files: messageFiles }
    //   })
    //   return res.status(200).json({messages: messagesWithFiles})
    // }

    async getSupportChat(req, res, next) {
      const {chatId} = req.query
      const chat = await Chat.findOne({_id: chatId})
      const user = await User.findOne({_id: chat.users[0]})
      const supporter = await User.findOne({_id: chat.users[1]})
      return res.status(200).json({chat: { ...chat, supporter, user }})
    }

    async getSupportChats(req, res, next) {
      const chats = await Chat.aggregate([
        {
          $match: { isSupport: true }
        },
        {
          $lookup: {
            from: 'messages',
            localField: '_id',
            foreignField: 'chatId',
            as: 'messages'
          }
        },
        {
          $addFields: {
            latestMessage: {
                $cond: {
                  if: { $gt: [{ $size: '$messages' }, 0] },
                  then: { $arrayElemAt: ['$messages', -1] },
                  else: null
                }
            },
            unreadCount: {
                $size: {
                  $filter: {
                    input: '$messages',
                    as: 'message',
                    cond: { $eq: ['$$message.isRead', false] }
                  }
                }
            }
          }
        },
        {
          $project: {
            messages: 0
          }
        }
      ])
      const chatersIds = chats.map((chat) => {
          return chat.users[0]
      })
      const chaters = await User.find({_id: {$in: chatersIds} })
      const chatsWithOffers = chats.map( (chat) => {
          const chater = chaters.find(chater => chater._id.toString() === chat.users[0])
          return Object.assign({}, chat, {
              chater: { id: chater._id, username: chater.username }
          })
      })
      return res.status(200).json({chats: chatsWithOffers})
    }

    async setRead(req, res, next) {
      const {chatId} = req.body
      await Message.updateMany({chatId, isRead: false}, {isRead: true})
      return res.status(200).json({message: 'Read'})
    }

}

module.exports = new ChatController()