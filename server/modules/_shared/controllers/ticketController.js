const { Ticket, User } = require("../db")


class TicketController {

    async getTickets(req, res, next) {
        const tickets = await Ticket.find()
        const usersIds = tickets.map(ticket => ticket.receiverId)
        const users = await User.find({ _id: { $in: usersIds } })
        const ticketsWithWallet = tickets.map(ticket => {
            const wallet = users.find(user => user._id.toString() === ticket.receiverId).wallet
            return { ...ticket.toObject(), wallet }
        })
        return res.status(200).json({tickets: ticketsWithWallet})
    }

    async removeTickets(req, res, next) {
        const {ticketsIds} = req.body
        await Ticket.deleteMany({_id: {$in: ticketsIds}})
        return res.status(200).json({message: 'Tickets was deleted'})
    }
}

module.exports = new TicketController()