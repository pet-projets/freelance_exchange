const {User, Message, Chat, File} = require('../db')
const emailStore = require('../../../stores/emailStore')
const { generateJwt } = require('../../../utils/functions')
const { generateNumber } = require('../../../utils/functions')
const { getAuthEmail } = require('../../../utils/emailHtml')
const { Category, Rating } = require('../../freelance/db')
const fileStore = require('../../../stores/fileStore')

async function createSupportChat(id) {
    const chat = new Chat({isSupport: true, users: [id, process.env.ADMIN_ID]})
    await chat.save()
    const text = 'Welcome, we will be happy to help you!'
    const message = new Message({chatId: chat.id, senderId: process.env.ADMIN_ID, text, timestamp: Date.now()})
    await message.save()
}

function checkUsername(username) {
    let isLegit = true
    let regExsp =  /^[A-Za-z]([A-Za-z0-9_.-]+)$/g
    isLegit =  username.match(regExsp) ? isLegit : false
    isLegit = username.length > 50 || username.length < 4 ? false : isLegit
    return isLegit
}
function checkPassword(password) {
    return !(password.length < 8 || password.length > 20)
}

class UserController {

    async registration(req, res, next) {
        const {username, password, email, code, role} = req.body
        if (!checkUsername(username)) {
            return res.status(400).json({message: `A username can contain only letters of the Latin alphabet, numbers. A username must be from 4 to 50 characters long`})
        }
        if (!checkPassword(password)) {
            return res.status(400).json({message: 'Password must contain from 8 to 20 characters'})
        }
        let candidate = await User.findOne({username}) 
        if (!!candidate) {
            return res.status(400).json({message: `Username '${username}' has already taken`})
        }
        candidate = await User.findOne({email})
        if (!!candidate || emailStore.getCode(email) !== code) {
            return res.status(400).json({message: `Invalid email or code`})
        }
        emailStore.removeData(email)
        const user = new User({username, password, email, role})
        await user.save()
        await createSupportChat(user.id)
        const token = generateJwt(user)
        return res.json({token})
    }

    async login(req, res, next) {
        const {username, password, email, code} = req.body
        if (!!code) {
            const user = await User.findOne({email})
            if (!user) {
                return res.status(400).json({message: `User does not exist`})
            }
            if (emailStore.getCode(email) !== code) {
                return res.status(400).json({message: `Invalid email or code`})
            }
            emailStore.removeData(email)
            const token = generateJwt(user)
            return res.status(200).json({token})
        }
        const user = await User.findOne({username})
        if (!user) {
            return res.status(400).json({message: `User ${username} does not exist`})
        }
        if (password !== user.password) {
            return res.status(400).json({message: `Incorrect password`})
        }
        const token = generateJwt(user)
        return res.status(200).json({token})
    }

    async check(req, res, next) {
        const user = await User.findOne({_id: req.user.id})
        const token = generateJwt(user)
        return res.json({token})
    }

    async sendEmail(req, res, next) {
        const {email} = req.body
        const emailCode = generateNumber(8)
        const isSent = await emailStore.sendMessage(email, 'Melius Effect confirmation code', getAuthEmail(emailCode))
        if (!isSent) {
            return res.status(400).json({message: 'Invalid email'})
        }
        emailStore.pushData(email, emailCode)
        return res.status(200).json({message: 'Email sent successfully'})
    }

    async editPassword(req, res, next) {
        const {password} = req.body
        const user = req.user

        const userModel = await User.findOne({_id: user.id})

        if (password.length < 8 && password.length >= 20) {
            return res.status(400).json({message: 'Password must contain 8-20 symbols'})
        }
        await userModel.updateOne({password})

        const token = generateJwt(userModel)
        return res.status(200).json({token})
    }

    async connectWallet(req, res, next) {
        const {wallet} = req.body
        await User.updateOne({_id: req.user.id}, {wallet})
        return res.status(200).json({wallet})
    }

    async setNotification(req, res, next) {
        const {isNotificate} = req.body
        const user = await User.findOneAndUpdate({_id: req.user.id}, {notification: isNotificate})
        const token = generateJwt(user)
        return res.status(200).json({token})
    }

    async createDescription(req, res, next) {
        const {description} = req.body
        await User.updateOne({_id: req.user.id}, {description})
        return res.status(200).json({description})
    }

    async setAvatar(req, res, next) {
        const {avatarId} = req.body
        const user = await User.findOne({_id: req.user.id})
        if (!!user.avatar) {
            const prevAvatar = await File.findOne({_id: user.avatar})
            await fileStore.delete(prevAvatar.pathId)
            await prevAvatar.deleteOne()
        }
        await user.updateOne({avatar: avatarId})
        return res.status(200).json({message: 'Avatar was set'})
    }

    async removeAvatar(req, res, next) {
        const user = await User.findOne({_id: req.user.id})
        if (!!user.avatar) {
            const prevAvatar = await File.findOne({_id: user.avatar})
            await fileStore.delete(prevAvatar.pathId)
            await prevAvatar.deleteOne()
            await user.updateOne({avatar: null})
        }
        return res.status(200).json({message: 'Avatar was removed'})
    }

    async getInfo(req, res, next) {
        const {userId} = req.query
        const userModel = await User.findOne({_id: userId})
        if (!userModel) return res.status(404).json({message: 'User does not exist'})
        const avatar = await File.findOne({_id: userModel.avatar})
        const categories = await Category.find({_id: {$in: userModel.categories}})
        const ratings = await Rating.find({userId})
        const info = {
            id: userModel.id,
            username: userModel.username,
            description: userModel.description,
            avatar,
            categories: categories,
            ratings,
            cv: userModel.cv
        }
        return res.status(200).json({info})
    }

    async searchUser(req, res, next) {
        const {name} = req.query
        if (!name.length) return res.status(200).json({users: []})
        const regex = new RegExp(name, 'i')
        const userModels = await User.find({username: regex})
        const infos = userModels
        .filter((um) => um.id !== process.env.ADMIN_ID)
        .map((um) => {
            return {
                id: um.id,
                username: um.username
            }
        })
        return res.status(200).json({users: infos})
    }

}

module.exports = new UserController()