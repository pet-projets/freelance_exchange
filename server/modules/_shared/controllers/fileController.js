const { File } = require("../db")
const fileStore = require('../../../stores/fileStore')

const LINK_EXPIRATION = 14000

class FileController {

    async uploadFile(req, res, next) {
        const {file} = req.files
        const {path} = req.query
        const type = file.name.split('.').slice(-1)[0]
        const pathId = await fileStore.uploadFile(file.data, path, type)
        if (!pathId) return res.status(403).json({message: `File was not uploaded`})
        const name = Buffer.from(file.name, 'binary').toString('utf-8')
        const fileModel = new File({ name, pathId, mimetype: file.mimetype.split('/')[0], size: file.size, path })
        await fileModel.save()
        return res.status(200).json({ file: fileModel.id }) 
    }

    async createFolder(req, res, next) {
        const {path} = req.body
        await fileStore.createFolder(path)
        return res.status(200).json({ message: 'Folder was created' })
    }

    async downloadFile(req, res, next) {
        const {fileId} = req.query
        let file = await File.findOne({_id: fileId})
        if (file.linkExp <= Date.now()) {
            const link = await fileStore.generateLink(file.pathId)
            await file.updateOne({link, linkExp: Date.now() + LINK_EXPIRATION * 1000})
            file = await File.findOne({_id: fileId})
        }
        return res.status(200).json({ link: file.link, linkExp: file.link })
    }

    async getFilesMetadata(req, res, next) {
        const {ids} = req.query
        const files = await File.find({_id: {$in: ids}}).select('-pathId')
        return res.status(200).json({ files })
    }

}

module.exports = new FileController()