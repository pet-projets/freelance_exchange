const Router = require('express')
const router = new Router()
const authMiddleware = require('../../../middlewares/authMiddleware')
const userController = require('../controllers/userController')

router.post('/registration', userController.registration)
router.post('/login', userController.login)
router.put('/email', userController.sendEmail)
router.get('/auth', authMiddleware, userController.check)
router.put('/password', authMiddleware, userController.editPassword)
router.put('/wallet', authMiddleware, userController.connectWallet)
router.put('/notification', authMiddleware, userController.setNotification)
router.post('/description', authMiddleware, userController.createDescription)
router.post('/avatar', authMiddleware, userController.setAvatar)
router.delete('/avatar', authMiddleware, userController.removeAvatar)
router.get('/', userController.getInfo)
router.get('/search', userController.searchUser)

module.exports = router