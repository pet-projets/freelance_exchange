const Router = require('express')
const router = new Router()
const authMiddleware = require('../../../middlewares/authMiddleware')
const adminMiddleware = require('../../../middlewares/adminMiddleware')
const chatController = require('../controllers/chatController')

router.post('/', authMiddleware, chatController.createChat)
router.post('/message', authMiddleware, chatController.sendMessage)
router.get('/messages', authMiddleware, chatController.getChat)
router.put('/messages', authMiddleware, chatController.setRead)
router.get('/', authMiddleware, chatController.getChats)
router.get('/support/messages', adminMiddleware, chatController.getSupportChat)
router.get('/support', adminMiddleware, chatController.getSupportChats)
router.get('/byuser', authMiddleware, chatController.getChatByUser)

module.exports = router