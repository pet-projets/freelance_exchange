const Router = require('express')
const router = new Router()
const adminMiddleware = require('../../../middlewares/adminMiddleware')
const ticketController = require('../controllers/ticketController')

router.get('/', adminMiddleware, ticketController.getTickets)
router.put('/', adminMiddleware, ticketController.removeTickets)

module.exports = router