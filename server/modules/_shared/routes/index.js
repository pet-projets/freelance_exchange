const Router = require('express')
const router = new Router()
const fileRouter = require('./fileRouter')
const ticketRouter = require('./ticketRouter')
const userRouter = require('./userRouter')
const chatRouter = require('./chatRouter')

router.use('/file', fileRouter)
router.use('/ticket', ticketRouter)
router.use('/user', userRouter)
router.use('/chat', chatRouter)

module.exports = router
