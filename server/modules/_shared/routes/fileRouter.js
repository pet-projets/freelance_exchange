const Router = require('express')
const router = new Router()
const authMiddleware = require('../../../middlewares/authMiddleware')
const adminMiddleware = require('../../../middlewares/adminMiddleware')
const fileController = require('../controllers/fileController')
const fileUpload = require("express-fileupload")

router.post('/', authMiddleware, fileUpload({createParentPath: true}), fileController.uploadFile)
router.post('/folder', authMiddleware, fileController.createFolder)
router.get('/', fileController.getFilesMetadata)
router.get('/download', fileController.downloadFile)

module.exports = router