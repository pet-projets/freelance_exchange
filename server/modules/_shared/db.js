const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    description: { type: String, default: '' },
    avatar: { type: String, default: null },
    status: { type: String, default: 'basic' },
    wallet: { type: String, default: '' },
    categories: { type: Array, default: []},
    notification: { type: Boolean, default: true },
    role: { type: String, required: true },
    cv: { type: String, default: null },
}, { timestamps: true })

const ticketSchema = new Schema({
    price: { type: Number, required: true },
    receiverId: { type: Object, required: true }
}, { timestamps: true })

const fileSchema = new Schema({
    name: { type: String, required: true },
    pathId: { type: String, required: true },
    link: { type: String, default: ''},
    linkExp: { type: Number, default: 0 },
    mimetype: { type: String, required: true },
    size: { type: Number, required: true },
    path: { type: String, required: true }
})

const chatSchema = new Schema({
    users: { type: Array, required: true },
    isSupport: { type: Boolean, default: false },
    isFreelance: { type: Boolean, default: false },
    offerId: { type: Object, default: null },
}, { timestamps: true })

const messageSchema = new Schema({
    text: { type: String, default: '' },
    files: { type: Array, default: [] },
    chatId: { type: Schema.Types.ObjectId, required: true },
    senderId: { type: Object, required: true },
    timestamp: { type: Number, required: true },
    isRead: { type: Boolean, default: false },
    relatedMessage: { type: Object, default: null }
})


const User = mongoose.model('User', userSchema)
const Ticket = mongoose.model('Ticket', ticketSchema)
const File = mongoose.model('File', fileSchema)
const Chat = mongoose.model('Chat', chatSchema)
const Message = mongoose.model('Message', messageSchema)

module.exports = {
    User,
    Ticket,
    File,
    Chat,
    Message
}
