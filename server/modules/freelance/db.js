const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categorySchema = new Schema({
    name: { type: String, required: true },
    categoryId: { type: Object, default: null },
    isLast: { type: Boolean, default: true },
    shortCode: { type: String, required: true },
    longCode: { type: String, required: true }
}, { timestamps: true })

const offerSchema = new Schema({
    employerId: { type: Object, required: true },
    employeeId: { type: Object, default: null },
    categoryId: { type: Object, default: null},
    title: { type: String, required: true },
    keywords: { type: Array, default: [] },
    description: { type: String, default: '' },
    tasks: { type: Array, default: [] },
    files: { type: Array, default: [] },
    deadline: { type: Number, required: true },
    price: { type: Number, required: true },
    rejected: { type: Boolean, default: false },
    startAt: { type: Number, default: 0 },
    preview: { type: Object, required: true },
    cryptoId: { type: Number, required: true }
}, { timestamps: true })

const ratingSchema = new Schema({
    title: { type: String, required: true },
    value: { type: Number, required: true },
    userId: { type: Object, required: true },
    comment: { type: String, default: '' }
}, { timestamps: true })

const candidateSchema = new Schema({
    userId: { type: Object, required: true },
    offerId: { type: Object, required: true }
})

const Category = mongoose.model('Category', categorySchema)
const Offer = mongoose.model('Offer', offerSchema)
const Rating = mongoose.model('Rating', ratingSchema)
const Candidate = mongoose.model('Candidate', candidateSchema)

module.exports = {
    Category,
    Offer,
    Rating,
    Candidate,
}