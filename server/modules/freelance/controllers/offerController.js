const { Ticket, User, File, Chat, Message } = require("../../_shared/db")
const { Offer, Category, Rating, Candidate } = require('../db')
const emailStore = require('../../../stores/emailStore')
const fileStore = require('../../../stores/fileStore')
const { getOfferEmail } = require('../../../utils/emailHtml')
const { URL } = require("../../../utils/consts")

async function deleteChat(chatId) {
    const messages = await Message.find({chatId: chatId.toString()})
    const filesIds = messages.reduce((a, m) => [...a, ...m.files] , [])
    await File.deleteMany({_id: {$in: filesIds}})
    await Message.deleteMany({chatId: chatId.toString()})
    await Chat.deleteOne({_id: chatId.toString()})
}

async function deleteOffer(offerId, path) {
    const chat = await Chat.findOne({offerId})
    if (chat) await deleteChat(chat._id)
    try {
        await fileStore.delete('/offers/' + path)
    } catch {}
    const offer = await Offer.findOne({_id: offerId})
    await File.deleteMany({_id: {$in: offer.files}})
    await File.deleteOne({_id: offer.preview})
    await Candidate.deleteMany({offerId})
    await Offer.deleteOne({_id: offerId})
}

function sendEmail(email, type, link) {
    const types = [
        { subject: 'New offer created', title: 'Good news!', p: 'A new task has just been created for your activity. You can apply for fulfillment right now!' },
        { subject: 'Congratulations! You are accepted', title: 'Good news!', p: 'You have been accepted for the offer! You can start its implementation right now!' },
        { subject: 'Congratulations! Your offer is complete', title: 'Good job!', p: 'Your employer is satisfied with the result and has credited your offer. Soon you will receive funds for completing this offer in your wallet!' },
        { subject: 'Oops! Your offer was rejected', title: 'Oops...', p: 'Your customer is dissatisfied with your performance and refuses your services. Our team will contact you shortly to clarify the situation.'},
        { subject: 'New candidate!', title: 'New candidate!', p: 'Someone responded to your offer. Check out his candidacy right now!'},
        { subject: 'Consideration of the execution of the offer', title: 'Result', p: 'The complaint regarding the execution of the offer was considered. To get more detailed information about its result, follow the link.' }
    ]
    const activeType = types[type]
    emailStore.sendMessage(email, activeType.subject, getOfferEmail(activeType.title, activeType.p, link))
}

class OfferController {

    async createOffer(req, res, next) {
        const {price, categoryId, title, keywords, description, tasks, deadline, cryptoId, files, preview} = req.body
        const offer = new Offer({employerId: req.user.id, title, categoryId, keywords, deadline, description, tasks, price, cryptoId, files, preview})
        await offer.save()
        // email
        const category = await Category.findOne({_id: categoryId})
        const matchingCategories = await Category.find({ code: { $regex: `^${category.longCode}` } })
        const categoryIds = matchingCategories.map(category => category._id.toString())
        const emailUsers = await User.find({categories: { $in: categoryIds}, notification: true})
        emailUsers.forEach((euser) => sendEmail(euser.email, 0, URL + '/freelance?o=' + offer.id))
        const file = await File.findOne({_id: offer.preview})
        return res.status(200).json({offer: { ...offer.toObject(), preview: file }})
    }

    async createCandidate(req, res, next) {
        const {offerId} = req.body
        const offer = await Offer.findOne({_id: offerId, startAt: 0})
        if (!offer) return res.status(403).json({message: `The offer is no longer valid`})
        if (offer.employerId === req.user.id) return res.status(403).json({message: `You can not apply your own offer`})
        const candidate = new Candidate({userId: req.user.id, offerId})
        await candidate.save()
        // email
        const emailUser = await User.findOne({_id: offer.employerId, notification: true})
        if (emailUser) sendEmail(emailUser.email, 4, URL + '/freelance?o=' + offer.id)
        return res.status(200).json({candidate})
    }

    async removeCandidate(req, res, next) {
        const {candidateId} = req.query
        const candidate = await Candidate.findOne({_id: candidateId})
        if (!candidate) return res.status(403).json({message: `The candidate is no longer exist`})
        if (candidate.userId !== req.user.id) return res.status(403).json({message: `You have not access to this candidate`})
        await candidate.deleteOne()
        return res.status(200).send()
    }

    async getCandidates(req, res, next) {
        const {offerId} = req.query
        const offer = await Offer.findOne({_id: offerId})
        if (offer.employerId !== req.user.id) return res.status(403).json({message: `You have not access to this offer`})
        const candidates = await Candidate.find({offerId})
        const candidateIds = candidates.map(c => c.userId)
        const userCandidates = await User.find({_id: {$in: candidateIds}})
        const userCandidatesInfo = userCandidates.map((u) => {
            return {
                id: candidates.filter((c) => c.userId === u.id)[0].id,
                userId: u.id,
                username: u.username
            }
        })
        return res.status(200).json({candidates: userCandidatesInfo})
    }

    async getRelatedCandidates(req, res, next) {
        const offers = await Offer.find({employerId: req.user.id})
        const candidates = await Candidate.find({
            $or: [
                {userId: req.user.id},
                {offerId: {$in: offers.map((offer) => offer.id)}}
            ]
        })
        return res.status(200).json({candidates})
    }

    async acceptCandidate(req, res, next) {
        const {candidateId} = req.body
        const candidate = await Candidate.findOne({_id: candidateId})
        if (!candidate) return res.status(400).json({message: `Candidate does not exist`})
        const offer = await Offer.findOneAndUpdate({_id: candidate.offerId}, {employeeId: candidate.userId, startAt: Date.now()}, {new: true})
        const chat = new Chat({offerId: candidate.offerId, users: [offer.employerId, candidate.userId], isFreelance: true})
        await chat.save()
        await Candidate.deleteMany({offerId: candidate.offerId})
        const file = await File.findOne({_id: offer.preview})
        // email
        const userEmail = await User.findOne({_id: candidate.userId, notification: true})
        if (userEmail) sendEmail(userEmail.email, 1, URL + '/freelance/chats?ch=' + chat.id)
        return res.status(200).json({offer: { ...offer.toObject(), preview: file }})
    }

    async rejectOffer(req, res, next) {
        const {offerId} = req.body
        let offer = await Offer.findOne({_id: offerId})
        if (!offer) return res.status(400).json({message: `Offer does not exist`})
        if (offer.employerId !== req.user.id) return res.status(403).json({message: `You have not access to remove this offer`})
        if (offer.employeeId) {
            offer = await Offer.findOneAndUpdate({_id: offerId}, {rejected: true}, {new: true})
            const file = await File.findOne({_id: offer.preview})
            const userEmail = await User.findOne({_id: offer.employeeId, notification: true})
            if (userEmail) sendEmail(userEmail.email, 3, URL)
            return res.status(200).json({deleted: false, offer: { ...offer.toObject(), preview: file }})
        } else {
            const ticket = new Ticket({price: offer.price, receiverId: offer.employerId})
            await ticket.save()
            await deleteOffer(offerId, offer.cryptoId)
            return res.status(200).json({deleted: true})
        }
    }

    async acceptOffer(req, res, next) {
        const {offerId, rating} = req.body
        const offer = await Offer.findOne({_id: offerId})
        if (!offer) return res.status(400).json({message: `Offer does not exist`})
        if (!offer.employeeId) return res.status(400).json({message: `Offer's employee does not exist`})
        if (offer.employerId !== req.user.id) return res.status(403).json({message: `You have not access to access this offer`})
        const ratingModel = new Rating({value: rating.value, userId: offer.employeeId, title: offer.title, comment: rating.comment})
        await ratingModel.save()
        const ticket = new Ticket({price: offer.price, receiverId: offer.employeeId})
        await ticket.save()
        const userEmail = await User.findOne({_id: offer.employeeId, notification: true})
        if (userEmail) sendEmail(userEmail.email, 2, URL)
        await deleteOffer(offerId, offer.cryptoId)
        return res.status(200).json({})
    }

    async getRejectedOffers(req, res, next) {
        const offers = await Offer.find({rejected: true})
        // const offersIds = offers.map(offer => offer._id.toString());
        // const chats = await Chat.find({ offerId: { $in: offersIds } });
        // const offersWithChats = offers.map(offer => {
        //     const chat = chats.find(chat => offer._id.toString() === chat.offerId);
        //     return { ...offer.toObject(), chat: chat._id };
        // })
        return res.status(200).json({offers})
    }

    async reviewRejectedOffer(req, res, next) {
        const {result, offerId} = req.body
        const offer = await Offer.findOne({_id: offerId})
        // email
        const emailUsers = await User.find({_id: {$in: [offer.employerId, offer.employeeId]}, notification: true})
        emailUsers.forEach((euser) => sendEmail(euser.email, 5, URL))
        if (result === 'loan') {
            await Offer.updateOne({_id: offerId}, {rejected: false})
        } else if (result === 'reject') {
            await Offer.updateOne({_id: offerId}, {rejected: false, employeeId: null, startAt: 0})
            const chat = await Chat.findOne({offerId})
            await deleteChat(chat?._id)
        } else if (result === 'accept') {
            const ticket = new Ticket({price: offer.price, receiverId: offer.employeeId})
            await ticket.save()
            await deleteOffer(offerId, offer.cryptoId)
        }
        return res.status(200).json({result})
    }

    async getActiveOffers(req, res, next) {
        const offers = await Offer.find();
        const fileIds = offers.map(offer => offer.preview);
        const files = await File.find({ _id: { $in: fileIds } });
        const offersWithFiles = offers.map(offer => {
            const file = files.find(file => file._id.toString() === offer.preview);
            return { ...offer.toObject(), preview: file };
        })
        return res.status(200).json({offers: offersWithFiles})
    }

    // async getRelatedOffers(req, res, next) {
    //     const candidates = await Candidate.find({userId: req.user.id})
    //     const offers = await Offer.find({
    //         $or: [
    //             {employerId: req.user.id},
    //             {employeeId: req.user.id},
    //             {_id: {$in: candidates.map((c) => c.offerId)}}
    //         ]
    //     })
    //     const fileIds = offers.map(offer => offer.preview);
    //     const files = await File.find({ _id: { $in: fileIds } });
    //     const offersWithFiles = offers.map(offer => {
    //         const file = files.find(file => file._id.toString() === offer.preview);
    //         return { ...offer.toObject(), preview: file };
    //     })
    //     return res.status(200).json({offers: offersWithFiles})
    // }

    async createCategory(req, res, next) {
        const {categoryId, name} = req.body
        let category = null
        let mainCategory = null
        if (!!categoryId.length) mainCategory = await Category.findOne({_id: categoryId})
        const subcategories = await Category.find({categoryId})
        if (!subcategories.length) {
            const otherEntityCategory = new Category({name: 'Other', categoryId, shortCode: '0', longCode: mainCategory.longCode + '-' + '0'})
            await otherEntityCategory.save()
            category = new Category({name, categoryId, shortCode: '1', longCode: mainCategory?.longCode + '-' + '1'})
            await Offer.updateMany({categoryId}, {categoryId: otherEntityCategory.id})
        } else {
            const maxCodeValue = subcategories.map(s => s.shortCode).sort((a, b) => parseInt(b, 16) - parseInt(a, 16))[0]
            const newCodeValue = (parseInt(maxCodeValue, 16) + 1).toString(16)
            const longCode = !!categoryId.length ? mainCategory?.longCode + '-' + newCodeValue : '0-' + newCodeValue
            category = new Category({name, categoryId, shortCode: newCodeValue, longCode})
        }
        await category.save()
        await mainCategory.updateOne({ isLast: false })
        return res.status(200).json({})
    }

    async deleteCategory(req, res, next) {
        const {categoryId} = req.query
        const category = await Category.findOne({_id: categoryId})
        const subcategories = await Category.find({longCode: new RegExp(`^${category.longCode}`) })
        const subcategoriesIds = subcategories.map((sub) => sub.id)
        const otherEntityCode = category.longCode.split('-').slice(0, -1).join('-') + '-0'
        const otherEntityCategory = await Category.findOne({longCode: otherEntityCode})
        await Offer.updateMany({categoryId: {$in: subcategoriesIds}}, {categoryId: otherEntityCategory.id})
        await Category.deleteMany({ longCode: new RegExp(`^${category.longCode}`) })
        return res.status(200).json({})
    }

    async editCategory(req, res, next) {
        const {categoryId, name} = req.body
        await Category.updateOne({_id: categoryId}, {name})
        return res.status(200).json({})
    }

    async getAllCategories(req, res, next) {
        const categories = await Category.find()
        return res.status(200).json({categories})
    }
    
}

module.exports = new OfferController()