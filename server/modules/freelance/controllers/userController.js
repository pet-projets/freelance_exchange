const {User, File} = require("../../_shared/db")
const {Category, Rating} = require('../db')
const { generateJwt } = require('../../../utils/functions')
const fileStore = require('../../../stores/fileStore')

class UserController {

    async changeCategories(req, res, next) {
        const {categories} = req.body
        await User.updateOne({_id: req.user.id}, {categories})
        return res.status(200).json({categories})
    }

    async setCV(req, res, next) {
        const {cvId} = req.body
        const user = await User.findOne({_id: req.user.id})
        if (!!user.cv) {
            const prevCv = await File.findOne({_id: user.cv})
            await fileStore.delete(prevCv.pathId)
            await prevCv.deleteOne()
        }
        await user.updateOne({cv: cvId})
        return res.status(200).json({message: 'CV was set'})
    }

    async removeCV(req, res, next) {
        const user = await User.findOne({_id: req.user.id})
        if (!!user.cv) {
            const prevCv = await File.findOne({_id: user.cv})
            await fileStore.delete(prevCv.pathId)
            await prevCv.deleteOne()
            await user.updateOne({cv: null})
        }
        return res.status(200).json({message: 'CV was removed'})
    }

    async setRole(req, res, next) {
        const {role} = req.body
        const user = await User.findOneAndUpdate({_id: req.user.id}, {role})
        const token = generateJwt(user)
        return res.status(200).json({token})
    }

}

module.exports = new UserController()