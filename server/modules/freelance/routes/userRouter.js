const Router = require('express')
const router = new Router()
const authMiddleware = require('../../../middlewares/authMiddleware')
const userController = require('../controllers/userController')

router.put('/cv', authMiddleware, userController.setCV)
router.delete('/cv', authMiddleware, userController.removeCV)
router.put('/categories', authMiddleware, userController.changeCategories)
router.put('/role', authMiddleware, userController.setRole)

module.exports = router