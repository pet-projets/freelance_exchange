const Router = require('express')
const router = new Router()
const offerRouter = require('./offerRouter')
const userRouter = require('./userRouter')

router.use('/offer', offerRouter)
router.use('/user', userRouter)

module.exports = router
