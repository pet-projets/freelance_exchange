const jwt = require('jsonwebtoken')

module.exports = function(req, res, next) {
    if (req.method === "OPTIONS") {
        next()
    }
    try {
        const token = req.headers.authorization.split(' ')[1]
        const decoded = jwt.verify(token, process.env.JWT_KEY)
        if (decoded.status !== 'admin') res.status(401).json({message: 'Forbidden'})
        next()
    } catch(e) {
        res.status(401).json({message: 'User has not authorized'})
    }
}