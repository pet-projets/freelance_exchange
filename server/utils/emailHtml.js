

const getAuthEmail = (emailCode) => {
    const emailHtml = 
    `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Montserrat';
                color: #141414;
            }
            .wrapper {
            }
            .content {
                margin: 20px auto 0;
                max-width: 600px;
                background-color: #efefef;
                border-radius: 25px;
                padding: 20px 40px;
            }
            .message-img {
            }
            .message-img > img {
                display: block;
                margin: 0 auto;
                width: 120px;
                height: 120px;
            }
            .text-content {
                padding: 20px 40px 20px;
                background-color: #fcfcfc;
                border-radius: 20px;
                margin-top: 20px;
                margin-bottom: 20px;
            }
            .text-content > h2 {
                font-size: 28px;
                font-weight: 600;
                margin-bottom: 40px;
            }
            .text-content > p {
                font-size: 18px;
                margin-bottom: 30px;
            }
            .code {
                margin: 0 auto;
                max-width: 200px;
                padding: 12px 40px;
                background: transparent;
                border: 4px solid #141414;
                border-radius: 20px;
            }
            .code > p {
                text-align: center;
                text-decoration: none;
                color: #141414;
                font-weight: 600;
                font-size: 24px;
                letter-spacing: 7.5px;
                margin: 0;
            }
            .end-text {
                margin: 40px 0;
            }
            .end-text > p {
                font-size: 18px;
            }
            .expire {
                padding-top: 20px;
                border-top: 1px solid #666666;
            }
            .expire > p {
                font-size: 16px;
                text-align: center;
            }
            @media (max-width: 500px) {
                .content {
                    padding: 20px 20px 10px;
                }
                .text-content {
                    padding: 10px 20px 10px;
                }
            }
        </style>
    </head>
    <body>
        
        <div class="wrapper">
        
            <div class="content">
        
                <div class="message-img">
                    <img src="https://i.ibb.co/vHNV9t5/logo.jpg">
                </div>
        
                <div class="text-content">
                    <h2>Welcome!</h2>
                    <p>You're receiving this message because you recently sent an email confirmation request.</p>
                    <p>Confirm your email address by copying the code below. This step adds extra security to your business by verifying you own this email.</p>
                </div>

                <div class="code">
                    <p>${emailCode}</p>
                </div>
        
                <div class="end-text">
                    <p>Thanks, </p>
                    <p>Melius Effect</p>
                </div>
        
                <div class="expire">
                    <p>This code expire in 10 minutes</p>
                </div>
        
            </div>

        </div>

    </body>
    </html>
    `
    return emailHtml
}

const getOfferEmail = (title, p, link) => {
    const emailHtml =
    `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Montserrat';
                color: #141414;
            }
            .wrapper {
            }
            .content {
                margin: 20px auto 0;
                max-width: 600px;
                background-color: #efefef;
                border-radius: 25px;
                padding: 20px 40px;
            }
            .message-img {
            }
            .message-img > img {
                display: block;
                margin: 0 auto;
                width: 120px;
                height: 120px;
            }
            .text-content {
                padding: 20px 40px 20px;
                background-color: #fcfcfc;
                border-radius: 20px;
                margin-top: 20px;
                margin-bottom: 20px;
            }
            .text-content > h2 {
                font-size: 28px;
                font-weight: 600;
                margin-bottom: 40px;
            }
            .text-content > p {
                font-size: 18px;
                margin-bottom: 30px;
            }
            @media (max-width: 500px) {
                .content {
                    padding: 20px 20px 10px;
                }
                .text-content {
                    padding: 10px 20px 10px;
                }
            }
        </style>
    </head>
    <body>
        
        <div class="wrapper">
        
            <div class="content">
        
                <div class="message-img">
                    <img src="https://i.ibb.co/vHNV9t5/logo.jpg">
                </div>
        
                <div class="text-content">
                    <h2>${title}</h2>
                    <p>${p}</p>
                    <a href="${link}">${link}</a>
                </div>
        
            </div>

        </div>

    </body>
    </html>
    `
    return emailHtml
}

module.exports = {getAuthEmail, getOfferEmail}