const jwt = require('jsonwebtoken')

function generateNumber(digitCount) {
    let number = []
    for (let i=0; i < digitCount; i++) {
        number.push(parseInt(Math.floor(Math.random()*10)))
    }
    return number.join('')
}

function generateCode(size) {
    let symbols = []
    const upper = String.fromCharCode(...Array.from({ length: 90 - 65 + 1 }, (_, i) => i + 65))
    const lower = String.fromCharCode(...Array.from({ length: 122 - 97 + 1 }, (_, i) => i + 97))
    const allCases = [...lower, ...upper]
    for (let i=0; i < size; i++) {
        symbols.push(allCases[parseInt(Math.floor(Math.random()*allCases.length))])
    }
    return symbols.join('')
}

const getRandomElement = (array) => {
    const randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
}

const generateJwt = (model) => {
    return jwt.sign(
        {
            id: model.id,
            username: model.username,
            email: model.email,
            status: model.status,
            notification: model.notification,
            role: model.role
        },
        process.env.JWT_KEY,
        {expiresIn: '7d'}
    )
}


module.exports = {generateCode, generateNumber, getRandomElement, generateJwt}