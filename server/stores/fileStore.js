const axios = require('axios')
const {v4} = require('uuid')

const BASE_URL_API = 'https://api.dropbox.com/oauth2/'
const BASE_API_URL_API = 'https://api.dropboxapi.com/2/'
const BASE_URL_CONTENT = 'https://content.dropboxapi.com/2/'
const EXPIRE_TIME = 14000

const TEMP_ACCESS_TOKEN = process.env.DROPBOX_ACCESS_TOKEN

class fileStore {

    constructor() {
        this._accessToken = ''
        this._tokenExpiration = 0
    }

    async refreshToken() {
        if (Date.now() < this._tokenExpiration) return
        const {data} = await axios.post(BASE_URL_API+'token'+
        `?grant_type=refresh_token&refresh_token=${process.env.DROPBOX_REFRESH_TOKEN}&client_id=${process.env.DROPBOX_CLIENT_ID}&client_secret=${process.env.DROPBOX_CLIENT_SECRET}`)
        this._accessToken = data.access_token
        this._tokenExpiration = Date.now() + EXPIRE_TIME * 1000
    }

    async uploadFile(file, folder, type) {
        await this.refreshToken()
        const {data} = await axios.post(BASE_URL_CONTENT+'files/upload', file, {
            headers: {
                'Authorization': `Bearer ${TEMP_ACCESS_TOKEN || this._accessToken}`,
                'Dropbox-API-Arg': JSON.stringify({
                    autorename: true,
                    mode: 'add',
                    mute: false,
                    path: `${folder}/${v4()}.${type}`
                }),
                'Content-Type': 'application/octet-stream'
            }
        })
        return data.id
    }

    async delete(path) {
        await this.refreshToken()
        await axios.post(BASE_API_URL_API+'files/delete_v2', {path}, {
            headers: {
                'Authorization': `Bearer ${TEMP_ACCESS_TOKEN || this._accessToken}`,
                'Content-Type': 'application/json'
            }
        })
    }

    async createFolder(path) {
        await this.refreshToken()
        await axios.post(BASE_API_URL_API+'files/create_folder_v2', {autorename: false, path}, {
            headers: {
                'Authorization': `Bearer ${TEMP_ACCESS_TOKEN || this._accessToken}`,
                'Content-Type': 'application/json'
            }
        })
    }

    async generateLink(pathId) {
        await this.refreshToken()
        const {data} = await axios.post(BASE_API_URL_API+'files/get_temporary_link', {path: pathId}, {
            headers: {
                'Authorization': `Bearer ${TEMP_ACCESS_TOKEN || this._accessToken}`,
                'Content-Type': 'application/json'
            }
        })
        return data.link
    }
    
}


module.exports = new fileStore()
