const nodemailer = require('nodemailer')

class EmailStore {

    constructor() {
        this._data = {}
        this._name = 'Melius Effect'
        this._email = process.env.NODEMAILER_EMAIL
        this._password = process.env.NODEMAILER_PASSWORD
    }

    getCode(email) {
        return this._data[email]
    }

    removeData(email) {
        delete this._data[email]
    }

    pushData(email, code) {
        this._data[email] = code
        setTimeout(() => {
            this.removeData(email)
        }, 1000 * 60 * 10)
    }

    get email() {
        return this._email
    }

    get password() {
        return this._password
    }

    get from() {
        return { name: this._name, address: this._email }
    }

    async sendMessage(email, subject, html) {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {user: this.email, pass: this.password}
        })
        const mailConfigs = {
            from: this.from, to: email,
            subject, html
        }
        try {
            await transporter.sendMail(mailConfigs)
            return true
        } catch {
            return false
        }
    }
}


module.exports = new EmailStore()
