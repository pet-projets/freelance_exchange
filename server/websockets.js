let connectedUsers = []

const connect = (io) => {
    io.on('connection', (socket) => {

        // General
        socket.on('join-server', (room) => {
            socket.join(room)
            connectedUsers = connectedUsers.filter(obj => obj.userId !== room.split('-')[1])
            connectedUsers.push({socketId: socket.id, userId: room.split('-')[1]})
            io.emit('online', room.split('-')[1])
            io.to(room).emit('onlines', connectedUsers.map((cu) => cu.userId))
        })
        socket.on('disconnect', () => {
            const usrId = connectedUsers.find(obj => obj.socketId === socket.id)?.userId
            connectedUsers = connectedUsers.filter(obj => obj.socketId !== socket.id)
            io.emit('offline', usrId)
        })

        // Chat
        socket.on('join-chat', (room) => {
            socket.join(room)
        })
        socket.on('new-chat', (chaterId, chat) => {
            socket.to('user-' + chaterId).emit('new-chat', chat)
        })
        socket.on('send-message', (room, message, chaterId) => {
            io.in(room).allSockets().then((data) => {
                if (data.size !== 2) {
                    socket.to('user-' + (chaterId || process.env.ADMIN_ID)).emit('notification-message', message, room.split('-')[1])
                } else {
                    socket.to(room).emit('send-message', message)
                }
            })
        })
        socket.on('leave-chat', (room) => {
            socket.leave(room)
        })
        socket.on('read-messages', (chaterId, chatId) => {
            socket.to('user-' + (chaterId || process.env.ADMIN_ID)).emit('read-messages')
            socket.to('user-' + (chaterId || process.env.ADMIN_ID)).emit('read-panel-messages', chatId)
        })
        socket.on('typing', (room) => {
            socket.to(room).emit('typing')
        })
        
        
    })
}

module.exports = { connect }