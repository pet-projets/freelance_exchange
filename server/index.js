require('dotenv').config()
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const {connect} = require('./websockets')
const PORT = process.env.PORT || 5000
const http = require('http')

const sharedRouter = require('./modules/_shared/routes')
const freelanceRouter = require('./modules/freelance/routes')
const { URL } = require('./utils/consts')

const app = express()
app.use(express.json({limit: '100mb'}))

app.use('/shared', cors({origin: URL}), sharedRouter)
app.use('/freelance', cors({origin: URL}), freelanceRouter)


const server = http.createServer(app)
const io = require('socket.io')(server, {
    cors: {
        origin: URL
    }
})

const start = async () => {
    try {
        mongoose.set('strictQuery', true)
            .connect("mongodb://freelance-mongo-db/freelanceExchange")
            .then(() => console.log('MongoDB was connected successfully'))
            .then(() => connect(io))
            .catch((e) => console.log(e))
        server.listen(PORT, () => console.log(`Server starts on port ${PORT}`))
    } catch(e) {
        console.log(e)
    }
}

start()