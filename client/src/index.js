import React, { createContext } from 'react';
import ReactDOM from 'react-dom/client';
import App from './app/App';
import CategoryStore from './app/stores/CategoryStore';
import ChatStore from './app/stores/ChatStore';
import OfferStore from './app/stores/OfferStore';
import UserStore from './app/stores/UserStore';
import {io} from 'socket.io-client'
import PostStore from './app/stores/PostStore';
export const Context = createContext(null)

const socket = io(process.env.REACT_APP_API_URL)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Context.Provider value={{
      user: new UserStore(),
      categories: new CategoryStore(),
      offers: new OfferStore(),
      chats: new ChatStore(),
      posts: new PostStore(),
      socket
    }}>
      <App />
    </Context.Provider>
    
);

