import {$host, $authHost} from 'api'

export const createOffer = async (price, categoryId, title, keywords, description, tasks, deadline, cryptoId, files, preview) => {
    const {data} = await $authHost.post('freelance/offer', {price, categoryId, title, keywords, description, tasks, deadline, cryptoId, files, preview})
    return data
}

export const createCandidate = async (offerId) => {
    const {data} = await $authHost.post('freelance/offer/candidate', {offerId})
    return data
}

export const getCandidates = async (offerId) => {
    const {data} = await $authHost.get('freelance/offer/candidate', {params: {offerId}})
    return data
}

export const getRelatedCandidates = async () => {
    const {data} = await $authHost.get('freelance/offer/candidate/related')
    return data
}

export const acceptCandidate = async (candidateId) => {
    const {data} = await $authHost.put('freelance/offer/candidate', {candidateId})
    return data
}

export const removeCandidate = async (candidateId) => {
    const {data} = await $authHost.delete('freelance/offer/candidate', {params: {candidateId}})
    return data
}

export const rejectOffer = async (offerId) => {
    const {data} = await $authHost.put('freelance/offer/reject', {offerId})
    return data
}

export const acceptOffer = async (offerId, rating) => {
    const {data} = await $authHost.put('freelance/offer/accept', {offerId, rating})
    return data
}

export const getRejectedOffers = async () => {
    const {data} = await $authHost.get('freelance/offer/review')
    return data
}

export const reviewRejectedOffer = async (result, offerId) => {
    const {data} = await $authHost.put('freelance/offer/review', {result, offerId})
    return data
}

export const getActiveOffers = async () => {
    const {data} = await $host.get('freelance/offer')
    return data
}

export const createCategory = async (categoryId, name) => {
    const {data} = await $authHost.post('freelance/offer/category', {categoryId, name})
    return data
}

export const deleteCategory = async (categoryId) => {
    const {data} = await $authHost.delete('freelance/offer/category', {params: {categoryId}})
    return data
}

export const editCategory = async (categoryId, name) => {
    const {data} = await $authHost.put('freelance/offer/category', {categoryId, name})
    return data
}

export const getCategoriesTree = async () => {
    const {data} = await $host.get('freelance/offer/category-tree')
    return data
}

export const getAllCategories = async () => {
    const {data} = await $host.get('freelance/offer/category')
    return data
}