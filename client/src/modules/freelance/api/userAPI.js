import {$host, $authHost} from 'api'

export const setCV = async (cvId) => {
    const {data} = await $authHost.put('freelance/user/cv', {cvId})
    return data
}

export const removeCV = async () => {
    const {data} = await $authHost.delete('freelance/user/cv')
    return data
}

export const changeCategories = async (categories) => {
    const {data} = await $authHost.put('freelance/user/categories', {categories})
    return data
}

export const setRole = async (role) => {
    const {data} = await $authHost.put('freelance/user/role', {role})
    localStorage.setItem('token', data.token)
    return role
}