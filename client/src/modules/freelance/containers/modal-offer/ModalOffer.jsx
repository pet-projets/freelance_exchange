import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { Context } from 'index'
import Button from 'UI/button/Button'
import Modal from 'UI/modal/Modal'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'
import './modal-offer.scss'
import listIcon from 'assets/icons/white/offer.svg'
import attachIcon from 'assets/icons/white/attach.svg'
import messageIcon from 'assets/icons/white/message.svg'
import { getFilesMetadata } from 'modules/_shared/api/fileAPI'
import { acceptCandidate, acceptOffer, createCandidate, rejectOffer, removeCandidate } from 'modules/freelance/api/offerAPI'
import ButtonPanel from './components/ButtonPanel'
import { CHATS_ROUTE } from 'routes/routesPath'
import Rating from './components/Rating'
import Candidates from './components/Candidates'
import ConnectWallet from 'modules/_shared/containers/connect-wallet/ConnectWallet'
import { deadlineConvert } from 'utils/functions'
import Alert from 'modules/_shared/containers/alert/Alert'

const ModalOffer = observer((props) => {
    const [loading, setLoading] = useState(true)
    const {offers, user, chats} = useContext(Context)
    const navigate = useNavigate(null)
    const [offer, setOffer] = useState(null)
    const [files, setFiles] = useState([])
    const [active, setActive] = useState(false)
    const location = useLocation(null)
    const [functionalCode, setFunctionalCode] = useState(0)

    const [isCandidates, setIsCandidates] = useState(false)

    const [isRating, setIsRating] = useState(false)
    const [ratingValue, setRatingValue] = useState(1)
    const [ratingComment, setRatingComment] = useState('')

    const [isConnectWallet, setIsConnectWallet] = useState(false)

    const [alert, setAlert] = useState('')

    useEffect(() => {
        const offerId = new URLSearchParams(location.search).get('o')
        const tempOffer = offers.getItemById(offerId)
        if (!tempOffer) {
            setOffer(null)
            setActive(false)
            setFiles([])
            return
        }
        setOffer(tempOffer)
        setActive(true)
        setLoading(false)
    }, [location])

    useEffect(() => {
        if (!offer || !offer?.files.length) return
        getFilesMetadata(offer.files)
        .then((data) => {
            setFiles(data.files)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }, [offer])

    useEffect(() => {
        if (!active) return
        setFunctionalCode(offers.getOfferFunctionalCode(offer?._id, user.role, user.info.id))
    }, [active])

    const closeFunc = () => {
        const searchValue = new URLSearchParams(location.search).get('o')
        let newSearchParams = location.search.replace('?o=' + searchValue, '').replace('&o=' + searchValue, '')
        navigate(location.pathname.split('?')[0] + newSearchParams)
    }

    const acceptCandidateFunc = (candidateId) => {
        setLoading(true)
        acceptCandidate(candidateId)
        .then((data) => {
            offers.updateItem(data.offer)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
        .finally(() => closeFunc())
    }

    const rejectOfferFunc = () => {
        setLoading(true)
        rejectOffer(offer._id)
        .then((data) => {
            if (data.deleted) offers.deleteItem(offer._id)
            else offers.updateItem(data.offer)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
        .finally(() => closeFunc())
    }

    const acceptOfferFunc = () => {
        setLoading(true)
        acceptOffer(offer._id, {value: ratingValue, comment: ratingComment})
        .then(() => {
            offers.deleteItem(offer._id)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
        .finally(() => closeFunc())
    }

    const applyFunc = () => {
        setLoading(true)
        createCandidate(offer._id)
        .then((data) => {
            offers.addCandidate(data.candidate)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
        .finally(() => closeFunc())
    }

    const removeCandidateFunc = () => {
        setLoading(true)
        const candidateId = offers.getCandidateByOfferId(offer._id)?._id
        removeCandidate(candidateId)
        .then(() => {
            offers.deleteCandidate(candidateId)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
        .finally(() => closeFunc())
    }

    const chatFunc = () => {
        const chatId = chats.getItemByChaterId(user.info.id, user.role === 'employer' ? offer.employeeId : offer.employerId, user.role)?._id
        navigate(CHATS_ROUTE + '?ch=' + chatId)
    }

    return (
        <>
        {active && <>
            <Modal className={'modal-offer'} title={''} closeFunc={closeFunc} width={450} loading={loading} >
                <div className="modal-offer-header">
                    <div className="modal-offer-preview">
                        {offer?.preview && <FileDisplayer
                            previewOnly
                            fileModel
                            image
                            file={offer?.preview}
                            width={'auto'} height={'auto'}
                        /> }
                        <div className="preview-darker"></div>
                    </div>
                    <div className="modal-offer-top">
                        <div className="top-info">
                            <div>
                                <div className="label">Price</div>
                                <div className="price">{offer?.price} ETH</div>
                            </div>
                            {(functionalCode === 2 || functionalCode === 5) &&
                                <Button className='empty' img={messageIcon} onClick={chatFunc} />
                            }
                        </div>
                        <ButtonPanel
                            functionalCode={functionalCode}
                            rejectOfferFunc={rejectOfferFunc}
                            removeCandidateFunc={removeCandidateFunc}
                            applyFunc={applyFunc}
                            openRating={() => setIsRating(true)}
                            openCandidates={() => setIsCandidates(true)}
                            openConnectWallet={() => setIsConnectWallet(true)}
                        />
                    </div>
                </div>
                <div className="modal-offer-body">
                    <div className="title">{offer?.title}</div>
                    <div className="keywords">
                        {offer?.keywords.map((p, i) => <div key={i}>#{p}</div> )}
                    </div>
                    {offer?.description && <div className="desc">
                        {offer?.description.split('\n').map((p, i) => <div key={i}>{p}</div> )}
                    </div>}
                    <div className="tasks">
                        <div className="section-title">
                            <img src={listIcon} />
                            <span>List of tasks</span>
                        </div>
                        <div className="task-list">
                            {offer?.tasks.map((p, i) => <div key={i}>{i+1}. {p}</div> )}
                        </div>
                    </div>
                    {!!files.length && <div className="files">
                        <div className="section-title">
                            <img src={attachIcon} />
                            <span>Attach files</span>
                        </div>
                        <div className="file-list">
                            {files.map((file) => {
                                return <FileDisplayer
                                    key={file._id}
                                    file={file}
                                    fileModel
                                    download
                                    fullscreen
                                    // width={220}
                                    fullscreenImageFunc={props.fullscreenImageFunc}
                                    fullscreenVideoFunc={props.fullscreenVideoFunc}
                                />
                            })}
                        </div>
                    </div>}
                    <div className="deadline">
                        {offer?.deadline > 0 ?
                            (!offer?.startAt ? `Deadline: ${offer?.deadline} days` : deadlineConvert(offer?.deadline, offer?.startAt))
                            : 'No Deadline'}
                    </div>
                </div>
            </Modal>
            {isRating && <Rating
                ratingValue={ratingValue}
                setRatingValue={setRatingValue}
                ratingComment={ratingComment}
                setRatingComment={setRatingComment}
                acceptOfferFunc={acceptOfferFunc}
                closeFunc={() => setIsRating(false)}
            />}
            {functionalCode === 1 && isCandidates && <Candidates
                offerId={offer._id}
                userInfoFunc={props.userInfoFunc}
                acceptCandidateFunc={acceptCandidateFunc}
                closeFunc={() => setIsCandidates(false)}
            />}
            {isConnectWallet && <ConnectWallet closeFunc={() => setIsConnectWallet(false)} />}
        </>}
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
    )
})

export default ModalOffer