import React, { useEffect, useState } from 'react'
import { getCandidates } from 'modules/freelance/api/offerAPI'
import Button from 'UI/button/Button'
import EmptySign from 'UI/empty-sign/EmptySign'
import Loader from 'UI/loader/Loader'
import Modal from 'UI/modal/Modal'
import Alert from 'modules/_shared/containers/alert/Alert'

const Candidates = (props) => {
    const [candidates, setCandidates] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [alert, setAlert] = useState('')

    useEffect(() => {
        getCandidates(props.offerId)
            .then((data) => {
                setCandidates(data.candidates)
            })
            .catch((err) => {
                setAlert(err.response.data.message)
            })
            .finally(() => setIsLoading(false))
    }, [])

    return (
        <>
        <Modal title='Candidates' closeFunc={props.closeFunc}>
            <div className='modal-offer-candidates button-panel-apendix'>
                {candidates.map((c, i) => {
                    return <div key={c.userId} className="modal-offer-candidate">
                        <div className="username">
                            <b>{i+1}.</b>
                            <span onClick={() => props.userInfoFunc(c.userId)}>{c.username}</span>
                        </div>
                        <Button label={'Accept'} className='' onClick={() => {
                            props.acceptCandidateFunc(c.id)
                            props.closeFunc()
                        }} />
                    </div>
                })}
                {!candidates.length && !isLoading && <EmptySign />}
                {isLoading && <Loader /> }
            </div>
        </Modal>
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
}

export default Candidates