import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import useWallet from 'hooks/useWallet'
import { LOGIN_ROUTE } from 'routes/routesPath'
import Button from 'UI/button/Button'

const ButtonPanel = (props) => {
    const navigate = useNavigate(null)

    const {isConnected} = useWallet()

    return (
        <>
        {!!props.functionalCode && <div className='button-panel'>
            {props.functionalCode === 1 && <>
                <Button
                    label={'Candidates'}
                    className='empty'
                    onClick={props.openCandidates}
                />
                <Button label={'Delete'} className='red' onClick={props.rejectOfferFunc} />
            </>}
            {props.functionalCode === 2 && <>
                <Button
                    label={'Accept'}
                    className={'green'}
                    onClick={props.openRating}
                />
                <Button label='Reject' className='red' onClick={props.rejectOfferFunc} />
            </> }
            {props.functionalCode === 3 && <Button label='Apply' onClick={isConnected ? props.applyFunc : props.openConnectWallet} />}
            {props.functionalCode === 4 && <Button label='Cancel' className='red' onClick={props.removeCandidateFunc} />}
            {props.functionalCode === 6 && <Button label='Apply' onClick={() => navigate(LOGIN_ROUTE)} />}
        </div>}
        </>
        
    )
}

export default ButtonPanel