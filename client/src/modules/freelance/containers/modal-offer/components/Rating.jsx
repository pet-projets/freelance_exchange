import React from 'react'
import Button from 'UI/button/Button'
import Modal from 'UI/modal/Modal'
import StarsChooser from 'UI/stars/StarsChooser'
import { Textarea } from 'UI/textarea/Textarea'

const Rating = (props) => {

    return (
        <>
        <Modal title="Rate" closeFunc={props.closeFunc}>
            <div className="modal-offer-rating button-panel-apendix">
                <StarsChooser value={props.ratingValue} valueFunc={props.setRatingValue} />
                <Textarea
                    value={props.ratingComment}
                    valueFunc={props.setRatingComment}
                    rows={3.5}
                    placeholder={'Comment (optional) ...'}
                />
                <Button label={'Accept'} onClick={() => {
                    props.acceptOfferFunc()
                    props.closeFunc()
                }} />
            </div>
        </Modal>
        </>
    )
}

export default Rating