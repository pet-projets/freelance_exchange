import React from 'react'
import Success from 'UI/success/Success'

const Payment = () => {

    return (
        <>
        <div className='create-offer-modal-box payment'>
            <Success label={'Payment Success'} />
        </div>
        </>
        
    )
}

export default Payment