import React, { useState } from 'react'
import Button from 'UI/button/Button'
import Input from 'UI/input/Input'
import closeIcon from 'assets/icons/white/add.svg'

const Tasks = (props) => {
    const [taskInput, setTaskInput] = useState('')

    const removeTask = (task) => {
        props.setTasks([...props.tasks.filter((t) => t !== task)])
    }

    const addTask = () => {
        if (!taskInput.length) return
        if (props.tasks.includes(taskInput)) return
        props.setTasks([...props.tasks, taskInput])
        setTaskInput('')
    }
    
    return (
        <>
        <div className='create-offer-modal-box tasks'>
            <div className="offer-modal-text">Write a list of tasks that must be completed by the employee for the offer to be accepted</div>
            <div className="offer-modal-content">
                <div className="add-panel">
                    <Input placeholder={`Task ${props.tasks.length+1} ...`} valueFunc={setTaskInput} value={taskInput} />
                    <Button className={'empty'} label={'Add'} onClick={addTask} />
                </div>
                <div className="tasks-box">
                    {props.tasks.map((task, i) => {
                        return <div className="task-item" key={i}>
                            <span><b>{i+1}.</b> {task}</span>
                            <img src={closeIcon} onClick={() => removeTask(task)} />
                        </div>
                    })}
                </div>
            </div>
        </div>
        <div className="button-panel">
            <Button className={'thin'} label={'Back'} onClick={props.backFunc} />
            {!!props.tasks.length && <Button label={'Next'} onClick={props.nextFunc} />}
        </div>
        </>
    )
}

export default Tasks