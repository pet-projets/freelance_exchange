import React from 'react'
import Button from 'UI/button/Button'
import InputNumber from 'UI/input-number/InputNumber'

const Price = (props) => {

    return (
        <>
        <div className='create-offer-modal-box price'>
            <div className="offer-modal-text">Set a price for the execution of this offer</div>
            <div className="offer-modal-balance">Balance: {props.balance} ETH</div>
            <div className="offer-modal-content">
                <InputNumber
                    sign={'eth'}
                    value={props.price}
                    valueFunc={props.setPrice}
                />
            </div>
        </div>
        <div className="button-panel">
            <Button className={'thin'} label={'Back'} onClick={props.backFunc} />
            {props.price > 0 && props.balance >= props.price && <Button label={'Submit'} onClick={props.nextFunc} />}
        </div>
        </>
    )
}

export default Price