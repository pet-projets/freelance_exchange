import React from 'react'
import Button from 'UI/button/Button'
import InputNumber from 'UI/input-number/InputNumber'

const Deadline = (props) => {


    return (
        <>
        <div className='create-offer-modal-box deadline'>
            <div className="offer-modal-text">Choose how much time is allocated to perform these tasks (optional)</div>
            <div className="offer-modal-content">
                <InputNumber
                    positiveOnly
                    sign={'days'}
                    value={props.deadline}
                    valueFunc={props.setDeadline}
                />
            </div>
        </div>
        <div className="button-panel">
            <Button className={'thin'} label={'Back'} onClick={props.backFunc} />
            <Button label={'Next'} onClick={props.nextFunc} />
        </div>
        </>
    )
}

export default Deadline