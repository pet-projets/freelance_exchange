import React from 'react'
import Button from 'UI/button/Button'
import CategoriesPanel from 'modules/freelance/containers/categories-panel/CategoriesPanel'

const Category = (props) => {

    return (
        <>
        <div className='create-offer-modal-box category'>
            <div className="offer-modal-text">Сhoose the category that best corresponds to your task</div>
            <div className="offer-modal-content">
                
                <div className="category-active">
                    {props.category?.name || 'Category ...'}
                </div>
                <CategoriesPanel func={props.setCategory} initCode={props.category?.longCode || '0'} />
            </div>
        </div>
        <div className="button-panel">
            <Button className={'thin'} label={'Back'} onClick={props.backFunc} />
            {props.category && <Button label={'Next'} onClick={props.nextFunc} />}
        </div>
        </>
        
    )
}

export default Category