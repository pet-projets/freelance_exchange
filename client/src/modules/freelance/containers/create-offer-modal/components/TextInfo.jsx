import React from 'react'
import Button from 'UI/button/Button'
import Input from 'UI/input/Input'
import { Textarea } from 'UI/textarea/Textarea'

const TextInfo = (props) => {

    return (
        <>
        <div className='create-offer-modal-box text-info'>
            <div className="offer-modal-text">Describe the task</div>
            <div className="offer-modal-content">
                <Input
                    placeholder={'Title ...'}
                    initValue={props.title || ''}
                    valueFunc={props.setTitle}
                />
                <Textarea
                    placeholder={'Description (optional) ...'}
                    initValue={props.desc || ''}
                    valueFunc={props.setDesc}
                    rows={5}
                />
            </div>
        </div>
        <div className="button-panel">
            {!!props.title.length && <Button label={'Next'} onClick={props.nextFunc} />}
        </div>
        </>
    )
}

export default TextInfo