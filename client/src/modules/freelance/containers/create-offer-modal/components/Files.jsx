import React, { useEffect, useState } from 'react'
import Button from 'UI/button/Button'
import InputFile from 'UI/input-file/InputFile'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'
import imageIcon from 'assets/icons/white/image.svg'
import attachIcon from 'assets/icons/white/attach.svg'

const Files = (props) => {
    const [files, setFiles] = useState([])

    const removeFile = (name) => {
        const newFiles = files.filter((f) => f.name !== name)
        props.setFiles(newFiles)
    }

    useEffect(() => {
        if (!props.files) return
        setFiles(props.files)
    }, [props.files])

    return (
        <>
        <div className='create-offer-modal-box files'>
            <div className="offer-modal-text">Choose a preview for your offer</div>
            <div className="offer-modal-content preview">
                {!props.preview && <InputFile
                    label="Choose image"
                    img={imageIcon}
                    accept={'image/*'}
                    changeFunc={props.setPreview}
                    height={30} padding={30}
                />}
                {props.preview && <FileDisplayer
                    file={props.preview}
                    image
                    deleteFunc={() => props.setPreview(null)}
                    width={272} height={200}
                />}
            </div>

            <div className="offer-modal-text">You can attach up to 10 files (optional)</div>
            <div className="offer-modal-content attach">
                {!props.files?.length && <InputFile
                    label="Attach files"
                    img={attachIcon}
                    multiple
                    buttonClassName="empty"
                    changeFunc={props.setFiles}
                    height={30} padding={30}
                />}
                {!!files.length && <div className="files-list">
                    {files.map((file) => {
                        return <FileDisplayer
                            key={file.name}
                            file={file}
                            deleteFunc={() => removeFile(file.name)}
                            displayerWidth={272}
                        />}
                    )}
                </div>}
                
            </div>
        </div>
        <div className="button-panel">
            <Button className={'thin'} label={'Back'} onClick={props.backFunc} />
            {props.preview && <Button label={'Next'} onClick={props.nextFunc} />}
        </div>
        </>
    )
}

export default Files