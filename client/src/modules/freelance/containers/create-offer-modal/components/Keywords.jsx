import React, { useState } from 'react'
import Button from 'UI/button/Button'
import Input from 'UI/input/Input'
import closeIcon from 'assets/icons/white/add.svg'

const Keywords = (props) => {
    const [keywordInput, setKeywordInput] = useState('')

    const removeKeyword = (keyword) => {
        props.setKeywords([...props.keywords.filter((k) => k !== keyword)])
    }

    const addKeyword = () => {
        if (!keywordInput.length) return
        const newKeyword = keywordInput.toLowerCase().replaceAll(' ', '_')
        if (props.keywords.includes(newKeyword)) return
        props.setKeywords([...props.keywords, newKeyword])
        setKeywordInput('')
    }

    return (
        <>
        <div className='create-offer-modal-box keywords'>
            <div className="offer-modal-text">Add keywords to make your offer easier to find from the search query (optional)</div>
            <div className="offer-modal-content">
                <div className="add-panel">
                    <Input placeholder={'Keyword ...'} valueFunc={setKeywordInput} value={keywordInput} />
                    <Button className={'empty'} label={'Add'} onClick={addKeyword} />
                </div>
                <div className="keywords-box">
                    {props.keywords.map((keyword, i) => {
                        return <div className="keyword-item" key={i}>
                            <span>#{keyword}</span>
                            <img src={closeIcon} onClick={() => removeKeyword(keyword)} />
                        </div>
                    })}
                </div>
            </div>
        </div>
        <div className="button-panel">
            <Button className={'thin'} label={'Back'} onClick={props.backFunc} />
            <Button label={'Next'} onClick={props.nextFunc} />
        </div>
        </>
    )
}

export default Keywords