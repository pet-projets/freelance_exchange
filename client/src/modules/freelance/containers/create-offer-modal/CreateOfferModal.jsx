import React, { useContext, useEffect, useState } from 'react'
import Modal from 'UI/modal/Modal'
import Category from './components/Category'
import Tasks from './components/Tasks'
import Keywords from './components/Keywords'
import TextInfo from './components/TextInfo'
import './create-offer-modal.scss'
import Deadline from './components/Deadline'
import Price from './components/Price'
import useWallet from 'hooks/useWallet'
import { observer } from 'mobx-react-lite'
import { Context } from 'index'
import { createOffer } from 'modules/freelance/api/offerAPI'
import Files from './components/Files'
import {generateNumber} from 'utils/functions'
import { createFolder, uploadFile } from 'modules/_shared/api/fileAPI'
import Payment from './components/Payment'
import Alert from 'modules/_shared/containers/alert/Alert'

const CreateOfferModal = observer((props) => {
  const {categories, offers} = useContext(Context)
  const [isBlocked, setIsBlocked] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [activeBox, setActiveBox] = useState(0)

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [category, setCategory] = useState(null)
  const [keywords, setKeywords] = useState([])
  const [tasks, setTasks] = useState([])
  const [deadline, setDeadline] = useState(0)
  const [price, setPrice] = useState(0)

  const [preview, setPreview] = useState(null)
  const [files, setFiles] = useState([])

  const [alert, setAlert] = useState('')

  const {
    balance,
    transactionStatus,
    sendTo,
    clearTransactionStatus
  } = useWallet()

  useEffect(() => {
    if (!transactionStatus) return
    if (transactionStatus === 'success') {
      createOfferFunc()
    } else if (transactionStatus === 'failed') {
      setAlert(`Transaction failed`)
    }
    clearTransactionStatus()
  }, [transactionStatus])

  const boxTitles = [
    'Title & Desc', 'Preview & Files', 'Category', 'Keywords', 'Tasks', 'Deadline', 'Price'
  ]

  const setDeadlineFunc = (value) => {
    const newValue = Math.abs(parseInt(value)) || 0
    setDeadline(newValue)
  }
  const setPriceFunc = (value) => {
    const newValue = Math.abs(parseFloat(value)) || 0
    setPrice(newValue.toFixed(value.split('.')[1]?.length || 0))
  }

  const createContractFunc = () => {
    setIsBlocked(true)
    setIsLoading(true)
    sendTo(price)
  }

  const createOfferFunc = async () => {
    const offerId = generateNumber(10)
    let fileIds = []
    await createFolder(`/offers/${offerId}`)
    const metaFiles = [preview, ...files]
    for (let metaFile of metaFiles) {
      const {file: metaFileId} = await uploadFile(metaFile, `/offers/${offerId}`)
      fileIds.push(metaFileId)
    }
    let newCategory = null
    if (!category.isLast) {
      newCategory = categories.getItemByCode(category.longCode + '-0')
    } else newCategory = category
    const newKeywords = [...categories.getTopItemsByCode(newCategory.longCode, false).map(c => c.name.toLowerCase().replaceAll(' ', '_')), ...keywords]
    createOffer(parseFloat(price), newCategory._id, title, newKeywords, description, tasks, deadline, offerId, fileIds.slice(1), fileIds[0])
    .then((data) => {
      setIsLoading(false)
      offers.addItem(data.offer)
      setActiveBox(7)
      setIsBlocked(false)
    })
    .catch((err) => {
      setAlert(err.response.data.message)
    })
  }

  return (
    <>
    <Modal closeFunc={props.closeFunc} title={boxTitles[activeBox]} width={450} loading={isLoading} blockClose={isBlocked}>
      <div className="create-offer-modal">
        {activeBox === 0 && <TextInfo
          title={title}
          desc={description}
          setTitle={setTitle}
          setDesc={setDescription}
          nextFunc={() => setActiveBox(1)}
        /> }
        {activeBox === 1 && <Files
          preview={preview}
          files={files}
          setPreview={setPreview}
          setFiles={setFiles}
          backFunc={() => setActiveBox(0)}
          nextFunc={() => setActiveBox(2)}
        /> }
        {activeBox === 2 && <Category
          category={category}
          setCategory={setCategory}
          backFunc={() => setActiveBox(1)}
          nextFunc={() => setActiveBox(3)}
        /> }
        {activeBox === 3 && <Keywords
          keywords={keywords}
          setKeywords={setKeywords}
          backFunc={() => setActiveBox(2)}
          nextFunc={() => setActiveBox(4)}
        /> }
        {activeBox === 4 && <Tasks
          tasks={tasks}
          setTasks={setTasks}
          backFunc={() => setActiveBox(3)}
          nextFunc={() => setActiveBox(5)}
        /> }
        {activeBox === 5 && <Deadline
          deadline={deadline}
          setDeadline={setDeadlineFunc}
          backFunc={() => setActiveBox(4)}
          nextFunc={() => setActiveBox(6)}
        /> }
        {activeBox === 6 && <Price
          price={price}
          setPrice={setPriceFunc}
          balance={balance?.data?.formatted || 0}
          backFunc={() => setActiveBox(5)}
          nextFunc={createContractFunc}
        /> }
        {activeBox === 7 && <Payment closeFunc={props.closeFunc} /> }
      </div>
    </Modal>
    {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
    </>
  )
})

export default CreateOfferModal