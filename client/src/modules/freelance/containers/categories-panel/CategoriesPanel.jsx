import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useRef, useState } from 'react'
import { Context } from 'index'
import './categories-panel.scss'
import arrowIcon from 'assets/icons/white/arrow.svg'
import homeIcon from 'assets/icons/white/home.svg'
import homeFilledIcon from 'assets/icons/white/home-filled.svg'

const CategoriesPanel = observer((props) => {
    const {categories} = useContext(Context)
    const [code, setCode] = useState(props.initCode || '0')
    const [isBottomActive, setIsBottomActive] = useState(true)
    const panelLineRef = useRef(null)

    useEffect(() => {
        panelLineRef.current.scrollTo(panelLineRef.current.scrollWidth, 0)
        if (!props.func) return
        const currCategory = categories.getItemByCode(code)
        props.func(currCategory)
    }, [code])

    return (
        <div className='categories-panel'>

            <div className="top-panel panel">
                <div className="panel-line" ref={panelLineRef}>
                    <div className="top-panel-item panel-item home" onClick={() => setCode('0')}>
                        <img src={code === '0' ? homeFilledIcon : homeIcon} />
                    </div>
                    {categories.getTopItemsByCode(code).map((item) => {
                        return <div
                            key={item._id}
                            className={`top-panel-item panel-item next ${code === item.longCode ? 'active' : ''}`}
                            onClick={() => setCode(item.longCode)}
                        >
                            {item.name}
                        </div>
                    })}
                </div>
                {!!categories.getBottomItemsByCode(code).length && <div className="bottom-hider"
                    onClick={() => setIsBottomActive(!isBottomActive)}
                >
                    <img src={arrowIcon} style={{transform: `rotate(${isBottomActive ? '0' : '90deg'})`}} />
                </div>}
            </div>

            {isBottomActive && !!categories.getBottomItemsByCode(code, props.other).length && <div className="bottom-panel panel">
                {categories.getBottomItemsByCode(code, props.other).map((item) => {
                    return <div
                        key={item._id}
                        className="bottom-panel-item panel-item"
                        onClick={() => setCode(item.longCode)}
                    >
                        {item.name}
                    </div>
                })}
            </div>}

        </div>
    )
})

export default CategoriesPanel