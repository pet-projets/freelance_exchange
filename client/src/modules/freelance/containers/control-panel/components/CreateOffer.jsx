import React, { useState } from 'react'
import newOfferIcon from 'assets/icons/white/new-offer.svg'
import useWallet from 'hooks/useWallet'
import Button from 'UI/button/Button'
import CreateOfferModal from 'modules/freelance/containers/create-offer-modal/CreateOfferModal'

const CreateOffer = () => {
    const [isModal, setIsModal] = useState(false)

    const {isConnected} = useWallet()

    return (
        <>
        {isConnected && <div className="create-offer">
            <Button
                label={'New Offer'}
                img={newOfferIcon}
                style={{width: 120, height: 35, fontSize: 14, fontWeight: 500}}
                onClick={() => setIsModal(true)}
            />
            {isModal && <CreateOfferModal closeFunc={() => setIsModal(false)} /> }
        </div>}
        
        </>
    )
}

export default CreateOffer