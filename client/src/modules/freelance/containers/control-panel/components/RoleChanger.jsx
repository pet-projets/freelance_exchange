import React, {useState } from 'react'
import employerIcon from 'assets/icons/white/employer.svg'
import employeeIcon from 'assets/icons/white/employee.svg'
import { setRole } from 'modules/freelance/api/userAPI'
import Modal from 'UI/modal/Modal'
import Button from 'UI/button/Button'

const RoleChanger = (props) => {
    const [currRole, setCurrRole] = useState(props.role)
    const [active, setActive] = useState(false)

    const roles = {
        employer: {name: 'Employer', icon: employerIcon},
        employee: {name: 'Employee', icon: employeeIcon},
    }

    const setRoleFunc = (value) => {
        if (value === currRole || !value) {
            setActive(false)
            return
        }
        setRole(value)
        .then(() => {
            setCurrRole(value)
            props.changeRoleFunc(value)
        })
        .catch((err) => console.log(err.response.data.message))
        .finally(() => setActive(false))
        
    }

    return (
        <>
        {/* <div className='role-changer' onClick={() => setActive(!active)}>
            <img className='role-icon' src={roles[currRole]?.icon} />
            <div className='role-name'>{roles[currRole]?.name}</div>
        </div> */}
            <Button
            className={'empty'}
                label={roles[currRole]?.name}
                img={roles[currRole]?.icon}
                style={{width: 120, height: 35, fontSize: 14, fontWeight: 500}}
                onClick={() => setActive(!active)}
            />
        {active && <Modal
                title='Select your role'
                className={'role-changer-modal'}
                closeFunc={() => setActive(false)}
            >
                <div className="roles">
                    {Object.keys(roles).map((roleCode) => {
                        return <div
                            key={roleCode}
                            className={`role-item ${roleCode === currRole ? 'active' : ''}`}
                            onClick={() => setRoleFunc(roleCode)}
                        >
                            <div className="item-side">
                                <img src={roles[roleCode]?.icon} />
                                <span>{roles[roleCode]?.name}</span>
                            </div>
                            <div className="item-side">
                                <div className="dot"></div>
                            </div>
                        </div>
                    })}
                </div>
            </Modal>}
        </>
    )
}

export default RoleChanger