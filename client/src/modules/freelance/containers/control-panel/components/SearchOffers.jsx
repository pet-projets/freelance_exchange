import { Context } from 'index'
import { observer } from 'mobx-react-lite'
import React, { useContext, useState } from 'react'
import Search from 'UI/search/Search'
import EmptySign from 'UI/empty-sign/EmptySign'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'
import { useLocation, useNavigate } from 'react-router-dom'
const SearchOffers = observer(() => {
    const {offers} = useContext(Context)
    const [active, setActive] = useState(false)
    const [itemActive, setItemActive] = useState(0)
    const [isMouseOver, setIsMouseOver] = useState(false)
    const location = useLocation(null)
    const navigate = useNavigate(null)
    const [items, setItems] = useState([])

    const openFunc = (id) => {
        let currSearchParams = new URLSearchParams(location.search)
        currSearchParams.append('o', id.toString())
        navigate(location.pathname + '?' + currSearchParams.toString())
    }

    const searchFunc = (value) => {
        const tempItems = offers.searchOffers(value)
        setItems(tempItems)
    }

    return (
        <div className="search-offers">
            <Search
                placeholder={"Search offers ..."}
                searchFunc={searchFunc}
                isMouseOver={isMouseOver}
                setActive={setActive}
                clearItemActive={() => setItemActive(0)}
                width={450}
                autoclose
            >
                {active && <div
                    className="search-list"
                    onMouseOver={() => setIsMouseOver(true) }
                    onMouseLeave={() => setIsMouseOver(false) }
                >
                    {items.map((item, index) => {
                        return <div
                            key={item._id}
                            onMouseOver={() => setItemActive(index)}
                            className={`search-list-item ${index === itemActive ? 'active' : ''}`}
                            onClick={() => openFunc(item._id)}
                        >
                            <div className='name'>
                                <FileDisplayer
                                    previewOnly
                                    fileModel
                                    image
                                    file={item?.preview}
                                    width={35} height={35}
                                />
                                <span>{item.title}</span>
                            </div>
                            <div className='price'>
                                {item.price} ETH
                            </div>
                        </div>
                    })}
                    {!items.length && <div className="search-empty-list">
                        <EmptySign />
                    </div> }
                </div>}
            </Search>
        </div>
    )
})

export default SearchOffers