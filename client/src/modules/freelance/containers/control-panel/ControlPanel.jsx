import { Context } from 'index'
import { observer } from 'mobx-react-lite'
import React, { useContext } from 'react'
import Button from 'UI/button/Button'
import CreateOffer from './components/CreateOffer'
import RoleChanger from './components/RoleChanger'
import offerIcon from 'assets/icons/white/offer.svg'
import './control-panel.scss'
import { useNavigate } from 'react-router-dom'
import { OFFERS_ROUTE } from 'routes/routesPath'
import SearchOffers from './components/SearchOffers'

const ControlPanel = observer(() => {
    const {user} = useContext(Context)
    const navigate = useNavigate()

    const changeRole = (role) => {
        user.setRole(role)
    }

    return (
        <div className='control-panel'>
            <div className="side side-left">
                <SearchOffers />
            </div>
            <div className="side side-right">
                {user.isAuth && user.info.status !== 'admin' &&
                    <>
                    <RoleChanger role={user.role} changeRoleFunc={changeRole} />
                    <Button
                        className={'empty'}
                        label={'My Offers'}
                        img={offerIcon}
                        style={{width: 120, height: 35, fontSize: 14, fontWeight: 500}}
                        onClick={() => navigate(OFFERS_ROUTE)}
                    />
                    </>
                }
                {user.isAuth && user.role === 'employer' && <CreateOffer />}
            </div>
        </div>
    )
})

export default ControlPanel