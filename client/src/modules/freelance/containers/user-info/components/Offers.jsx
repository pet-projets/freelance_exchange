import React from 'react'
import StarsDisplayer from 'UI/stars/StarsDisplayer'
import starIcon from 'assets/icons/yellow/star.svg'

const Offers = (props) => {

    return (
        <div className='user-info-ratings'>
            <div className="user-info-ratings-panel">
                <img src={starIcon} />
                <span>{props.ratings.length ? (props.ratings?.reduce((pr, r) => pr + r.value, 0) / props.ratings?.length).toFixed(2) : '--'} </span>
                <span>{`(${props.ratings.length})`}</span>
            </div>
            {props.ratings.sort((a, b) => b.value - a.value).map((rating) => {
                return <div className="rating" key={rating._id}>
                    <StarsDisplayer value={rating.value} />
                    <div className="rating-title">{rating.title}</div>
                    <div className="rating-comment">{rating.comment}</div>
                </div>
            })}
        </div>
    )
}

export default Offers