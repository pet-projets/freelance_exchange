import React, { useEffect, useState } from 'react'
import Button from 'UI/button/Button'
import addIcon from 'assets/icons/white/add.svg'
import CategoriesPanel from '../../categories-panel/CategoriesPanel'
import InputFile from 'UI/input-file/InputFile'
import deleteIcon from 'assets/icons/white/delete.svg'
import downloadIcon from 'assets/icons/white/download.svg'

const General = (props) => {
    const [isCategoryAdd, setIsCategoryAdd] = useState(false)
    const [currCategory, setCurrCategory] = useState(null)

    const addCategory = () => {
        if (!currCategory || props.categories.some((item) => item._id === currCategory._id)) return
        props.setCategoriesFunc([...props.categories, currCategory])
        setIsCategoryAdd(false)
    }
    const removeCategory = (longCode) => {
        props.setCategoriesFunc([...props.categories.filter((item) => item.longCode !== longCode)])
    }

    return (
        <div className='general-info'>

            {(props.self || !!props.categories?.length) && <div className="categories-info">
                <div className="info-label">Directions of activity</div>
                <div className="categories-list">
                    {props.categories.map((item) => {
                        return <div key={item._id} className="category-item">
                            <span>{item.name}</span>
                            {props.self && <img src={addIcon} onClick={() => removeCategory(item.longCode)} />}
                        </div>
                    })}
                </div>
                {props.self && !isCategoryAdd && <Button
                    className={'empty'}
                    label={'ADD'}
                    img={addIcon}
                    style={{height: 35, width: '100%'}}
                    onClick={() => setIsCategoryAdd(true)}
                />}
                {isCategoryAdd && <div className="add-category-panel">
                    <div className="active-category-panel">
                        <div className="active-category-name">{currCategory?.name || ''}</div>
                        <div className="add-category-buttons">
                            <Button className={'thin'} label='Back' onClick={() => setIsCategoryAdd(false)} />
                            <Button className={'empty'} label='Add' onClick={addCategory} />
                        </div>
                    </div>
                    <CategoriesPanel func={setCurrCategory} />
                </div>}
            </div>}

            {(props.self || props.cv) && <div className="cv-info">
                <div className="info-label">CV</div>
                <div className="cv-panel">
                    {(!props.cv && !props.currCV || props.currCV === undefined) && props.self && <InputFile
                        label={'Add'}
                        accept={'.pdf'}
                        changeFunc={props.setCVFunc}
                        height={30}
                        padding={30}
                    /> }
                    {props.cv && props.currCV !== undefined && <Button
                        img={downloadIcon}
                        onClick={props.downloadCV}
                    /> }
                    {(props.cv && props.currCV !== undefined || !!props.currCV) && props.self && <Button
                        className="empty"
                        img={deleteIcon}
                        onClick={() => props.setCVFunc(undefined)}
                    /> }
                </div>
            </div>}

            
        </div>
    )
}

export default General