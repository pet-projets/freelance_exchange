import React, { useContext, useEffect } from 'react'
import './user-info-freelance.scss'
import General from './components/General'
import Offers from './components/Offers'
import { downloadFile } from 'modules/_shared/api/fileAPI'
import { observer } from 'mobx-react-lite'
import { Context } from 'index'

const UserInfoFreelance = observer((props) => {
    const {user} = useContext(Context)

    useEffect(() => {
        if (!props.self) return
        user.setFetchType('categories')
    }, [])

    const downloadCV = () => {
        if (!props.info.cv) return
        downloadFile(props.info.cv)
        .then((data) => {
            window.open(data.link)

        })
        .catch((err) => console.log(err.response.data.message))
    }

    return (
        <div className="user-info-freelance">
            <General
                categories={props.newCategories || props.info.categories}
                cv={props.info.cv}
                currCV={props.newCV}
                self={props.self}
                setCategoriesFunc={props.setNewCategories}
                setCVFunc={props.setNewCV}
                downloadCV={downloadCV}
            />
            {!!props.info.ratings.lenght && <Offers
                ratings={props.info.ratings}
            />}
        </div>
        
    )
})

export default UserInfoFreelance