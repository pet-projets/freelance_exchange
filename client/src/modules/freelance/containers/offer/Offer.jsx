import React from 'react'
import Button from 'UI/button/Button'
import './offer.scss'
import ethereumIcon from 'assets/icons/base/ethereum.svg'
import { useNavigate } from 'react-router-dom'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'

const Offer = (props) => {
    const navigate = useNavigate(null)

    const openOffer = () => {

        navigate(props.route + 'o=' + props.item._id)
    }

    return (
        <div className='offer' onClick={openOffer}>
            {/* <div className="offer-preview">
                {props.item?.preview && <FileDisplayer
                    previewOnly
                    fileModel
                    image
                    file={props.item?.preview}
                    width={275} height={200}
                /> }
            </div> */}
            <div className="offer-body">
                <div className="offer-title">{props.item?.title}</div>
                <div className="offer-keywords">
                    {props.item?.keywords.map((keyword, i) => {
                        return <div className="offer-keyword" key={i}>
                            <span>#{keyword}</span>
                        </div>
                    })}
                </div>
                <div className="offer-button">
                    <div className="offer-price">
                        <img src={ethereumIcon} />
                        <span>{props.item?.price}</span>
                    </div>
                    <Button label={'More'} onClick={() => {}} />
                </div>
            </div>
        </div>
        
    )
}

export default Offer