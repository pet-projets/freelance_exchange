import React, { useState } from 'react'
import ModalOffer from '../modal-offer/ModalOffer'
import Fullscreen from 'UI/fullscreen/Fullscreen'
import UserInfo from '../user-info/UserInfoFreelance'

const ModalOfferContainer = () => {
    const [fullscreenImage, setFullscreenImage] = useState(null)
    const [fullscreenVideo, setFullscreenVideo] = useState(null)

    const [userInfoId, setUserInfoId] = useState(0)

    return (
        <>
        <ModalOffer
            fullscreenImageFunc={setFullscreenImage}
            fullscreenVideoFunc={setFullscreenVideo}
            userInfoFunc={setUserInfoId}
        />
        {(fullscreenImage || fullscreenVideo) && <Fullscreen image={fullscreenImage} video={fullscreenVideo} closeFunc={() => {
            setFullscreenImage(null)
            setFullscreenVideo(null)
        }} />}
        {!!userInfoId && <UserInfo
            userId={userInfoId}
            closeFunc={() => setUserInfoId(0)}
        />}
        </>
    )
}

export default ModalOfferContainer