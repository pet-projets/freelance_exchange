import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Context } from 'index'
import OffersPageItemsPanel from './components/OffersPageItemsPanel'
import './offers.scss'
import fixIcon from 'assets/icons/white/fix.svg'
import timeIcon from 'assets/icons/white/time.svg'
import pauseIcon from 'assets/icons/white/pause.svg'
import Button from 'UI/button/Button'
import CreateOfferModal from 'modules/freelance/containers/create-offer-modal/CreateOfferModal'
import { useNavigate } from 'react-router-dom'
import { FREELANCE_ROUTE } from 'routes/routesPath'
import ModalOfferContainer from 'modules/freelance/containers/modal-offer-container/ModalOfferContainer'

const Offers = observer(() => {
    const {offers, user} = useContext(Context)
    const [isCreate, setIsCreate] = useState(false)
    const navigate = useNavigate(null)

    useEffect(() => {
        user.setFetchType('freelance')
    }, [])

    const panels = [
        {title: 'In Process', key: 'develop', icon: fixIcon},
        {title: 'Pending', key: 'pending', icon: timeIcon},
        {title: 'Paused', key: 'paused', icon: pauseIcon}
    ]

    return (
        <div className='offers page'>
            
            <div className="offers-page-container">
                {panels.map((panel) => {
                    return <OffersPageItemsPanel
                        key={panel.key}
                        icon={panel.icon}
                        title={panel.title}
                        items={offers.getRelatedItems(user.role, panel.key, user.info.id).reverse()}
                    />
                })}
                {!offers.isRelated(user.info.id, user.role) && <div className="offers-page-empty-part">
                    <div className="empty-part-content">
                        {user.role === 'employer' && <>
                            <span>You currently have no offers</span>
                            <Button label='Create Offer' onClick={() => setIsCreate(true)} />
                        </>}
                        {user.role === 'employee' && <>
                            <span>You currently have no offers</span>
                            <Button label='Show Offers' onClick={() => navigate(FREELANCE_ROUTE)} />
                        </>}
                    </div>
                </div> }
            </div>
            
            {isCreate && <CreateOfferModal closeFunc={() => setIsCreate(false)} />}
            <ModalOfferContainer />
        </div>
    )
})

export default Offers