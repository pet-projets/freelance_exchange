import React from 'react'
import Offer from 'modules/freelance/containers/offer/Offer'
import { OFFERS_ROUTE } from 'routes/routesPath'

const OffersPageItemsPanel = (props) => {

    

    return (
        <>
        {!!props.items.length && <div className="offers-page-items-panel">
            <div className="side side-top">
                <div className="category-title">
                    <img src={props.icon} />
                    <span>{props.title}</span>
                </div>
            </div>
            <div className="side side-bottom offers-container">
                {props.items?.map((item) => {
                    return <Offer
                        key={item._id}
                        item={item}
                        route={OFFERS_ROUTE + '?'}
                    />
                })}
            </div>
        </div>}
        </>
        
    )
}

export default OffersPageItemsPanel