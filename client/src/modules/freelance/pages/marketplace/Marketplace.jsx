import React, { useContext, useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import MainPage from './components/MainPage'
import './marketplace.scss'
import CategoryPage from './components/CategoryPage'
import ModalOfferContainer from 'modules/freelance/containers/modal-offer-container/ModalOfferContainer'
import { observer } from 'mobx-react-lite'
import { Context } from 'index'

const Marketplace = observer(() => {
    const {user} = useContext(Context)
    const locationSearch = useLocation(null).search
    const [currCategoryId, setCurrCatedoryId] = useState(null)

    useEffect(() => {
        user.setFetchType('freelance')
    }, [])

    useEffect(() => {
        const tempCategoryId = new URLSearchParams(locationSearch)
        setCurrCatedoryId(tempCategoryId.get('c'))
    }, [locationSearch])

    return (
        <div className='marketplace page'>
            
            {currCategoryId === null && <MainPage />}
            {currCategoryId !== null && <CategoryPage /> }

            <ModalOfferContainer />
        </div>
    )
})

export default Marketplace