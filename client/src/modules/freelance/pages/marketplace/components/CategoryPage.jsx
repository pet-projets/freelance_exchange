import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { Context } from 'index'
import CategoriesPanel from 'modules/freelance/containers/categories-panel/CategoriesPanel'
import Offer from 'modules/freelance/containers/offer/Offer'
import { FREELANCE_ROUTE } from 'routes/routesPath'
import EmptySign from 'UI/empty-sign/EmptySign'

const CategoryPage = observer(() => {
    const {categories, offers} = useContext(Context)
    const locationSearch = useLocation(null).search
    const navigate = useNavigate(null)

    const [currCategory, setCurrCategory] = useState(null)
    const [currOffers, setCurrOffers] = useState(null)

    useEffect(() => {
        const cId = new URLSearchParams(locationSearch).get('c')
        let tempCategory = null
        if (!cId) {
            tempCategory = {
                name: 'All', longCode: '0'
            }
        } else tempCategory = categories.getItemById(cId)
        setCurrCategory(tempCategory)
        if (!tempCategory) {
            setCurrOffers(offers.items)
        } else {
            const tempOffers = offers.getItemsByCategoryIds(categories.getSubcategories(tempCategory.longCode).map((sub) => sub._id))
            setCurrOffers(tempOffers)
        }
    }, [locationSearch])

    const switchCategory = (category) => {
        navigate(FREELANCE_ROUTE + '?c=' + (category?._id || ''))
    }

    return (
        <div className='category-page'>
            {currOffers && <>
            <div className="side side-top">
                <CategoriesPanel
                    initCode={currCategory?.longCode}
                    func={switchCategory}
                    other
                />
            </div>
            <div className="category-title">{currCategory?.name || 'All'} ({currOffers?.length || 0})</div>
            <div className="side offers-container side-bottom">
                {currOffers.sort((a, b) => b.price - a.price).map((item) => {
                    return <Offer key={item._id} item={item} route={FREELANCE_ROUTE + locationSearch + '&'} />
                })}
                {!currOffers.length && <EmptySign />}
            </div>
            </>}
            
        </div>
    )
})

export default CategoryPage