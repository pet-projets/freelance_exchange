import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Context } from 'index'
import MainPageItemsPanel from './MainPageItemsPanel'
import ControlPanel from 'modules/freelance/containers/control-panel/ControlPanel'

const MainPage = observer(() => {
    const {categories, offers, user} = useContext(Context)
    const [categoryOffers, setCategoryOffers] = useState([])

    useEffect(() => {
        if (!categories && !offers) return
        let tempArray = []
        categories.getBottomItemsByCode('0', true).forEach((category) => {
            const tempOffers = offers.getItemsByCategoryIds(categories.getSubcategories(category.longCode).map((sub) => sub._id))
            tempArray.push({category, offers: tempOffers})
        })
        // tempArray = tempArray.filter((ta) => !!ta.offers.length).sort((a, b) => b.offers.length - a.offers.length)
        tempArray = tempArray.sort((a, b) => b.offers.length - a.offers.length)
        setCategoryOffers(tempArray)
    }, [categories?.items, offers?.items])

    return (
        <div className='main-page'>
            {/* <Banner /> */}
            <div className="main-page-content">
                <div className="side side-top">
                    <ControlPanel />
                </div>
                {categoryOffers.map((co) => {
                    return <MainPageItemsPanel
                        key={co.category._id}
                        category={co.category}
                        items={co.offers}
                    />
                })}
            </div>
        </div>
    )
})

export default MainPage