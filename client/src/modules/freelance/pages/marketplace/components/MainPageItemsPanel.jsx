import React from 'react'
import { useNavigate } from 'react-router-dom'
import Offer from 'modules/freelance/containers/offer/Offer'
import { FREELANCE_ROUTE } from 'routes/routesPath'
import Button from 'UI/button/Button'
import EmptySign from 'UI/empty-sign/EmptySign'

const MainPageItemsPanel = (props) => {
    const navigate = useNavigate(null)

    const showMoreFunc = () => {
        navigate(FREELANCE_ROUTE + '?c=' + props.category?._id)
    }

    return (
        <div className='main-page-items-panel'>
            <div className="side-top side">
                <div className="category-title">{props.category?.name} ({props.items?.length})</div>
                {props.items.length > 10 && <Button className={'outline'} label={'More'} onClick={showMoreFunc} />}
            </div>
            <div className="side-bottom offers-container side">
                {props.items?.sort((a, b) => b.price - a.price).slice(0, 10).map((item) => {
                    return <Offer key={item._id} item={item} route={FREELANCE_ROUTE + '?'} />
                })}
                {!props.items?.length && <EmptySign bgc={'var(--trans-color)'} />}
            </div>
        </div>
    )
}

export default MainPageItemsPanel