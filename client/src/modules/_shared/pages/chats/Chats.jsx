import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Context } from 'index'
import ChatsPanel from './components/ChatsPanel'
import './chats.scss'
import { useLocation } from 'react-router-dom'
import Loader from 'UI/loader/Loader'
import Chat from 'modules/_shared/containers/chat/Chat'
import ModalOfferContainer from 'modules/freelance/containers/modal-offer-container/ModalOfferContainer'

const Chats = observer(() => {
    const {chats, user} = useContext(Context)
    const locationSearch = useLocation().search
    const [chatsItems, setChatsItems] = useState([])
    const [chatId, setChatId] = useState(null)
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        setChatsItems(chats.getItems(user.info.status))
    }, [chats.items, user.role])

    useEffect(() => {
        const chatIdNew = new URLSearchParams(locationSearch).get('ch')
        if (chatId === chatIdNew) return
        if (!chatIdNew) {
            setChatId(null)
            return
        }
        setChatId(chatIdNew)
    }, [locationSearch])

    return (
        <>
        <div className='chats page big'>

            <ChatsPanel
                items={chatsItems}
                userId={user.info.id}
                chatId={chatId}
            />

            <Chat
                chat={chatsItems.find((item) => item._id === chatId)}
                isLoading={isLoading}
                setIsLoading={setIsLoading}
            />

            {isLoading && <div className="load-wrapper">
                <Loader scale={1.25} />
            </div>}
            {!chatId && !isLoading && <div className="empty-wrapper">
                <span>No messages here ...</span>
            </div> }

        </div>
        <ModalOfferContainer />
        </>
        
    )
})

export default Chats