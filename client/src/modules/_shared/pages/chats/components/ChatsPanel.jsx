import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { CHATS_ROUTE } from 'routes/routesPath'
import Avatar from 'UI/avatar/Avatar'
import { formatTimestamp } from 'utils/functions'

const ChatsPanel = (props) => {
    const [chats, setChats] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        if (!props.items) {
            setChats([])
            return
        }
        setChats([...props.items])
    }, [props.items])

    return (
        <div className='chats-panel'>
            {chats.sort((a, b) => (b.latestMessage?.timestamp || 0) - (a.latestMessage?.timestamp || 0)).map((item) => {
                const isUserlatestMessage = props.userId === item.latestMessage?.senderId
                return <div
                    className={`chat-item ${props.chatId === item._id ? 'active' : ''}`}
                    key={item._id}
                    onClick={() => navigate(CHATS_ROUTE + '?ch=' + item._id)}
                >
                    <Avatar avatar={item.chater?.avatar} id={item.chater?.id} size={50} online />
                    <div className="chat-body">
                        <div className="chat-info-panel">
                            <div className="chat-title">{item.chater?.username}</div>
                            <div className="chat-timestamp">{formatTimestamp(item.latestMessage?.timestamp)}</div>
                        </div>
                        <div className="chat-last-message">
                            {isUserlatestMessage && <div className="sender">You:</div>}
                            <div className="chat-message-text">{!!item.latestMessage ? (!!item.latestMessage?.files?.length ? 'File' : item.latestMessage?.text) : 'No messages'}</div>
                            {!!item.unreadCount && <div className={`chat-unreads ${isUserlatestMessage ? 'small' : ''}`}>
                                <span>{isUserlatestMessage ? '' : item.unreadCount}</span>
                            </div>}
                        </div>
                    </div>
                </div>
            })}
        </div>
    )
}

export default ChatsPanel