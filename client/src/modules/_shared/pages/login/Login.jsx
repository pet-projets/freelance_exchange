import React, { useState } from 'react'
import Box from 'UI/box/Box'
import Input from 'UI/input/Input'
import Password from 'UI/password/Password'
import { login, sendEmail } from 'modules/_shared/api/userAPI'
import Button from 'UI/button/Button'
import { useNavigate } from 'react-router-dom'
import { REGISTRATION_ROUTE } from 'routes/routesPath'
import Alert from 'modules/_shared/containers/alert/Alert'

const Login = () => {
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [isSent, setIsSent] = useState(false)
    const [isForgot, setIsForgot] = useState(false)
    const [code, setCode] = useState('')
    const navigate = useNavigate(null)
    const [alert, setAlert] = useState('')

    const sendEmailFunc = () => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/g
        if (!emailRegex.test(email)) {
            setAlert('Invalid email')
            return
        }
        sendEmail(email)
        .then(() => {
            setIsSent(true)
            setIsForgot(false)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    const loginFunc = () => {
        if (isSent ? (!email || !code) : (!username || !password)) {
            setAlert('Fill all fields')
            return
        }
        login(username, password, email, code)
        .then(() => {
            navigate(0)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    return (
        <>
        <div className='authorization page'>
            {!isSent && !isForgot && <Box title={'Login'} width={750}>
                <Input placeholder='Username' valueFunc={setUsername} />
                <Password placeholder='Password' valueFunc={setPassword} />
                <div className="buttons-panel">
                    <div className="thin-buttons">
                        <Button label='Sign Up' className='thin' onClick={() => navigate(REGISTRATION_ROUTE)}/>
                        <Button label='Forgot password' className='thin' onClick={() => setIsForgot(true)}/>
                    </div>
                    <Button label='Submit' onClick={loginFunc} />
                </div>
            </Box>}
            {isForgot && <Box title={'Confirm email'} width={750}>
                <Input placeholder='Email' valueFunc={setEmail} />
                <div className="buttons-panel">
                    <Button label='Back' className='thin' onClick={() => setIsForgot(false)}/>
                    <Button label='Submit' onClick={sendEmailFunc}/>
                </div>
            </Box>}
            {isSent && <Box title={'Confirm email'} width={750}>
                <div className="warning">
                    <span>Confirmation email may be in Spam section</span>
                </div>
                <Password placeholder='Code' valueFunc={setCode} />
                <div className="buttons-panel">
                    <Button label='Back' className='thin' onClick={() => setIsSent(false)}/>
                    <Button label='Submit' onClick={loginFunc}/>
                </div>
            </Box>}
        </div>
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
}

export default Login