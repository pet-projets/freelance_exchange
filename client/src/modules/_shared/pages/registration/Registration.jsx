import React, { useState } from 'react'
import Box from 'UI/box/Box'
import Input from 'UI/input/Input'
import Password from 'UI/password/Password'
import Switcher from 'UI/switcher/Switcher'
import employerIcon from 'assets/icons/white/employer.svg'
import employeeIcon from 'assets/icons/white/employee.svg'
import './registration.scss'
import { registration, sendEmail } from 'modules/_shared/api/userAPI'
import Button from 'UI/button/Button'
import { useNavigate } from 'react-router-dom'
import { LOGIN_ROUTE } from 'routes/routesPath'
import Alert from 'modules/_shared/containers/alert/Alert'

const Registration = () => {
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('employer')
    const [isSent, setIsSent] = useState(false)
    const [code, setCode] = useState('')
    const navigate = useNavigate(null)
    const [alert, setAlert] = useState('')

    const doValidation = (isCode=false) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/g
        const regExsp =  /^[A-Za-z]([A-Za-z0-9_.-]+)$/g
        if (!emailRegex.test(email)) {
            return { success: false, message: 'Invalid email' }
        } 
        if (!username.match(regExsp) || (username.length > 50 || username.length < 4)) {
            return { success: false, message: 'Invalid username' }
        }
        if (password.length < 8 || password.length > 20) {
            return { success: false, message: 'Password must be in range 8-20 symbols' }
        }
        if (isCode && code.length !== 8) {
            return { success: false, message: 'Invalid code' }
        }
        return { success: true }
    }

    const sendEmailFunc = () => {
        const validationStatus = doValidation()
        if (!validationStatus.success) {
            setAlert(validationStatus.message)
            return
        }
        sendEmail(email)
        .then(() => {
            setIsSent(true)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    const registerFunc = () => {
        const validationStatus = doValidation(true)
        if (!validationStatus.success) {
            setAlert(validationStatus.message)
            return
        }
        registration(username, password, email, code, role)
        .then(() => {
            navigate(0)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    const roleItems = [
        {name: 'Employer', icon: employerIcon, code: 'employer'},
        {name: 'Employee', icon: employeeIcon, code: 'employee'}
    ]

    const changeRole = (roleIndex) => {
        setRole(roleItems[roleIndex].code)
    }

    return (
        <>
        <div className='authorization page'>
            {!isSent && <Box title={'Registration'} width={750}>
                {/* <div className="role-choise">
                    <span>Choose your role</span>
                    <Switcher items={roleItems} func={changeRole}/>
                </div> */}
                <Input placeholder='Email' valueFunc={setEmail} />
                <Input placeholder='Username' valueFunc={setUsername} />
                <Password placeholder='Password' valueFunc={setPassword} />
                <div className="buttons-panel">
                    <Button label='Sign In' className='thin' onClick={() => navigate(LOGIN_ROUTE)}/>
                    <Button label='Send code' onClick={sendEmailFunc} />
                </div>
            </Box>}

            {isSent && <Box title={'Confirm email'} width={750}>
                <div className="warning">
                    <span>Confirmation email may be in Spam section</span>
                </div>
                <Password placeholder='Code' valueFunc={setCode} />
                <div className="buttons-panel">
                    <Button label='Back' className='thin' onClick={() => setIsSent(false)}/>
                    <Button label='Submit' onClick={registerFunc}/>
                </div>
            </Box>}
        </div>
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
}

export default Registration