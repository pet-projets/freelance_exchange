import React, { useContext, useState } from 'react'
import Button from 'UI/button/Button'
import Modal from 'UI/modal/Modal'
import CategoriesPanel from 'modules/_shared/containers/categories-panel/CategoriesPanel'
import Input from 'UI/input/Input'
import { observer } from 'mobx-react-lite'
import { Context } from 'index'
import Alert from 'modules/_shared/containers/alert/Alert'
import { createCategory, deleteCategory, editCategory, getAllCategories } from '../../../api/offerAPI'

const CategoriesBlock = observer(() => {
    const {categories} = useContext(Context)
    const [modal, setModal] = useState(-1)

    const [name, setName] = useState('')
    const [categoryId, setCategoryId] = useState('')

    const [alert, setAlert] = useState('')

    const changeCategory = (category) => {
        setCategoryId(category?._id || '')
        if (modal === 2) setName(category?.name || '')
    }

    const createFunc = () => {
        if (!name) {
            setAlert('Fill all fields')
            return
        }
        createCategory(categoryId, name)
        .then(() => {
            getAllCategories()
            .then((data) => {
                categories.setItems(data.categories)
                setModal(-1)
            })
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    const editFunc = () => {
        if (!name || !categoryId) {
            setAlert('Fill all fields')
            return
        }
        editCategory(categoryId, name)
        .then(() => {
            getAllCategories()
            .then((data) => {
                categories.setItems(data.categories)
                setModal(-1)
            })
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    const deleteFunc = () => {
        if (!categoryId) {
            setAlert('Fill all fields')
            return
        }
        deleteCategory(categoryId)
        .then(() => {
            getAllCategories()
            .then((data) => {
                categories.setItems(data.categories)
                setModal(-1)
            })
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }

    const categoriesConfig = [
        { title: 'Create Category', label: 'Create', func: createFunc, placeholder: 'Category name ...' },
        { title: 'Edit Category', label: 'Edit', func: editFunc, placeholder: 'New category name ...' },
        { title: 'Delete Category', label: 'Delete', func: deleteFunc },
    ]

    return (
        <>
        <div className='field categories'>
            <span>Categories</span>
            <div className="category-buttons">
                <Button
                    label='Delete'
                    className='thin'
                    onClick={() => setModal(2)}
                    style={{height: 30, padding: `0 15px`}}
                />
                <Button
                    label='Edit'
                    className='thin'
                    onClick={() => setModal(1)}
                    style={{height: 30, padding: `0 15px`}}
                />
                <Button
                    label='Create'
                    className='thin'
                    onClick={() => setModal(0)}
                    style={{height: 30, padding: `0 15px`}}
                />
            </div>
        </div>
        {modal >= 0 && <Modal
            title={categoriesConfig[modal].title}
            closeFunc={() => setModal(-1)}
            width={500}
        >
            <div className="field">
                <Input
                    placeholder={categoriesConfig[modal].placeholder}
                    value={name}
                    valueFunc={setName}
                    readOnly={modal === 2}
                />
            </div>
            <CategoriesPanel func={changeCategory} />
            <div className="button-panel">
                <Button
                    label={categoriesConfig[modal].label}
                    onClick={categoriesConfig[modal].func}
                />
            </div>
        </Modal>}
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
    )
})

export default CategoriesBlock