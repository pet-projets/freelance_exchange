import { parseEther } from 'ethers'
import React, { useEffect, useState } from 'react'
import { useContractWrite } from 'wagmi'
import { getTickets, removeTickets } from 'modules/_shared/api/ticketAPI'
import useWallet from 'hooks/useWallet'
import Button from 'UI/button/Button'
import { walletConfig } from 'utils/walletUtils/walletConfig'
import Alert from 'modules/_shared/containers/alert/Alert'

const TicketsBlock = () => {
    const {address} = useWallet()
    const [tickets, setTickets] = useState([])
    const [alert, setAlert] = useState('')

    const { writeAsync: sendMoney } = useContractWrite({
        address: walletConfig[process.env.REACT_APP_TYPE].address,
        abi: walletConfig[process.env.REACT_APP_TYPE].abi,
        functionName: 'sendToMany',
        chainId: walletConfig[process.env.REACT_APP_TYPE].chain
    })

    const sendFunc = () => {
        if (!tickets.length) return
        const addresses = tickets.map((t) => t.wallet)
        const values = tickets.map((t) => parseEther(`${((t.price * .9) - 0).toFixed(10)}`))
        sendMoney({
            args: [addresses, values]
        })
        .then(() => {
            removeTickets(tickets.map((t) => t._id))
            .then(() => {
                setTickets([])
            })
            .catch((err) => setAlert(err.response.data.message))
        })
        .catch((err) => setAlert(err.response.data.message))
    }

    useEffect(() => {
        getTickets()
        .then((data) => {
            setTickets(data.tickets)
        })
        .catch((err) => setAlert(err.response.data.message))
    }, [])

    return (
        <>
        <div className='field tickets'>
            <span>Tickets</span>
            <div className="tickets-content">
                <div className="tickets-info">{tickets.length} - ${tickets.reduce((a, b) => a + ((b.price * .9) - 0), 0).toFixed(4)}</div>
                {address === process.env.REACT_APP_WALLET && <Button label='Send' onClick={sendFunc} />}
            </div>
        </div>
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
}

export default TicketsBlock