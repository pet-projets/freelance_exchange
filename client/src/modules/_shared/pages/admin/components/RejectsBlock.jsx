import React, { useContext, useEffect, useState } from 'react'
import { getRejectedOffers, reviewRejectedOffer } from '../../../api/offerAPI'
import Button from '../../../UI/button/Button'
import Modal from '../../../UI/modal/Modal'
import chatIcon from '../../../assets/menu/chats.svg'
import employerIcon from '../../../assets/icons/white/employer.svg'
import employeeIcon from '../../../assets/icons/white/employee.svg'
import Chat from '../../../containers/chat/Chat'
import { getSupportChat } from '../../../api/chatAPI'
import { useLocation, useNavigate } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import { Context } from '../../..'
import { CHATS_ROUTE } from '../../../routes/routesPath'

const RejectsBlock = observer(() => {
    const {chats} = useContext(Context)
    const location = useLocation(null)
    const navigate = useNavigate(null)
    const [active, setActive] = useState(false)
    const [loading, setLoading] = useState(false)
    const [offers, setOffers] = useState([])

    useEffect(() => {
        if (!active || loading) return
        setLoading(true)
        getRejectedOffers()
        .then((data) => {
            setOffers(data.offers)
        })
        .catch((err) => console.log(err.response.data.message))
        .finally(() => setLoading(false))
    }, [active])

    const openFunc = (id) => {
        let currSearchParams = new URLSearchParams(location.search)
        currSearchParams.append('o', id.toString())
        navigate(location.pathname + '?' + currSearchParams.toString())
    }

    const toChatFunc = (id) => {
        const chatId = chats.getSupportItem(id)?._id
        if (!chatId) return
        navigate(CHATS_ROUTE + '?ch=' + chatId)
    }

    const reviewFunc = (id, result) => {
        setLoading(true)
        reviewRejectedOffer(result, id)
        .finally(() => {
            setActive(false)
            setLoading(false)
        })
    }

    return (
        <>
        <div className='field rejects'>
            <span>Rejects</span>
            <div className="rejects-content">
                <Button className='thin' label='Show' onClick={() => setActive(true)} />
            </div>
        </div>
        {active && <Modal title='Rejects' closeFunc={() => setActive(false)} width={450} loading={loading}>
            <div className="rejected-offers-list">
                {offers.map((offer) => {
                    return <div className="rejected-offer" key={offer._id}>
                        <div className="rejected-offer-content">
                            <div>
                                <div className="title" onClick={() => openFunc(offer._id)}>{offer.title}</div>
                            </div>
                            <div className='rejected-offer-chats'>
                                <Button className='empty' img={employerIcon} onClick={() => toChatFunc(offer.employerId)} />
                                <Button className='empty' img={employeeIcon} onClick={() => toChatFunc(offer.employeeId)} />
                            </div>
                        </div>
                        <div className="rejected-offer-buttons">
                            <Button label='Accept' className='green' onClick={() => reviewFunc(offer._id, 'accept')} />
                            <Button label='Loan' className='empty' onClick={() => reviewFunc(offer._id, 'loan')}/>
                            <Button label='Reject' className='red' onClick={() => reviewFunc(offer._id, 'reject')}/>
                        </div>
                    </div>
                })}
            </div>
        </Modal> }
        </>
        
    )
})

export default RejectsBlock