import React, { useState } from 'react'
import Box from 'UI/box/Box'
import './admin.scss'
// import CategoriesBlock from './components/CategoriesBlock'
// import RejectsBlock from './components/RejectsBlock'
import TicketsBlock from './components/TicketsBlock'

const Admin = () => {

    return (
        <div className='admin page'>
            <Box title={'Admin Panel'}>
                {/* <CategoriesBlock /> */}
                <TicketsBlock />
                {/* <RejectsBlock /> */}
            </Box>
        </div>
    )
}

export default Admin