import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { Context } from 'index'
import './footer.scss'
import supportIcon from 'assets/menu/support.svg'
import closeIcon from 'assets/icons/white/close.svg'
import Chat from 'modules/_shared/containers/chat/Chat'
import Loader from 'UI/loader/Loader'

const Footer = observer(() => {
    const {chats, user, socket} = useContext(Context)
    const [loading, setLoading] = useState(true)
    const [supportActive, setSupportActive] = useState(false)

    useEffect(() => {
        if (supportActive) {
            socket.emit('join-chat', 'chat-' + chats.getSupportItem(user.info.id)?._id)
            socket.emit('read-messages', null, chats.getSupportItem(user.info.id)?._id)
            chats.clearUnreadCount(chats.getSupportItem(user.info.id)?._id, user.info.id)
        } else {
            socket.emit('leave-chat', 'chat-' + chats.getSupportItem(user.info.id)?._id)
        }
    }, [supportActive])

    return (
        <>
        {user.isAuth && <div className='footer'>
            <div className="side-left side">
                <div className="live-data">
                    <div className="detector"></div>
                    <div className="online-count">Live: {user.onlines?.length || 0}</div>
                </div>
            </div>
            <div className="side-right side">
                {user.info.status !== 'admin' && <div className="support-panel" onClick={() => setSupportActive(true)}>
                    <img src={supportIcon} />
                    <span>Support</span>
                    <div className={`unreads ${chats.getUnreadSupport(user.info.id) < 1 ? 'none' : ''}`}>
                        {chats.getUnreadSupport(user.info.id)}
                    </div>
                    
                </div>}
                {supportActive && <div className="support-box">
                    
                        <div className="top-side">
                            <div className="title">
                                <img src={supportIcon} />
                                <span>Support</span>
                            </div>
                            <div className="close" onClick={() => setSupportActive(false)}>
                                <img src={closeIcon} />
                            </div>
                        </div>
                        <div className="bottom-side">
                            <Chat
                                chat={chats.getSupportItem()}
                                isLoading={loading}
                                setIsLoading={setLoading}
                                support
                            />
                            {loading && <div className="load-wrapper" style={{height: '100%', position: 'relative'}}>
                                <Loader scale={.75} />
                            </div>}
                        </div>
                </div>}
            </div>
        </div>}
        
        </>
        
    )
})

export default Footer