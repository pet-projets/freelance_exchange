import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import useWallet from 'hooks/useWallet'
import Button from 'UI/button/Button'
import Modal from 'UI/modal/Modal'
import {wallets} from 'utils/walletUtils/wallets'
import WalletItem from './components/WalletItem'
import './connect-wallet.scss'

const ConnectWallet = (props) => {
    const navigate = useNavigate()

    const {connectWallet, disconnectWallet, connectCurrName} = useWallet()

    const connectFunc = (name) => {
        connectWallet(name)
        if (!!localStorage.getItem('me-wallet-name')) {
            props.closeFunc()
        }
    }

    const disconnectFunc = () => {
        props.closeFunc()
        disconnectWallet()
        navigate(0)
    }
    
    return (
        <>
        {!connectCurrName.length && <Modal
            className={'connect-wallet'}
            title='Connect wallet'
            closeFunc={props.closeFunc}
        >
            <div className="wallets">
                {Object.keys(wallets).map((walletName) => {
                    return <WalletItem key={walletName} name={walletName} item={wallets[walletName]} func={connectFunc} />
                })}
            </div>
        </Modal>}
        {!!connectCurrName.length && <Modal
            className={'connect-wallet'}
            title='Oops!'
            closeFunc={props.closeFunc}
        >
            <div className="wallet-banner">
                {connectCurrName === 'Injected' && <span>
                    Selected wallet not found. Enable your wallet plugin (extention), reload and try again
                </span>}
                {connectCurrName !== 'Injected' && <span>
                    There was a collision of wallets. Disable the wallet "{connectCurrName}" plugin (extention), reload and try again
                </span>}
                <div className="button-panel">
                    <Button label='Reload' onClick={disconnectFunc} />
                </div>
            </div>
        </Modal>}
        </>
        
    )
}

export default ConnectWallet