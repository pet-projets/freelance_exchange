import React from 'react'

const WalletItem = (props) => {

    return (
        <div className='wallet-item' onClick={() => props.func(props.name)}>
            <div className="item-side">
                <img src={props.item.icon} />
                <span>{props.item.name}</span>
            </div>
            <div className="item-side">
                <div className="dot"></div>
            </div>
        </div>
    )
}

export default WalletItem