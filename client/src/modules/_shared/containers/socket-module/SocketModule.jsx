import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { Context } from 'index'

const SocketModule = observer(() => {
    const {user, socket, chats} = useContext(Context)
    const [currentChatId, setCurrentChatId] = useState(null)
    const location = useLocation()

    socket.on('new-chat', (chat) => {
        chats.addItem(chat)
    })
    socket.on('notification-message', (message, chatId) => {
        chats.setLatestMessage(chatId, message)
    })
    socket.on('read-panel-messages', (chatId) => {
        chats.clearUnreadCount(chatId, chats.getChat(chatId)?.chater?.id)
    })

    socket.on('online', (onlineUsrId) => {
        user.addOnline(onlineUsrId)
    })
    socket.on('offline', (onlineUsrId) => {
        user.removeOnline(onlineUsrId)
    })
    socket.on('onlines', (onlineUsrIds) => {
        user.setOnlines(onlineUsrIds)
    })

    useEffect(() => {
        if (!user.info.id) return
        socket.emit('join-server', 'user-' + user.info.id)
    }, [user.info.id])

    useEffect(() => {
        const searchPart = location.search
        const chatId = new URLSearchParams(searchPart).get('ch')
        if (chatId === currentChatId) return
        socket.emit('leave-chat', 'chat-' + currentChatId)
        setCurrentChatId(chatId)
        if (!chatId?.length) return
        socket.emit('join-chat', 'chat-' + chatId)
        socket.emit('read-messages', chats.getChat(chatId)?.chater?.id, chatId)
        chats.clearUnreadCount(chatId, user.info.id)
    }, [location.search])


    return (<></>)
})

export default SocketModule