import React, { useContext, useEffect, useState } from 'react'
import Wallet from './components/Wallet'
import './navbar.scss'
import Menu from './components/Menu'
import { observer } from 'mobx-react-lite'
import {Context} from 'index'
import Button from 'UI/button/Button'
import { useLocation, useNavigate } from 'react-router-dom'
import { LOGIN_ROUTE, REGISTRATION_ROUTE } from 'routes/routesPath'
import { BASE_ROUTE } from 'routes/routesPath'
import Messanger from './components/Messanger'
import UserSearch from './components/UserSearch'
import Profile from './components/Profile'

const Navbar = observer(() => {
    const {user, chats} = useContext(Context)
    const navigate = useNavigate(null)

    const location = useLocation(null)
    const [prevLocation, setPrevLocation] = useState(null)
    useEffect(() => {
        if (!(new URLSearchParams(location?.search).get('o')) &&
            !(new URLSearchParams(prevLocation?.search).get('o'))) {
            window.scroll(0, 0)
        }
        setPrevLocation(location)
      }, [location])

    return (
        <>
        <div className='navbar'>
            
            <div className="side l">
                <div className="logo">
                    <img src='' onClick={() => navigate(BASE_ROUTE)} />
                </div>
            </div>

            <div className="side r">
                <UserSearch />
                {user.isAuth && <Messanger unreads={chats.getUnreadCount(user.info.id)} /> }
                {user.isAuth && <Profile id={user.info.id} /> }
                {user.isAuth && <Wallet />}
                {!user.isAuth && <div className="signs">
                    <Button onClick={() => navigate(LOGIN_ROUTE)} label={'Sign In'} className={'empty sign sign-in'} />
                    <Button onClick={() => navigate(REGISTRATION_ROUTE)} label={'Sign Up'} className={'sign sign-up'} />
                </div>} 
            </div>
            
        </div>
        <Menu
            isAuth={user.isAuth}
            isAdmin={user.isAdmin}
        />
        </>
        
    )
})

export default Navbar