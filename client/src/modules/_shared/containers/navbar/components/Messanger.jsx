import React from 'react'
import chatsIcon from 'assets/icons/white/chats.svg'
import { useNavigate } from 'react-router-dom'
import {CHATS_ROUTE} from 'routes/routesPath'

const Messanger = (props) => {
    const navigate = useNavigate()

    return (
        <div className='messanger' onClick={() => navigate(CHATS_ROUTE)}>
            <img src={chatsIcon} />

            <div className={`unreads ${!props.unreads ? 'none' : ''}`}>
                {props.unreads}
            </div>
        </div>
    )
}

export default Messanger