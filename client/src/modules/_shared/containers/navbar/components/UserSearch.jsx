import React, { useEffect, useState } from 'react'
import searchIcon from 'assets/icons/white/search.svg'
import Modal from 'UI/modal/Modal'
import Search from 'UI/search/Search'
import { searchUser } from 'modules/_shared/api/userAPI'
import UserInfo from 'modules/_shared/containers/user-info/UserInfo'

const UserSearch = () => {
    const [items, setItems] = useState([])
    const [isActive, setIsActive] = useState(false)
    const [userInfoId, setUserInfoId] = useState(null)

    useEffect(() => {
        if (!isActive) return
        setItems([])
    }, [isActive])

    const clickFunc = (id) => {
        setUserInfoId(id)
        setIsActive(false)
    }

    const searchFunc = (value) => {
        searchUser(value)
        .then((data) => {
            setItems(data)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    const closeFunc = () => {
        setUserInfoId(null)
        setItems([])
        setIsActive(false)
    }
    
    return (
        <>
        <div className='user-search' onClick={() => setIsActive(true)}>
            <img src={searchIcon} />
        </div>
        {isActive && <Modal title={'Search'} closeFunc={closeFunc}>
            <Search
                placeholder={"Search users ..."}
                searchFunc={searchFunc}
                delay={1}
            >
                <div className="user-search-list">
                    {items.map((item) => {
                        return <div
                            key={item.id}
                            className={`search-list-item`}
                            onClick={() => clickFunc(item.id)}
                        >
                            <div className="name">
                                <span>{item.username}</span>
                            </div>
                        </div>
                    })}
                </div>
            </Search>
        </Modal>}
        {userInfoId && <UserInfo
            userId={userInfoId}
            closeFunc={() => setUserInfoId(null)}
        />}
        </>
        
    )
}

export default UserSearch