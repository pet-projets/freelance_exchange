import React, { useState } from 'react'
import profileIcon from 'assets/menu/profile.svg'
import UserInfo from 'modules/_shared/containers/user-info/UserInfo'

const Profile = (props) => {
    const [active, setActive] = useState(false)

    return (
        <>
        <div className='profile' onClick={() => setActive(true)}>
            <img src={profileIcon} />
        </div>
        {active && <UserInfo
            closeFunc={() => setActive(false)}
            userId={props.id}
            self
        />}
        </>
        
    )
}

export default Profile