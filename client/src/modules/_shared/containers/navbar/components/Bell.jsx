import React, { useEffect, useState } from 'react'
import { setNotification } from 'modules/_shared/api/userAPI'
import bellIconActive from 'assets/icons/white/bell-active.svg'
import bellIconPassive from 'assets/icons/white/bell-passive.svg'
import Alert from 'modules/_shared/containers/alert/Alert'

const Bell = (props) => {
    const [active, setActive] = useState(props.active)
    const [alert, setAlert] = useState('')

    useEffect(() => {
        setNotification(active)
        .catch((err) => setAlert(err.response.data.message))
    }, [active])
    
    return (
        <>
        <div className='bell' onClick={() => setActive(!active)}>
            <img src={active ? bellIconActive : bellIconPassive} />
        </div>
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
}

export default Bell