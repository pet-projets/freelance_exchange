import React, { useMemo, useState } from 'react'
import {useLocation, useNavigate} from 'react-router-dom'
import {shared} from 'routes/menuItems'
import logoutIcon from 'assets/icons/white/logout.svg'


const Menu = (props) => {
    const [active, setActive] = useState(false)
    const navigate = useNavigate(null)
    const location = useLocation(null).pathname
    const menuItems = useMemo(() => {
        return props.isAuth ? (props.isAdmin ? shared.adminMenuItems : shared.authMenuItems) : shared.unAuthMenuItems
    }, [props.isAuth, props.isAdmin])

    const logoutFunc = () => {
        localStorage.removeItem('token')
        localStorage.removeItem('me-wallet-name')
        localStorage.removeItem('me-address')
        navigate(0)
    }

    return (
        <>
        <div
            className={`menu ${active ? 'active' : ''}`}
            onMouseOver={() => setActive(true)}
            onMouseLeave={() => setActive(false)}
        >
            <div className="menu-wrapper">
                <div className="menu-list">
                    {menuItems.filter((item) => !item.logout).map((item, index) => {
                        return <div
                            key={index}
                            className={`menu-item ${location === item.url ? 'active' : ''}`}
                            onClick={() => navigate(item.route)}
                        >
                            <img src={item.icon} />
                            <span>{item.name}</span>
                        </div>
                    })}
                </div>
                {props.isAuth && <div className="menu-list logout">
                    <div className="menu-item" onClick={logoutFunc}>
                        <img src={logoutIcon} />
                        <span>{'Logout'}</span>
                    </div>
                </div>}
            </div>
            
        </div>
        </>
        
    )
}

export default Menu