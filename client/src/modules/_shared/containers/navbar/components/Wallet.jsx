import React, { useEffect, useState } from 'react'
import Button from 'UI/button/Button'
import walletIcon from 'assets/icons/white/wallet.svg'
import ConnectWallet from 'modules/_shared/containers/connect-wallet/ConnectWallet'
import useWallet from 'hooks/useWallet'
import ManageWallet from 'modules/_shared/containers/manage-wallet/ManageWallet'
import { connectWallet } from 'modules/_shared/api/userAPI'
import { useNetwork, useSwitchNetwork } from 'wagmi'
import { walletConfig } from 'utils/walletUtils/walletConfig'
import Alert from 'modules/_shared/containers/alert/Alert'

const CHAIN_ID = walletConfig['development'].chain

const Wallet = () => {
    const [isModalConnect, setIsModalConnect] = useState(false)
    const [isModalManage, setIsModalManage] = useState(false)
    const {isConnected, address, disconnectWallet} = useWallet()
    const {chain} = useNetwork()
    const [alert, setAlert] = useState('')
    const {switchNetwork} = useSwitchNetwork({
        onError() { disconnectWallet() }
    })

    useEffect(() => {
        if (!isModalManage) return
        const handleDocumentClick = (event) => {
            const clickedElement = event.target
            const isNotOnManageWallet = !clickedElement.classList.contains('manage-wallet') &&
                !clickedElement.closest('.manage-wallet');
            const isNotOnManageWalletButton = !clickedElement.classList.contains('manage-wallet-button') &&
                !clickedElement.closest('.manage-wallet-button');
            if (isNotOnManageWallet && isNotOnManageWalletButton) {
                setIsModalManage(false)
            }
        }
        document.addEventListener('click', handleDocumentClick)
        return () => {
            document.removeEventListener('click', handleDocumentClick)
        }
    }, [isModalManage])

    useEffect(() => {
        if (!isConnected) return
        const currAddress = localStorage.getItem('me-address')
        if (currAddress !== address) {
            connectWallet(address || '')
            .then(() => {
                localStorage.setItem('me-address', address)
            })
            .catch((err) => {
                setAlert(err.response.data.message)
            })
        }
    }, [address])

    useEffect(() => {
        if (!chain) return
        if (chain.id !== CHAIN_ID) {
            switchNetwork(CHAIN_ID)
        }
    }, [chain])

    return (
        <>
        {!isConnected && <>
            <Button
                label={'Connect Wallet'}
                img={walletIcon}
                style={{width: 192, height: 40, fontSize: 14, fontWeight: 500}}
                onClick={() => setIsModalConnect(true)}
            />
            {isModalConnect && <ConnectWallet
                closeFunc={() => setIsModalConnect(false)}
            />}
        </>}
        {isConnected && <>
            <Button
                label={''}
                className={'empty manage-wallet-button'}
                img={walletIcon}
                style={{height: 40, padding: '0 25px'}}
                onClick={() => setIsModalManage(true)}
            />
            {isModalManage && <ManageWallet
                closeFunc={() => setIsModalManage(false)}
                reconnectFunc={() => setIsModalConnect(true)}
            />}
        </>}
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
}

export default Wallet