import React, { useEffect, useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Alert = (props) => {
    const [isOpen, setIsOpen] = useState(false)
     
    const notify = () => {
        toast.error(props.text || 'Error', {
            position: "bottom-right",
            theme: 'dark',
            onClose: () => {
                props.closeFunc()
            },
            onOpen: () => setIsOpen(true)
        })
    }

    useEffect(() => {
        if (isOpen) return
        notify()
    }, [])

    return (
        <ToastContainer />
    )
}

export default Alert