import React, { useContext, useState } from 'react'
import chatsIcon from 'assets/menu/chats.svg'
import sendIcon from 'assets/icons/white/send.svg'
import closeIcon from 'assets/icons/white/close.svg'
import Button from 'UI/button/Button'
import { createChat, getChatByUser, sendMessage } from 'modules/_shared/api/chatAPI'
import { useNavigate } from 'react-router-dom'
import { CHATS_ROUTE, LOGIN_ROUTE } from 'routes/routesPath'
import { observer } from 'mobx-react-lite'
import { Context } from 'index'
import Input from 'UI/input/Input'
import Alert from 'modules/_shared/containers/alert/Alert'
import Loader from 'UI/loader/Loader'

const UserInfoChat = observer((props) => {
    const {user, socket, chats} = useContext(Context)
    const [message, setMessage] = useState('')
    const [sendLoading, setSendLoading] = useState(false)
    const navigate = useNavigate()

    const [alert, setAlert] = useState('')

    const click = () => {
        if (!user.isAuth) {
            navigate(LOGIN_ROUTE)
            return
        }
        getChatByUser(props.info.id)
        .then((data) => {
            if (!data.id) {
                props.setIsChatActive(true)
            } else {
                navigate(CHATS_ROUTE + '?ch=' + data.id)
                props.closeFunc()
            }
        })
    }

    const sendFunc = () => {
        if (!message.length) return
        props.setIsBlocked(true)
        setSendLoading(true)
        createChat(props.info.id)
        .then((data) => {
            const chaters = [
                { id: props.info.id, username: props.info.username, avatar: props.info.avatar },
                { id: user.info.id, username: user.info.username, avatar: user.info.avatar }
            ]
            chats.addItem(Object.assign({}, data.chat, {
                latestMessage: null, unreadCount: 0, chater: chaters[0]
            }))
            socket.emit('new-chat', props.info.id, Object.assign({}, data.chat, {
                latestMessage: null, unreadCount: 0, chater: chaters[1]
            }))
            sendMessage(message, [], data.chat._id, null)
            .then((datam) => {
                chats.setLatestMessage(data.chat._id, datam.message)
                socket.emit('send-message', 'chat-' + data.chat._id, datam.message, props.info.id)
                navigate(CHATS_ROUTE + '?ch=' + data.chat._id)
                props.setIsBlocked(false)
                props.closeFunc()
            })
            .catch((err) => setAlert(err.response.data.message))
        })
        .catch((err) => setAlert(err.response.data.message))
    }

    return (
        <>
        {!props.isChatActive && <Button
            className={'user-info-chat'}
            img={chatsIcon}
            onClick={click}
        />}
        {props.isChatActive && <div className="first-message">
            <Input
                value={message}
                valueFunc={setMessage}
                autoFocus
                enterFunc={sendFunc}
            />
            {!!message.length && !sendLoading && <Button
                className={'thin send-icon'}
                img={sendIcon}
                onClick={sendFunc}
            />}
            {!message.length  && !sendLoading && <Button
                className={'thin send-icon'}
                img={closeIcon}
                onClick={() => props.setIsChatActive(false)}
            />}
            {sendLoading && <div className="loader-box send-icon"><Loader
                color={'#E8E5DA'}
                scale={0.25} />
            </div>}
        </div> }
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
    )
})

export default UserInfoChat