import React from 'react'
import { Textarea } from 'UI/textarea/Textarea'
import deleteIcon from 'assets/icons/white/delete.svg'
import imageIcon from 'assets/icons/white/image.svg'
import InputFile from 'UI/input-file/InputFile'
import Button from 'UI/button/Button'
import Password from 'UI/password/Password'

const Bio = (props) => {

    return (
        <div className='bio'>

            {props.self && <div className="avatar-info">
                <div className="avatar-panel">
                    {(!props.info?.avatar && !props.newAvatar || props.newAvatar === undefined) && props.self && <InputFile
                        label={'Set Photo'}
                        img={imageIcon}
                        accept={'image/*'}
                        changeFunc={props.setNewAvatar}
                        height={30}
                        padding={30}
                    /> }
                    {(props.info?.avatar && props.newAvatar !== undefined || !!props.newAvatar) && props.self && <Button
                        className="empty"
                        label={'Remove Photo'}
                        img={deleteIcon}
                        onClick={() => props.setNewAvatar(undefined)}
                    /> }
                </div>
            </div>}

            {(props.self || !!props.desc?.length) && <div className="description-info">
                <div className="info-label" style={{marginBottom: 5}}>Bio</div>
                <Textarea
                    rows={4.5}
                    readOnly={!props.self}
                    placeholder={'Bio ...'}
                    value={props.desc}
                    valueFunc={props.setDescriptionFunc}
                />
            </div>}

            {props.self && <div className="change-password">
                <div className="info-label" style={{marginBottom: 5}}>Change Password</div>
                <Password placeholder={'New Password'} valueFunc={props.setNewPassword} />
            </div> }
        </div>
    )
}

export default Bio