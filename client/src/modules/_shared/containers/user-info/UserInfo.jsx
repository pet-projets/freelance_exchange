import React, { useEffect, useState } from 'react'
import { changeCategories, removeCV, setCV } from 'modules/freelance/api/userAPI'
import { setAvatar, removeAvatar, createDescription, getInfo, editPassword } from 'modules/_shared/api/userAPI'
import Modal from 'UI/modal/Modal'
import './user-info.scss'
import Switcher from 'UI/switcher/Switcher'
import Button from 'UI/button/Button'
import saveIcon from 'assets/icons/white/save.svg'
import { uploadFile } from 'modules/_shared/api/fileAPI'
import Alert from 'modules/_shared/containers/alert/Alert'
import UserInfoFreelance from 'modules/freelance/containers/user-info/UserInfoFreelance'
import Bio from './components/Bio'
import profileIcon from 'assets/menu/profile.svg'
import freelanceIcon from 'assets/menu/freelance.svg'
import userIcon from 'assets/icons/white/user.svg'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'
import UserInfoChat from './components/UserInfoChat'

const UserInfo = (props) => {
    const [info, setInfo] = useState(null)
    const [isLoading, setIsLoading] = useState(true)
    const [isBlocked, setIsBlocked] = useState(false)
    const [isChatActive, setIsChatActive] = useState(false)
    const [alert, setAlert] = useState('')
    const [active, setActive] = useState(0)

    const [newDescription, setNewDescription] = useState(null)
    const [newAvatar, setNewAvatar] = useState(null)
    const [newPassword, setNewPassword] = useState(null)

    const [newCategories, setNewCategories] = useState(null)
    const [newCV, setNewCV] = useState(null)

    const switchElements = [
        { icon: profileIcon },
        { icon: freelanceIcon }
    ]

    useEffect(() => {
        if (props.self) {
            
        }
        getInfo(props.userId)
        .then((data) => {
            setInfo(data.info)
            setIsLoading(false)
        })
        .catch((err) => {
            setAlert(err.response.data.message)
        })
    }, [])

    useEffect(() => {
        if (
            newDescription === null &&
            newAvatar === null &&
            newPassword &&
            newCategories === null &&
            newCV === null &&
            isBlocked
        ) {
            props.closeFunc()
        }
    }, [
        newDescription,
        newAvatar,
        newPassword,
        newCategories,
        newCV,
        isBlocked
    ])

    const saveFunc = () => {
        if (!info) return
        setIsBlocked(true)
        setIsLoading(true)
        if (newDescription !== info.description && newDescription !== null) {
            createDescription(newDescription)
            .then(() => setNewDescription(null))
            .catch((err) => setAlert(err.response.data.message))
        }
        if (newCategories !== null) {
            changeCategories(newCategories.map((item) => item._id))
            .then(() => setNewCategories(null))
            .catch((err) => setAlert(err.response.data.message))
        }
        if (newPassword !== null && newPassword.length >= 8) {
            editPassword(newPassword)
            .then(() => setNewPassword(null))
            .catch((err) => setAlert(err.response.data.message))
        }
        if (newCV !== null) {
            if  (newCV === undefined) {
                removeCV()
                .then(() => setNewCV(null))
                .catch((err) => setAlert(err.response.data.message))
            } else {
                uploadFile(newCV, '/users/cvs')
                .then((data) => {
                    setCV(data.file)
                    .then(() => {
                        setNewCV(null)
                    })
                    .catch((err) => setAlert(err.response.data.message))
                })
                .catch((err) => setAlert(err.response.data.message))
            }
        }
        if (newAvatar !== null) {
            if  (newAvatar === undefined) {
                removeAvatar()
                .then(() => setNewAvatar(null))
                .catch((err) => setAlert(err.response.data.message))
            } else {
                uploadFile(newAvatar, '/users/avatars')
                .then((data) => {
                    setAvatar(data.file)
                    .then(() => {
                        setNewAvatar(null)
                    })
                    .catch((err) => setAlert(err.response.data.message))
                })
                .catch((err) => setAlert(err.response.data.message))
            }
        }
    }

    return (
        <>
        <Modal closeFunc={props.closeFunc} width={310} loading={isLoading} blockClose={isBlocked}>
            {!isLoading && <div className="user-info">
                <div className="user-info-panel">
                    <div className="user-avatar">
                        {(info?.avatar && newAvatar !== undefined || !!newAvatar) && <>
                            <FileDisplayer
                                previewOnly
                                fileModel={!newAvatar}
                                image
                                file={newAvatar || info?.avatar}
                                width={'auto'} height={'auto'}
                            />
                            <div className="avatar-darker"></div>
                        </>  }
                        {(!info?.avatar && !newAvatar || newAvatar === undefined) && <div className="avatar-default">
                            <img src={userIcon} />
                        </div>}
                    </div>
                    <div className={`user-info-top ${isChatActive ? 'chat-active' : ''}`}>
                        <div>
                            <div className="username">{info.username}</div>
                        </div>
                        {!props.self && <UserInfoChat
                            info={info}
                            closeFunc={props.closeFunc}
                            isChatActive={isChatActive}
                            setIsChatActive={setIsChatActive}
                            setIsBlocked={setIsBlocked}
                        />}
                    </div>
                </div>
                <div className="switcher-panel">
                    <Switcher func={setActive} items={switchElements} />
                </div>
                <div className="user-info-content">
                    {active === 0 && <Bio
                        self={props.self}
                        info={info}
                        desc={newDescription || info.description}
                        setDescriptionFunc={setNewDescription}
                        newAvatar={newAvatar}
                        setNewAvatar={setNewAvatar}
                        setNewPassword={setNewPassword}
                    /> }
                    {active === 1 && <UserInfoFreelance
                        self={props.self}
                        info={info}
                        newCategories={newCategories}
                        newCV={newCV}
                        setNewCategories={setNewCategories}
                        setNewCV={setNewCV}
                    />}
                </div>
                {props.self && <div className="button-panel">
                    {(newDescription !== null || newCategories !== null || newCV !== null || newAvatar !== null || newPassword?.length >= 8) &&
                        <Button label='Save' img={saveIcon} onClick={saveFunc}
                    />}
                </div>}
            </div>}
        </Modal>
        {!!alert?.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
    )
}

export default UserInfo