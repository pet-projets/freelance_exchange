import React from 'react'
import developmentIcon from 'assets/icons/white/development.svg'
import './access-module.scss'

const AccessModule = () => {
    

    return (
        <div className="access-module">
            <img src={developmentIcon} />
            <div className="access-module-text">
                <span>The service is under development.</span>
                <span>To access the site, contact the administrator</span>
            </div>
        </div>
    )
}

export default AccessModule