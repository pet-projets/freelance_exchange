import React, { useContext, useEffect, useMemo, useRef, useState } from 'react'
import Button from 'UI/button/Button'
import './file-displayer.scss'
import deleteIcon from 'assets/icons/white/delete.svg'
import downloadIcon from 'assets/icons/white/download.svg'
import fileIcon from 'assets/icons/d2/file.svg'
import imageIcon from 'assets/icons/d2/image.svg'
import videoIcon from 'assets/icons/d2/video.svg'
import { formatSize } from 'utils/functions'
import { downloadFile } from 'modules/_shared/api/fileAPI'
import Alert from 'modules/_shared/containers/alert/Alert'

const FileDisplayer = (props) => {
    const [image, setImage] = useState(null)
    const nameRef = useRef(null)
    const [name, setName] = useState(null)
    const [alert, setAlert] = useState('')

    useEffect(() => {
        if (!props.file) return
        const currName = name || props.file.name
        if ((nameRef.current?.offsetHeight || 0) > 16) {
            return setName(currName.slice(0, 5) + '...' + currName.slice(-7))
        } else {
            return setName(currName)
        }
    }, [props.file, name])

    useEffect(() => {
        if (!props.image || !props.file) return
        if (!props.fileModel) {
            const reader = new FileReader()
            reader.onloadend = () => {
                setImage(reader.result)
            }
            reader.readAsDataURL(props.file)
        } else {
            if (props.file.linkExp <= Date.now()) {
                downloadFile(props.file._id)
                .then((data) => {
                    setImage(data.link)
                })
                .catch((err) => {
                    setAlert(err.response.data.message)
                })
            } else {
                setImage(props.file.link)
            }
        }
    }, [props.image, props.file, props.fileModel])

    const downloadFunc = () => {
        if (!props.download) return
        if (props.file.linkExp <= Date.now()) {
            downloadFile(props.file._id)
            .then((data) => {
                window.open(data.link, '_blank', 'noopener')
            })
            .catch((err) => {
                setAlert(err.response.data.message)
            })
        } else {
            window.open(props.file.link, '_blank', 'noopener')
        }
    }

    const fullscreenFunc = () => {
        if (!props.fullscreenImageFunc && !props.fullscreenVideoFunc) return
        if (props.file.linkExp <= Date.now()) {
            downloadFile(props.file._id)
            .then((data) => {
                props.file.mimetype === 'image' ?
                    props.fullscreenImageFunc(data.link) :
                    props.fullscreenVideoFunc(data.link)
            })
            .catch((err) => {
                setAlert(err.response.data.message)
            })
        } else {
            props.file.mimetype === 'image' ?
                props.fullscreenImageFunc(props.file.link) :
                props.fullscreenVideoFunc(props.file.link)
        }
    }

    const clickMimetypeFunc = () => {
        if (props.file.mimetype === 'image' || props.file.mimetype === 'video') {
            fullscreenFunc()
        } else {
            downloadFunc()
        }
    }

    return (
        <>
        <div
            className={`file-displayer-wrapper ${props.image ? 'image' : ''}`}
            style={{width: props.width, height: props.height, borderRadius: props.image ? 5 : 0}}
        >
            {image && <div
                className="file-displayer-image"
                onClick={fullscreenFunc}
            >
                <div className="file-displayer-image-wrapper"><img src={image} /></div>
            </div>}
            {!props.previewOnly && <div
                className={`file-displayer`}
                style={{width: props.displayerWidth, backgroundColor: props.bgc ? props.bgc : 'var(--dark-color-2)'}}
            >
                <div className="side side-left">
                    <div className="mimetype" onClick={clickMimetypeFunc}>
                        {(props.file.mimetype !== 'image' && props.file.mimetype !== 'video') && <img src={fileIcon} />}
                        {props.file.mimetype === 'image' && <img src={imageIcon} />}
                        {props.file.mimetype === 'video' && <img src={videoIcon} />}
                    </div>
                    <div className="name">
                        <div className="name-wrapper"><span ref={nameRef}>{name || ''}</span></div>
                        <div className="size">{formatSize(props.file.size)}</div>
                    </div>
                </div>
                <div className="side side-right">
                    {props.download && <Button img={downloadIcon} className={'thin'} onClick={downloadFunc} />}
                    {props.deleteFunc && <Button img={deleteIcon} className={'thin'} onClick={props.deleteFunc} />}
                </div>
            </div>}
        </div>
        {!!alert.length && <Alert text={alert} closeFunc={() => setAlert('')}  />}
        </>
        
       
    )
}

export default FileDisplayer