import React from 'react'
import './manage-wallet.scss'
import closeIcon from 'assets/icons/white/add.svg'
import useWallet from 'hooks/useWallet'
import { wallets } from 'utils/walletUtils/wallets'
import tokenImg from 'assets/icons/base/ethereum.svg'
import disconnectIcon from 'assets/icons/white/disconnect.svg'
import refreshIcon from 'assets/icons/white/refresh.svg'

const ManageWallet = (props) => {

    const {address, balance, disconnectWallet} = useWallet()

    const disconnectFunc = () => {
        disconnectWallet()
        props.closeFunc()
    }

    const reconnectFunc = () => {
        disconnectWallet()
        props.reconnectFunc()
        props.closeFunc()
    }

    const formatCoins = (value) => {
        return parseFloat(value) > 0 ? parseFloat(value).toFixed(5) : 0
    }

    return (
        <div className="manage-wallet">
            <div className="manage-title">
                <div className="title-info">
                    <img src={wallets[localStorage.getItem('me-wallet-name')].icon} />
                    <span>{address.slice(0, 7)}...{address.slice(-4)}</span>
                </div>
                <div className="close" onClick={props.closeFunc}>
                    <img src={closeIcon} style={{transform: `rotate(45deg)`}} />
                </div>
            </div>
            <div className="manage-content">
                <div className="balance">
                    <div className="token"><img src={tokenImg}/></div>
                    <div className="coins">{formatCoins(balance.data?.formatted)} ETH</div>
                </div>
                <div className="manage-actions">
                    <div className="manage-action" onClick={reconnectFunc}>
                        <img src={refreshIcon} />
                        <span>Connect different wallet</span>
                    </div>
                    <div className="manage-action" onClick={disconnectFunc}>
                        <img src={disconnectIcon} />
                        <span>Disconnect wallet</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ManageWallet