import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import {Context} from 'index'
import {getActiveOffers, getAllCategories, getRelatedCandidates} from 'modules/freelance/api/offerAPI'

const FetchModule = observer((props) => {
    const {
        user,
        offers,
        categories,
        posts,
    } = useContext(Context)
    const [typesFetched, setTypesFetched] = useState([])

    useEffect(() => {
        const fetchTypeName = 'freelance'
        if (
            user.fetchType !== fetchTypeName ||
            typesFetched.includes(fetchTypeName)
        ) return
        async function fetchData() {
            try {
                props.loadingFunc(true)
                setTypesFetched([...typesFetched, fetchTypeName])
                const {categories: allCategories} = await getAllCategories()
                categories.setItems(allCategories)
                const {offers: activeOffers} = await getActiveOffers()
                offers.setItems(activeOffers)
                if (user.isAuth) {
                    const {candidates: relatedCandidates} = await getRelatedCandidates()
                    offers.setCandidatesItems(relatedCandidates)
                }
                props.loadingFunc(false)
            } catch (err) {
                console.error(`Fetch <freelance> data error: ${err.response.data.message}`)
            }
        }
        fetchData()
    }, [user.isAuth, user.fetchType])

    useEffect(() => {
        const fetchTypeName = 'categories'
        if (
            user.fetchType !== fetchTypeName ||
            typesFetched.includes(fetchTypeName)
        ) return
        async function fetchData() {
            try {
                setTypesFetched([...typesFetched, fetchTypeName])
                const {categories: allCategories} = await getAllCategories()
                categories.setItems(allCategories)
            } catch (err) {
                console.error(`Fetch <categories> data error: ${err.response.data.message}`)
            }
        }
        fetchData()
    }, [user.fetchType])

    return (<></>)
})

export default FetchModule