import React, { useContext, useEffect, useState } from 'react'
import './chat.scss'
import { useLocation, useNavigate } from 'react-router-dom'
import UserInfo from 'modules/_shared/containers/user-info/UserInfo'
import { CHATS_ROUTE } from 'routes/routesPath'
import { getChat, sendMessage, setRead } from 'modules/_shared/api/chatAPI'
import { formatMessages } from 'utils/functions'
import { uploadFile } from 'modules/_shared/api/fileAPI'
import Fullscreen from 'UI/fullscreen/Fullscreen'
import TopPanel from './components/TopPanel'
import BottomPanel from './components/BottomPanel'
import MainPanel from './components/MainPanel'
import { observer } from 'mobx-react-lite'
import { Context } from 'index'

const Chat = observer((props) => {
    const {user, socket, chats} = useContext(Context)
    const [messagesItems, setMessagesItems] = useState([])
    const [groups, setGroups] = useState([])
    const [isUserInfo, setIsUserInfo] = useState(false)
    const location = useLocation()
    const navigate = useNavigate()
    const [text, setText] = useState('')
    const [files, setFiles] = useState([])
    const [related, setRelated] = useState('')
    const [sendLoading, setSendLoading] = useState(false)
    const [fullscreenImage, setFullscreenImage] = useState(null)
    const [fullscreenVideo, setFullscreenVideo] = useState(null)
    const [lastReceived, setLastReceived] = useState(null)
    const [isTyping, setIsTyping] = useState(false)
    let timeoutsIds = []

    socket.on('send-message', (message) => {
        setLastReceived(message)
    })
    socket.on('read-messages', () => {
        readMessageFunc()
    })
    socket.on('typing', () => {
        setIsTyping(true)
        timeoutsIds.forEach((ti) => clearTimeout(ti))
        timeoutsIds = []
        timeoutsIds.push(setTimeout(() => {
          setIsTyping(false)
        }, 2000))
    })

    useEffect(() => {
        if (!text.length) return
        socket.emit('typing', 'chat-' + props.chat?._id)
    }, [text])

    useEffect(() => {
        if (!props.chat?._id) {
            setMessagesItems([])
            return
        }
        props.setIsLoading(true)
        getChat(props.chat._id)
        .then((data) => {
            setMessagesItems(data.messages)
            props.setIsLoading(false)
        })
        .catch((err) => console.log(err.response.data.message))
    }, [props.chat?._id])

    useEffect(() => {
        const groupedMessages = formatMessages(messagesItems)
        setGroups(groupedMessages.sort((a, b) => b.date - a.date))
    }, [messagesItems])

    const renewMessagesFunc = (message) => {
        setMessagesItems((prevMessagesItems) => {
            const isMessageExists = prevMessagesItems.some((m) => m._id === message._id)
            if (!isMessageExists) {
                return [...prevMessagesItems, message]
            }
            return prevMessagesItems;
        })
        chats.setLatestMessage(props.chat?._id, message)
    }

    const readMessageFunc = () => {
        const newItems = messagesItems.map((item) => item.isRead ? item : {...item, isRead: true})
        setMessagesItems(newItems)
    }

    useEffect(() => {
        if (!lastReceived) return
        renewMessagesFunc({...lastReceived, isRead: true})
        socket.emit('read-messages', props.chat?.chater?.id, props.chat?._id)
        chats.clearUnreadCount(props.chat?._id, user.info.id)
        setRead(props.chat?._id)
    }, [lastReceived])

    const sendMessageFunc = async () => {
        if (sendLoading || (!text.length && !files.length)) return
        setSendLoading(true)
        let fileIds = []
        const metaFiles = [...files]
        setText('')
        setFiles([])
        setRelated('')
        for (let metaFile of metaFiles) {
            const {file: metaFileId} = await uploadFile(metaFile, `/chats/${props.chat?._id}`)
            fileIds.push(metaFileId)
        }
        sendMessage(text, fileIds, props.chat?._id, related)
        .then((data) => {
            renewMessagesFunc(data.message)
            socket.emit('send-message', 'chat-' + props.chat?._id, data.message, props.chat?.chater?.id)
            setSendLoading(false)
        })
        .catch((err) => console.log(err.response.data.message))
    }

    return (
        <>
        {!props.isLoading && props.chat?._id && <>
        <div className={`chat ${props.support ? 'support' : ''}`}>
            
            {<TopPanel
                userInfoFunc={() => setIsUserInfo(true)}
                username={props.chat?.chater?.username}
                // offerFunc={() => { navigate(CHATS_ROUTE + location.search + (!!location.search.length ? '&' : '?') + 'o=' + props.chat?.offer?.id) }}
                isOnline={user.isOnline(props.chat?.chater?.id)}
                isTyping={isTyping}
                isFreelance={props.chat.isFreelance}
            />}

            <MainPanel
                related={related}
                setRelated={setRelated}
                groups={groups}
                setFullscreenImage={setFullscreenImage}
                setFullscreenVideo={setFullscreenVideo}
                userId={user.info.id}
                items={messagesItems}
                chaterUsername={props.chat?.chater?.username || 'Support'}
            />

            <BottomPanel
                text={text}
                setText={setText}
                files={files}
                related={related}
                clearRelated={() => setRelated('')}
                clearFiles={() => setFiles([])}
                setFiles={setFiles}
                replyAuthor={messagesItems?.find((item) => item._id === related)?.senderId === user.info.id ? 'You' : props.chat?.chater?.username}
                replyText={messagesItems?.find((item) => item._id === related)?.text || 'File'}
                sendMessageFunc={sendMessageFunc}
                loading={sendLoading}
            />

        </div>

        {isUserInfo && <UserInfo
            userId={props.chat?.chater?.id}
            closeFunc={() => setIsUserInfo(false)}
        />}
        {(fullscreenImage || fullscreenVideo) && <Fullscreen image={fullscreenImage} video={fullscreenVideo} closeFunc={() => {
            setFullscreenImage(null)
            setFullscreenVideo(null)
        }} />}
        </>}
        </>
        
    )
})

export default Chat