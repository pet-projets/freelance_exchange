import React from 'react'
import replyIcon from 'assets/icons/base/reply.svg'
import closeIcon from 'assets/icons/white/close.svg'
import fileIcon from 'assets/icons/white/file.svg'
import atachIcon from 'assets/icons/white/attach.svg'
import sendIcon from 'assets/icons/base/send.svg'
import InputFile from 'UI/input-file/InputFile'
import { Textarea } from 'UI/textarea/Textarea'
import Button from 'UI/button/Button'
import Loader from 'UI/loader/Loader'

const BottomPanel = (props) => {

    return (
        <div className={`bottom-panel ${!!props.files.length ? 'attached' : ''}`}>
            {!!props.related.length && <div className="reply-message-panel">
                <div className="reply-side">
                    <img src={replyIcon} />
                    <div className="reply-text-content">
                        <div className="reply-author">{props.replyAuthor || 'Support'}</div>
                        <div className="reply-text">{props.replyText}</div>
                    </div>
                </div>
                <img className='close' src={closeIcon} onClick={props.clearRelated} />
            </div> }
            {<InputFile
                buttonClassName='thin'
                img={atachIcon}
                padding={0} height={25}
                multiple
                changeFunc={props.setFiles}
            />}
            {!!props.files.length && <div className="attached-files">
                <div>
                    <img src={fileIcon} />
                    <span>Files ({props.files.length})</span>
                </div>
                <img className='close' src={closeIcon} onClick={props.clearFiles} />
            </div>}
            <Textarea
                rows={1}
                placeholder={'Write a message ...'}
                fontSize={14}
                value={props.text}
                valueFunc={props.setText}
                autoFocus
                shiftEnter
                enterFunc={props.sendMessageFunc}
            />
            {!props.loading && <Button img={sendIcon} className='thin' onClick={props.sendMessageFunc} />}
            {props.loading &&
                <div className="loader-box"><Loader scale={0.3} /></div>
            }
        </div>
    )
}

export default BottomPanel