import React from 'react'
import Button from 'UI/button/Button'

const TopPanel = (props) => {

    return (
        <div className="top-panel">
            <div className="text-info" onClick={props.userInfoFunc}>
                <div className="username">{props.username}</div>
                <div className={`action-info ${props.isOnline || props.isTyping ? 'active' : ''}`}>
                    {!props.isTyping && <span className={'online-indicator'}>{props.isOnline ? 'online' : 'offline'}</span>}
                    {props.isTyping && <span className={'typing-indicator'}>typing...</span>}
                </div>
            </div>
            {!!props.isFreelance && <div className="offer" onClick={props.offerFunc} >
                {/* <FileDisplayer
                    previewOnly
                    fileModel
                    image
                    file={props.offerPreview}
                    width={35} height={35}
                /> */}
                {/* <Button /> */}
            </div>}
        </div>
  )
}

export default TopPanel