import React, { useEffect, useRef, useState } from 'react'
import readIcon from 'assets/icons/white/done-all.svg'
import unreadIcon from 'assets/icons/white/done.svg'
import replyWhiteIcon from 'assets/icons/white/reply.svg'
import arrowIcon from 'assets/icons/white/arrow.svg'
import { formatTimestamp } from 'utils/functions'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'

const MainPanel = (props) => {
    const mainPanelRef = useRef()
    const [isScrollDown, setIsScrollDown] = useState(false)

    useEffect(() => {
        if (!props.groups.length) return
        mainPanelRef.current.scrollTo({
            top: mainPanelRef.current.scrollHeight
        })
    }, [props.groups])

    useEffect(() => {
        const handleScroll = () => {
            setIsScrollDown(mainPanelRef.current.scrollHeight - (mainPanelRef.current.offsetHeight + mainPanelRef.current.scrollTop) > 50)
        }
        mainPanelRef.current.addEventListener('scroll', handleScroll)
    
        return () => {
            if (mainPanelRef.current) {
                mainPanelRef.current.removeEventListener('scroll', handleScroll);
            }
        }
    }, [])

    const scrollerFunc = () => {
        mainPanelRef.current.scrollTo({
            top: mainPanelRef.current.scrollHeight,
            behavior: 'smooth',
        })
    }

    const scrollToMessage = (id) => {
        const messageElement = document.getElementById(`message-${id}`);
        if (messageElement && mainPanelRef.current) {
            const mainPanelHeight = mainPanelRef.current.offsetHeight;
            const messageHeight = messageElement.offsetHeight;
            const elementTop = messageElement.offsetTop;
            const scrollTo = elementTop - (mainPanelHeight - messageHeight);
            mainPanelRef.current.scrollTo({
              top: scrollTo,
              behavior: 'smooth',
            })
            messageElement.classList.add('point')
            setTimeout(() => {
                messageElement.classList.remove('point')
            }, 1000)
        }
    }

    return (
        <div className={`main-panel ${!!props.related.length ? 'related' : ''}`} ref={mainPanelRef}>
            {props.groups.map((group) => {
                return <div key={group.date} className="messages-group">
                    <div className="date-panel">{group.date}</div>
                    {group.messages.map((item) => {
                    const isSelf = item.senderId === props.userId
                    return <div
                        key={item._id}
                        className={`message-wrapper ${isSelf ? 'self' : 'other'}`}
                        id={`message-${item._id}`}
                    >
                        {isSelf && <div className="reply-button" onClick={() => props.setRelated(item._id)}><img src={replyWhiteIcon} /></div> }
                        <div className={`message ${isSelf ? 'self' : 'other'}`} key={item._id} >
                            {!!item.relatedMessage?.length && <div className="reply" onClick={() => scrollToMessage(item.relatedMessage)}>
                                <div className="reply-sender">{props.items?.find((it) => it._id === item.relatedMessage)?.senderId === props.userId ? 'You' : props.chaterUsername}</div>
                                <div className="reply-text">{props.items?.find((it) => it._id === item.relatedMessage)?.text || 'File'}</div>
                            </div>}
                            {!!item.files.length && <div className="files">
                                {item.files.map((file) => {
                                    return <FileDisplayer
                                        key={file._id}
                                        file={file}
                                        fileModel
                                        download
                                        fullscreen
                                        width={'auto'}
                                        bgc={isSelf ? 'transparent' : null}
                                        fullscreenImageFunc={props.setFullscreenImage}
                                        fullscreenVideoFunc={props.setFullscreenVideo}
                                    />
                                })}
                            </div>}
                            <div className="text-content">
                                <div className="text">{item.text}</div>
                                <div className='message-meta'>
                                    <div className="timestamp">{formatTimestamp(item.timestamp) || ''}</div>
                                    {isSelf && <div className="read-mark">
                                        {!item.isRead && <img src={unreadIcon} />}
                                        {!!item.isRead && <img src={readIcon} />}
                                    </div>}
                                </div>
                                
                            </div>
                        </div>
                        {!isSelf && <div className="reply-button" onClick={() => props.setRelated(item._id)}><img src={replyWhiteIcon} /></div> }
                    </div>
                })}
                </div>
            })}
            <div
                className={`scroller-down ${isScrollDown ? 'active' : ''}`}
                onClick={scrollerFunc}
                >
                    <img src={arrowIcon}/>
            </div>
        </div>
    )
}

export default MainPanel