import {$host, $authHost} from 'api'

export const createChat = async (userId) => {
    const {data} = await $authHost.post('shared/chat', {userId})
    return data
}

export const sendMessage = async (text, files, chatId, related) => {
    const {data} = await $authHost.post('shared/chat/message', {text, files, chatId, related})
    return data
}

export const getChat = async (chatId) => {
    const {data} = await $authHost.get('shared/chat/messages', {params: {chatId}})
    return data
}

export const getChats = async () => {
    const {data} = await $authHost.get('shared/chat')
    return data
}

export const getSupportChat = async (chatId) => {
    const {data} = await $authHost.get('shared/chat/support/messages', {params: {chatId}})
    return data
}

export const getSupportChats = async () => {
    const {data} = await $authHost.get('shared/chat/support')
    return data
}

export const setRead = async (chatId) => {
    const {data} = await $authHost.put('shared/chat/messages', {chatId})
    return data
}

export const getChatByUser = async (userId) => {
    const {data} = await $authHost.get('shared/chat/byuser', {params: {userId}})
    return data
}