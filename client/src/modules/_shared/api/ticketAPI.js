import {$authHost} from 'api'

export const getTickets = async () => {
    const {data} = await $authHost.get('shared/ticket')
    return data
}

export const removeTickets = async (ticketsIds) => {
    const {data} = await $authHost.put('shared/ticket', {ticketsIds})
    return data
}