import {$host, $authHost} from 'api'

export const uploadFile = async (file, path) => {
    const formData = new FormData()
    formData.append('file', file)
    const {data} = await $authHost.post('shared/file', formData, {params: {path}})
    return data
}

export const createFolder = async (path) => {
    const {data} = await $authHost.post('shared/file/folder', {path})
    return data
}

export const getFilesMetadata = async (ids) => {
    const {data} = await $host.get('shared/file', {params: {ids}})
    return data
}

export const downloadFile = async (fileId) => {
    const {data} = await $host.get('shared/file/download', {params: {fileId}})
    return data
}