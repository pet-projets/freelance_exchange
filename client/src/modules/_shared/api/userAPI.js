import {$host, $authHost} from 'api'
import {jwtDecode} from 'jwt-decode'

export const registration = async (username, password, email, code, role) => {
    const {data} = await $host.post('shared/user/registration', {username, password, email, code, role})
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)
}

export const login = async (username, password, email, code) => {
    const {data} = await $host.post('shared/user/login', {username, password, email, code})
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)
}

export const check = async () => {
    const {data} = await $authHost.get('shared/user/auth')
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)
}

export const sendEmail = async (email) => {
    const {data} = await $host.put('shared/user/email', {email})
    return data
}

export const editPassword = async (password) => {
    const {data} = await $authHost.put('shared/user/password', {password})
    return data
}

export const connectWallet = async (wallet) => {
    const {data} = await $authHost.put('shared/user/wallet', {wallet})
    return data
}

export const createDescription = async (description) => {
    const {data} = await $authHost.post('shared/user/description', {description})
    return data
}

export const setNotification = async (isNotificate) => {
    const {data} = await $authHost.put('shared/user/notification', {isNotificate})
    localStorage.setItem('token', data.token)
    return isNotificate
}

export const setAvatar = async (avatarId) => {
    const {data} = await $authHost.post('shared/user/avatar', {avatarId})
    return data
}

export const removeAvatar = async () => {
    const {data} = await $authHost.delete('shared/user/avatar')
    return data
}

export const getInfo = async (userId) => {
    const {data} = await $host.get('shared/user/', {params: {userId}})
    return data
}

export const searchUser = async (name) => {
    const {data} = await $host.get('shared/user/search', {params: {name}})
    const userId = jwtDecode(localStorage.getItem('token'))?.id
    return data.users.filter((users) => users.id !== userId)
}
