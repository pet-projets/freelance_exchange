import {FREELANCE_ROUTE, ADMIN_ROUTE} from './routesPath'
import adminIcon from 'assets/icons/white/star.svg'
import freelanceIcon from 'assets/menu/freelance.svg'
import logoutIcon from 'assets/icons/white/logout.svg'

export const shared = {
    authMenuItems: [
        {name: 'Freelance', icon: freelanceIcon, route: FREELANCE_ROUTE, url: '/freelance'},
        {name: 'Logout', icon: logoutIcon, logout: true}
    ],
    unAuthMenuItems: [
        {name: 'Freelance', icon: freelanceIcon, route: FREELANCE_ROUTE, url: '/freelance'},
    ],
    adminMenuItems: [
        {name: 'Freelance', icon: freelanceIcon, route: FREELANCE_ROUTE, url: '/freelance'},
        {name: 'Admin', icon: adminIcon, route: ADMIN_ROUTE, url: '/admin'},
        {name: 'Logout', icon: logoutIcon, logout: true}
    ]
}
