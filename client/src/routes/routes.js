import { ADMIN_ROUTE, CHATS_ROUTE, LOGIN_ROUTE, FREELANCE_ROUTE, OFFERS_ROUTE, REGISTRATION_ROUTE } from "./routesPath";
import Marketplace from '../modules/freelance/pages/marketplace/Marketplace'
import Admin from 'modules/_shared/pages/admin/Admin'
import Chats from 'modules/_shared/pages/chats/Chats'
import Login from 'modules/_shared/pages/login/Login'
import Registration from 'modules/_shared/pages/registration/Registration'
import Offers from "modules/freelance/pages/offers/Offers";


export const authRoutes = [
    { path: CHATS_ROUTE, component: <Chats /> },
    { path: OFFERS_ROUTE, component: <Offers /> },
    { path: FREELANCE_ROUTE, component: <Marketplace /> },
]

export const unAuthRoutes = [
    { path: FREELANCE_ROUTE, component: <Marketplace /> },
    { path: LOGIN_ROUTE, component: <Login /> },
    { path: REGISTRATION_ROUTE, component: <Registration /> }
]

export const adminRoutes = [
    { path: ADMIN_ROUTE, component: <Admin /> }
]