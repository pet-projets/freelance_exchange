import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect } from 'react'
import {Routes, Route, Navigate} from 'react-router-dom'
import { Context } from 'index'
import { adminRoutes, authRoutes, unAuthRoutes } from './routes'
import { BASE_ROUTE, ROOT_ROUTE } from './routesPath'

const AppRouter = observer(() => {
    const {user} = useContext(Context)

    return (
        <Routes>
            {user.isAuth &&
                authRoutes.map(({path, component}) => {
                    return <Route key={path} path={path} element={component} />
            })}
            {!user.isAuth &&
                unAuthRoutes.map(({path, component}) => {
                    return <Route key={path} path={path} element={component} />
            })}
            {user.isAdmin &&
                adminRoutes.map(({path, component}) => {
                    return <Route key={path} path={path} element={component} />
            })}
            
            <Route path={'*'} element={<Navigate replace to={BASE_ROUTE} />} />
        </Routes>
    )
})

export default AppRouter