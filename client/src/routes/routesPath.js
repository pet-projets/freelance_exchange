export const LOGIN_ROUTE = '/login'
export const REGISTRATION_ROUTE = '/registration'
export const ADMIN_ROUTE = '/admin'
export const CHATS_ROUTE = '/chats'

export const FREELANCE_ROUTE = '/freelance'
export const OFFERS_ROUTE = FREELANCE_ROUTE + '/offers'

export const ROOT_ROUTE = '/'
export const BASE_ROUTE = FREELANCE_ROUTE

// export const ABOUT_ROUTE = '/about'


