import {useState, useEffect} from 'react'
import { useAccount, useBalance, useConnect, useContractWrite, useDisconnect, useWaitForTransaction } from 'wagmi'
import { getConnector } from 'utils/walletUtils/connectors'
import { parseEther } from 'ethers'
import { walletConfig } from 'utils/walletUtils/walletConfig'


const useWallet = () => {
    const { isConnected, address } = useAccount()
    const balance = useBalance({ address })
    const { connect } = useConnect()
    const { disconnect } = useDisconnect()
    const [connectCurrName, setConnectCurrName] = useState('')

    const [transactionHash, setTransactionHash] = useState('')
    const [transactionStatus, setTransactionStatus] = useState(null)

    const { writeAsync: writeContractPutOnAccount } = useContractWrite({
        address: walletConfig['development'].address,
        abi: walletConfig['development'].abi,
        functionName: 'putOnAccount',
        chainId: walletConfig['development'].chain
    })

    useWaitForTransaction({
        hash: transactionHash,
        onSuccess() {
            if (!transactionHash.length) return
            setTransactionStatus('success')
        },
        onError() { 
            if (!transactionHash.length) return
            setTransactionStatus('failed')
         }
    })

    useEffect(() => {
        const connectorName = localStorage.getItem('me-wallet-name') || ''
        if (connectorName.length < 1 || isConnected) return
        connectWallet(connectorName)
    }, [])

    const connectWallet = (name) => {
        const connectorInfo = getConnector(name)
        if (!connectorInfo.error) {
            connect({ connector: connectorInfo.connector })
            localStorage.setItem('me-wallet-name', name)
        } else {
            setConnectCurrName(connectorInfo.currName)
            localStorage.removeItem('me-wallet-name')
        }
    }

    const disconnectWallet = () => {
        localStorage.removeItem('me-wallet-name')
        setConnectCurrName('')
        try { disconnect() } catch{}
    }

    const clearTransactionStatus = () => {
        setTransactionStatus(null)
    }

    const sendTo = async (price) => {
        const {hash} = await writeContractPutOnAccount({
            value: parseEther(`${price}`)
        })
        setTransactionHash(hash)
    }

    const sendFrom = () => {

    }

    return {
        isConnected,
        connectCurrName,
        balance,
        address,
        transactionStatus,
        connectWallet,
        disconnectWallet,
        clearTransactionStatus,
        sendTo,
    }
}

export default useWallet