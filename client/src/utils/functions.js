
export function generateNumber(digitCount) {
    let number = []
    for (let i=0; i < digitCount; i++) {
        number.push(parseInt(Math.floor(Math.random()*10)))
    }
    return number.join('')
}

export function formatSize(size) {
    let currSize = size
    const sizeNames = ['bytes', 'Kb', 'Mb']
    let sizeIndex = 0
    while (currSize >= 1024) {
        currSize /= 1024
        sizeIndex++
    }
    return `${currSize.toFixed(2)} ${sizeNames[sizeIndex]}`
}

export function deadlineConvert(deadline, startAt) {
    const currTime = Date.now()
    const timeLeft = (deadline*1000*60*60*24) - (currTime - startAt)
    if (timeLeft <= 0) return 'Deadline has passed'
    const daysLeft = timeLeft / 1000 / 60 / 60 / 24
    return `${Math.floor(daysLeft)}d ${Math.round((daysLeft - Math.floor(daysLeft)) * 24).toString().padStart(2, '0')}h left`
}

export function formatDate(timestamp) {
    const options = { month: 'long', day: 'numeric' };
    const date = new Date(timestamp);
    return date.toLocaleDateString('en-US', options);
}

export function formatMessages(messages) {
    const groupedMessages = messages.reduce((result, message) => {
        const dateKey = formatDate(message.timestamp);
        if (!result[dateKey]) {
          result[dateKey] = { date: dateKey, messages: [] };
        }
        result[dateKey].messages.push(message);
        return result;
    }, {})
    return Object.values(groupedMessages);
}

export function formatTimestamp(timestamp) {
    if (!timestamp) return null
    const date = new Date(timestamp);
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    
    return `${hours}:${minutes}`;
}

