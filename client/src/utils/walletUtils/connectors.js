import { InjectedConnector } from 'wagmi/connectors/injected'
import { wallets } from './wallets'

const connectorsPath = {
    phantom: window.phantom?.ethereum,
    metamask: window.web3?.currentProvider,
    coinbase: window.coinbaseWalletExtension,
    trust: window.trustwallet
}

export const getConnector = (name) => {
    const connector = new InjectedConnector({
        options: {
            name: (detectedName) => detectedName,
            getProvider: () => connectorsPath[name]
        }
    })
    const error = connector.name !== wallets[name].name
    return {
        error,
        currName: connector.name,
        connector: connector
    }
}