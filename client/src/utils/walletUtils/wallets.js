const metamaskIcon = 'https://img-cdn.magiceden.dev/rs:fill:400:400:0:0/plain/https://bafkreies74clpwa2jslad2ehkxqk6hn7z2tsoxhpg3g6lxtrx2ycmthhn4.ipfs.nftstorage.link/'
const phantomIcon = 'https://img-cdn.magiceden.dev/rs:fill:400:400:0:0/plain/https://bafkreib563klpv6s2ehbbxscogab2jm2nyzlctxacmccxt4ku5mebagi7y.ipfs.nftstorage.link/'
const coinbaseIcon = 'https://img-cdn.magiceden.dev/rs:fill:400:400:0:0/plain/https://bafkreianpwjvletuksfg6gvptwaf5qp6diejubzgf3izq2uaks42qtsrii.ipfs.nftstorage.link/'
const trustIcon = 'https://altcoinsbox.com/wp-content/uploads/2023/03/trust-wallet-logo-300x300.webp'

export const wallets = {
    metamask: {
        name: 'MetaMask', icon: metamaskIcon,
    },
    phantom: {
        name: 'Phantom', icon: phantomIcon,
    },
    coinbase: {
        name: 'Coinbase Wallet', icon: coinbaseIcon,
    },
    trust: {
        name: 'Trust Wallet', icon: trustIcon,
    },
}