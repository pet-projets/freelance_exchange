import abiDev from './abi-dev.json'

export const walletConfig = {
    development: {
        chain: 5,
        abi: abiDev,
        address: '0xc9a0De8C2abB079e847Ee50a7D4BF584dE91948A'
    },
    production: {
        chain: 5,
        abi: abiDev,
        address: '0xc9a0De8C2abB079e847Ee50a7D4BF584dE91948A'
    }
}