import React, { useEffect, useState } from 'react'
import './password.scss'
import eyeActiveIcon from '../../assets/icons/white/eye-active.svg'
import eyePassiveIcon from '../../assets/icons/white/eye-passive.svg'

const Password = (props) => {
    const [value, setValue] = useState(props.initValue || '')
    const [isHide, setIsHide] = useState(true)

    useEffect(() => {
        if (!props.valueFunc) return
        props.valueFunc(value)
    }, [value])

    return (
        <>
        <div className="password-wrapper">
            <input
                className='password'
                type={isHide ? 'password' : 'text'}
                placeholder={props.placeholder}
                value={value}
                onChange={(e) => setValue(e.target.value)}
                autoFocus={props.autoFocus}
            />
            <div className="show-hide-password" onClick={() => setIsHide(!isHide)}>
                <img src={isHide ? eyePassiveIcon : eyeActiveIcon} />
            </div>
        </div>
        
        </>
        
    )
}

export default Password