import React from 'react'
import './button.scss'

const Button = (props) => {

    const clickFunc = () => {
        props.onClick()
    }

    return (
        <button className={`button ${props.className}`} style={props.style} onClick={clickFunc}>
            {props.img && <img src={props.img} />}
            {props.label && <span>{props.label}</span>}
        </button>
    )
}

export default Button