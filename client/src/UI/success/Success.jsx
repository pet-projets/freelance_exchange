import React from 'react'
import './success.scss'

const Success = (props) => {

    return (
        <div className='success'>
            <div className="success-circle">
                <div className="success-stick success-stick-1"></div>
                <div className="success-stick success-stick-2"></div>
            </div>
            <div className="success-label">{props.label}</div>
        </div>
    )
}

export default Success