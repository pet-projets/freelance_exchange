import React, { useState } from 'react'
import './stars.scss'
import starFilledIcon from '../../assets/icons/yellow/star.svg'
import starOutlineIcon from '../../assets/icons/yellow/star-outline.svg'

const StarsDisplayer = (props) => {

    return (
        <div className='stars-displayer'>
            {Array.from({ length: 5 }, (_, index) => index + 1).map((value) => {
                return <div
                    key={value}
                    className="star small"
                >
                    <img src={props.value >= value ? starFilledIcon : starOutlineIcon} />
                </div>
            })}
        </div>
    )
}

export default StarsDisplayer