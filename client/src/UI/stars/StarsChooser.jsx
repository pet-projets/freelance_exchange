import React, { useState } from 'react'
import './stars.scss'
import starFilledIcon from '../../assets/icons/yellow/star.svg'
import starOutlineIcon from '../../assets/icons/yellow/star-outline.svg'

const StarsChooser = (props) => {
    const [starHover, setStarHover] = useState(0)
    
    const chooseFunc = (value) => {
        if (value <= 0) return
        props.valueFunc(value)
    }

    return (
        <div className='stars-chooser'>
            <div className="star-chooser-wrapper" onMouseLeave={() => setStarHover(0)}>
                {Array.from({ length: 5 }, (_, index) => index + 1).map((value) => {
                    const currValue = starHover ? starHover : props.value
                    return <div
                        key={value}
                        className="star"
                        onMouseOver={() => setStarHover(value)}
                        onClick={() => chooseFunc(value)}
                    >
                        <img src={currValue >= value ? starFilledIcon : starOutlineIcon} />
                    </div>
                })}
            </div>
        </div>
    )
}

export default StarsChooser