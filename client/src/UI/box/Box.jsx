import React from 'react'
import './box.scss'

const Box = (props) => {


    return (
        <>
        <div className="box-title">{props.title}</div>
        <div className='box' style={{width: props.width}}>
            <div className="box-content">
                {props.children}
            </div>
        </div>
        </>
        
    )
}

export default Box