import React from 'react'
import './fullscreen.scss'

const Fullscreen = (props) => {

    const closeFunc = (e) => {
        if (e.target.classList.contains('video')) return
        props.closeFunc()
    }

    return (
        <div className='fullscreen' onClick={closeFunc}>
            {props.image && <img className='image' src={props.image} />}
            {props.video && <video className='video' src={props.video} controls autoPlay></video> }
        </div>
    )
}

export default Fullscreen