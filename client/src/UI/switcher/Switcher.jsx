import React, { useState } from 'react'
import './switcher.scss'

const Switcher = (props) => {
    const [active, setActive] = useState(0)

    const setActiveFunc = (index) => {
        setActive(index)
        if (!props.func) return
        props.func(index)
    }

    return (
        <div className='switcher'>
            <div className="switcher-list">
                {props.items?.map((item, index) => {
                    return <div
                        key={index}
                        className={`switcher-item ${active === index ? 'active' : ''}`}
                        onClick={() => setActiveFunc(index)}
                    >
                        {item.icon && <img src={item.icon} />}
                        {item.name && <span>{item.name}</span>}
                    </div>
                })}
            </div>
        </div>
    )
}

export default Switcher