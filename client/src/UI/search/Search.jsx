import React, { useContext, useEffect, useState } from 'react'
import searchIcon from 'assets/icons/d5/search.svg'
import './search.scss'

const Search = (props) => {
    const [value, setValue] = useState('')

    useEffect(() => {
        const timerId = setTimeout(() => {
            props.searchFunc(value)
        }, (props.delay || 0) * 1000)
        return () => clearTimeout(timerId)
      }, [value])

    return (
        <div className="search-wrapper" style={{width: props.width || '100%'}}>
            <div className="search">
                <input
                    type="text"
                    placeholder={props.placeholder}
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                    onFocus={() => {
                        if (!props.autoclose) return
                        props.setActive(true)
                    }}
                    onBlur={() => {
                        if (!props.autoclose) return
                        if (props.isMouseOver) {
                            setTimeout(() => {
                                props.clearItemActive()
                                props.setActive(false)
                            }, 250)
                        } else {
                            props.clearItemActive()
                            props.setActive(false)
                        }
                    }}
                />
                <img src={searchIcon} />
            </div>
            {props.children}
        </div>
    )
}

export default Search