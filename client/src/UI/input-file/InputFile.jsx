import React from 'react'
import Button from '../button/Button'
import './input-file.scss'

const MAX_BYTES = 100 * 1024 * 1024

const InputFile = (props) => {

    const fileChange = (e) => {
        if (!props.changeFunc) return
        const files = e.target.files
        if (props.accept && Array.from(files).some((file) => !file.type.includes(props.accept.replace('.', '').replace('/*', '')))) {
            console.log('incorect type');
            return
        }
        if (Array.from(files).some((file) => file.size >= MAX_BYTES)) {
            console.log('file is too large');
            return
        }
        props.changeFunc(props.multiple ? Array.from(files).slice(0, props.max || 10) : files[0])
    }

    return (
        <div className='input-file'
            style={{padding: `0 ${props.padding || 'auto'}px`, height: props.height || 0}}
        >
            <Button
                className={props.buttonClassName}
                img={props.img}
                label={props.label}
                onClick={()=>{}}
            />
            <input
                type="file"
                multiple={props.multiple}
                onChange={fileChange}
                accept={props.accept || '*/*'}
            />
        </div>
    )
}

export default InputFile