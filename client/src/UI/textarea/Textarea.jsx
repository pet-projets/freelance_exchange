import React, { useEffect, useState } from 'react'
import './textarea.scss'

export const Textarea = (props) => {
    const [value, setValue] = useState(props.initValue || '')
    const [pressedKeys, setPressedKeys] = useState([])

    const onChangeFunc = (e) => {
        if (props.shiftEnter &&
            pressedKeys.includes('Enter') && !pressedKeys.includes('Shift')) {
            props.enterFunc()
            return
        }
        setValue(e.target.value)
        if (!props.valueFunc) return
        props.valueFunc(e.target.value)
    }

    const onKeyDownFunc = (e) => {
        if (!props.shiftEnter) return
        if (!pressedKeys.includes(e.key)) {
            setPressedKeys((prevKeys) => [...prevKeys, e.key]);
        }
    }

    const onKeyUpFunc = (e) => {
        if (!props.shiftEnter) return
        setPressedKeys((prevKeys) => prevKeys.filter((pressedKey) => pressedKey !== e.key))
    }
    
    useEffect(() => {
        setValue(props.value)
    }, [props.value])

    return (
        <textarea
            className='textarea'
            style={{fontSize: props.fontSize || 16}}
            placeholder={props.placeholder || 'Your text ...'}
            autoFocus={props.autoFocus}
            rows={props.rows || 10}
            readOnly={props.readOnly}
            value={value}
            onChange={onChangeFunc}
            onKeyDown={onKeyDownFunc}
            onKeyUp={onKeyUpFunc}
        />
    )
}
