import React, { useEffect, useState } from 'react'
import './input.scss'

const Input = (props) => {
    const [value, setValue] = useState(props.initValue || '')

    useEffect(() => {
        if (!props.valueFunc) return
        props.valueFunc(value)
    }, [value])

    const handleKeyDown = (event) => {
        if (!props.enterFunc) return
        if (event.key === 'Enter') {
            props.enterFunc()
        }
    }

    return (
        <input
            className='input'
            type={'text'}
            placeholder={props.placeholder}
            value={props.value !== undefined ? props.value : value}
            onChange={(e) => setValue(e.target.value)}
            onKeyDown={handleKeyDown}
            autoFocus={props.autoFocus}
            readOnly={props.readOnly}
        />
    )
}

export default Input