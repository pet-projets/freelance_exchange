import React from 'react'
import {Bars, Circles} from 'react-loader-spinner'
import './loader.scss'

const Loader = (props) => {

    return (
        <div className="loader-wrapper">
            <Circles
                height="80"
                width="80"
                color={props.color || "#7D4884"}
                ariaLabel="circles-loading"
                wrapperStyle={{
                    transform: `scale(${props.scale || 1})`
                }}
                wrapperClass=""
                visible={true}
            />
        </div>
        
    )
}

export default Loader