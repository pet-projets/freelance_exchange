import React from 'react'
import './empty-sign.scss'
import stackIcon from '../../assets/icons/d5/stack.svg'

const EmptySign = (props) => {

    return (
        <div className='empty-sign' style={{backgroundColor: props.bgc || 'transparent'}}>
            <div className="empty-sign-content">
                {/* <img src={stackIcon} /> */}
                <span>Empty</span>
            </div>
        </div>
    )
}

export default EmptySign