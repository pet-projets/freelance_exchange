import React, { useContext } from 'react'
import userIcon from 'assets/icons/white/user.svg'
import FileDisplayer from 'modules/_shared/containers/file-displayer/FileDisplayer'
import './avatar.scss'
import { Context } from 'index'
import { observer } from 'mobx-react-lite'

const Avatar = observer((props) => {
    const {user} = useContext(Context)

    return (
        <div className="avatar-wrapper">
            <div className='avatar'
                style={{width: props.size, height: props.size}}>
                {props.avatar && <FileDisplayer
                    previewOnly
                    fileModel
                    image
                    file={props.avatar}
                    width={props.size} height={props.size}
                />}
                {!props.avatar && <div className="avatar-default">
                    <img src={userIcon} />
                </div> }
            </div>
            {props.online && user.isOnline(props.id) && <div className="online-indicator"></div> }
        </div>
        
    )
})

export default Avatar