import React, { useEffect, useState } from 'react'
import './modal.scss'
import closeIcon from '../../assets/icons/white/add.svg'
import Loader from '../loader/Loader'

const Modal = (props) => {
    const [active, setActive] = useState(false)

    useEffect(() => {
        addActive()
    }, [])

    const closeFunc = (e) => {
        if (!e.target.classList.contains('modal')) return
        removeActive()
    }

    const addActive = () => {
            setActive(true)
    }
    const removeActive = () => {
        if (props.blockClose) return
        setActive(false)
        setTimeout(() => {
            props.closeFunc()
        }, 100)
    }


    return (
        <div className={`modal ${props.className} ${active ? 'active' : ''}`} onClick={closeFunc}>
            <div className={`modal-panel ${active ? 'active' : ''}`} style={{width: props.width}}>
                <div className={`modal-title ${!props.title?.length ? 'no-border' : ''}`}>
                    <span>{props.title}</span>
                    {!props.blockClose && <div className="close" onClick={removeActive}>
                        <img src={closeIcon} style={{transform: `rotate(45deg)`}} />
                    </div>}
                </div>
                {!props.loading && <div className="modal-content">
                    {props.children}
                    {/* {loading && <Loader scale={0.75} />} */}
                </div>}
                {props.loading && <div className="modal-loader">
                    <Loader scale={0.75} />
                </div>}
            </div>
        </div>
    )
}

export default Modal