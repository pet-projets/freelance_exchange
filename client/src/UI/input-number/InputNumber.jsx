import React from 'react'
import './input-number.scss'

const InputNumber = (props) => {


    return (
        <div className="input-number-wrapper">
            <input
                className='input-number'
                type="number"
                value={props.value.toString()}
                onChange={(e) => props.valueFunc(e.target.value)}
            />
            {props.sign && <div className="input-number-sign">{props.sign}</div>}
        </div>
    )
}

export default InputNumber