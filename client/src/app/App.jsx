import { observer } from 'mobx-react-lite'
import React, { useContext, useEffect, useState } from 'react'
import { BrowserRouter, useLocation } from 'react-router-dom'
import { Context } from '..'
import {check} from 'modules/_shared/api/userAPI'
import Navbar from 'modules/_shared/containers/navbar/Navbar'
import AppRouter from 'routes/AppRouter'
import './App.scss'
import { WagmiConfig, createConfig, configureChains } from 'wagmi'
import { infuraProvider } from 'wagmi/providers/infura'
import { mainnet, goerli } from 'wagmi/chains';
import Loader from 'UI/loader/Loader'
import SocketModule from 'modules/_shared/containers/socket-module/SocketModule'
import FetchModule from 'modules/_shared/containers/fetch-module/FetchModule'
import Footer from 'modules/_shared/containers/footer/Footer'
import Background from 'UI/background/Background'
import { getChats, getSupportChats } from 'modules/_shared/api/chatAPI'

const { publicClient } = configureChains(
  [mainnet, goerli],
  [infuraProvider({ apiKey: process.env.REACT_APP_PROVIDER_API })],
)

const wagmiConfig = createConfig({
  autoConnect: true,
  publicClient,
})

const App = observer(() => {
  const {user, chats} = useContext(Context)
  const [loading, setLoading] = useState(true)
  const [moduleLoading, setModuleLoading] = useState(false)

  useEffect(() => {
    check()
    .then((data) => {
      user.setUser(data)
      user.setIsAuth(true)
      if (data.status === 'admin') {
        getSupportChats().then((dataCh) => {
          chats.setItems(dataCh.chats)
        })
      } else {
        getChats().then((dataCh) => {
          chats.setItems(dataCh.chats)
        })
      }
    })
    .catch(() => {
      console.error('User has not authorized')
    })
    .finally(() => {
      setLoading(false)
    })
  }, [])

  if (loading) {
    return <Loader scale={1.5} />
  }

  return (
    <WagmiConfig config={wagmiConfig}>
      <BrowserRouter>
        <Navbar />
        {moduleLoading && <Loader scale={1.25} />}
        {!moduleLoading && <AppRouter />}
        <Background />
        <Footer />
        <FetchModule loadingFunc={setModuleLoading} />
        <SocketModule />
      </BrowserRouter>
    </WagmiConfig>
    
  )
})

export default App
