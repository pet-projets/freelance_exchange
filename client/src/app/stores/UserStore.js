import {makeAutoObservable} from 'mobx'

export default class UserStore {
    constructor() {
        this._isAuth = false
        this._info = {}
        this._onlines = []
        this._fetchType = ''
        makeAutoObservable(this)
    }

    setIsAuth(bool) {
        this._isAuth = bool
    }

    setUser(info) {
        this._info = info
    }

    setRole(role) {
        this._info.role = role
    }

    setFetchType(type) {
        this._fetchType = type
    }

    setOnlines(ids) {
        this._onlines = ids
    }
    addOnline(id) {
        this._onlines = this._onlines.filter((o) => o !== id)
        this._onlines = [...this._onlines, id]
    }
    removeOnline(id) {
        this._onlines = this._onlines.filter((o) => o !== id)
    }
    isOnline(id) {
        return this._onlines.some((o) => o === id)
    }
    get onlines() {
        return this._onlines
    }


    get isAuth() {
        return this._isAuth
    }

    get isAdmin() {
        return this._info.status === 'admin'
    }

    get info() {
        return this._info
    }

    get role() {
        return this._info.role
    }

    get fetchType() {
        return this._fetchType
    }
}