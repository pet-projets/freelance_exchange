import {makeAutoObservable} from 'mobx'
class PostStore {
 
  constructor() {
    this._posts = [];
    makeAutoObservable(this);
  }

  addPost(post) {
    this._posts=[...this._posts, post];
  }
  setPosts(posts) {
    this._posts = posts;
  }
  removePost(post) {
    this._posts = this._posts.filter((p) => p._id !== post.id);
  }
  updatePost(post, updatedPost) {
    this._posts = this._posts.map((p) => (p._id === updatedPost.id ? updatedPost : p));
  }
  get posts() {
    return this._posts;
  }
  getPostById(id) {
    return this._posts.find((p) => p._id === id);
  }
  searhPostByTitle(title) {
    return this._posts.filter((p) => p.title.includes(title));
  }
 
}

const postStore = new PostStore();
export default PostStore;
