import {makeAutoObservable} from 'mobx'

export default class CategoryStore {
    constructor() {
        this._items = []
        makeAutoObservable(this)
    }

    setItems(items) {
        this._items = items
    }

    get items() {
        return this._items
    }

    getTopItemsByCode(code, isOther=true) {
        const pathItems = code.split('-')
        const longCodes = pathItems.map((pa, i) => {
            return pathItems.slice(0, i+1).join('-')
        })
        const items = this._items.filter((item) => {
            return longCodes.includes(item.longCode)
        })
        .filter((item) => isOther || item.shortCode !== '0')
        return items
    }

    getBottomItemsByCode(code, isOther=false) {
        const lengthCode = code.split('-').length + 1
        return this._items
            .filter((item) => {
                return item.longCode.startsWith(code) && item.longCode.split('-').length === lengthCode
            })
            .filter((item) => isOther || item.shortCode !== '0')
            .sort((a, b) => b.shortCode - a.shortCode)
    }

    getSubcategories(code) {
        return this._items.filter((item) => {
            return item.longCode.startsWith(code)
        })
    }

    getItemByCode(code) {
        return this._items.find(item => item.longCode === code)
    }

    getItemById(id) {
        return this._items.find(item => item._id === id)
    }

}