import {makeAutoObservable} from 'mobx'

export default class ChatStore {
    constructor() {
        this._chats = []
        makeAutoObservable(this)
    }

    get items() {
        return this._chats
    }

    getChat(chatId) {
        return this._chats.find((chat) => chat._id === chatId)
    }

    setItems(items) {
        this._chats = items
    }

    getItems(status) {
        if (status === 'admin') {
            return this._chats;
        } else {
            return this._chats.filter((ch) => !ch.isSupport)
        }
    }

    addItem(item) {
        this._chats = [...this._chats, item]
    }

    setLatestMessage(chatId, latestMessage) {
        this._chats = this._chats
        .map((ch) => {
            return (ch._id !== chatId || ch.latestMessage?._id === latestMessage._id) ? ch : Object.assign({}, ch, {unreadCount: ch.unreadCount + 1})
        })
        .map((ch) => {
            return ch._id !== chatId ? ch : Object.assign({}, ch, {latestMessage})
        })
    }

    clearUnreadCount(chatId, userId) {
        this._chats = this._chats.map((ch) => {
            return (ch._id !== chatId || ch.latestMessage?.senderId === userId) ? ch : Object.assign({}, ch, {unreadCount: 0})
        })
    }

    getSupportItem() {
        return this._chats.find((item) => item.isSupport)
    }

    // getItemByChaterId(userId, chaterId, role) {
    //     const filterFieldUser = role === 'employer' ? 'employerId' : 'employeeId';
    //     const filterFieldChater = role === 'employer' ? 'employeeId' : 'employerId';
    //     return this._chats.find((item) => item[filterFieldUser] === userId && item[filterFieldChater] === chaterId)
    // }

    getUnreadCount(userId) {
        const chats = this._chats.filter((item) => item.latestMessage?.senderId !== userId && !item.isSupport)
        return chats.reduce((a, item) => a + item.unreadCount, 0)
    }

    getUnreadSupport(userId) {
        const chats = this._chats.filter((item) => item.users[0] === userId && item.isSupport && item.latestMessage?.senderId !== userId)
        return chats.reduce((a, item) => a + item.unreadCount, 0)
    }

}