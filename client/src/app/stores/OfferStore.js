import {makeAutoObservable} from 'mobx'

export default class OfferStore {
    constructor() {
        this._items = []
        this._candidates = []
        makeAutoObservable(this)
    }

    setItems(items) {
        this._items = items
    }

    addItem(item) {
        this._items = [...this._items, item]
    }

    updateItem(newItem) {
        this._items = this._items.map((item) => item._id === newItem._id ? newItem : item)
    }

    deleteItem(id) {
        this._items = this._items.filter((item) => item._id !== id)
    }

    setCandidatesItems(items) {
        this._candidates = items
    }

    get items() {
        return this._items.filter((item) => item.startAt === 0)
    }

    get candidates() {
        return this._candidates
    }

    getItemsByCategoryIds(ids) {
        return this.items.filter((item) => ids.includes(item.categoryId))
    }

    getItemById(id) {
        return this._items.find((item) => item._id === id)
    }

    // setPreview(id, link, linkExp) {
    //     console.log(this._items.find((item) => item.preview._id === id), 's')
    //     const currItem = this._items.find((item) => item.preview._id === id)
    //     currItem.preview.link = link
    //     currItem.preview.linkExp = linkExp
    //     console.log(this._items.find((item) => item.preview._id === id), 'e')
    // }

    addCandidate(candidate) {
        this._candidates = [...this._candidates, candidate]
    }

    deleteCandidate(id) {
        this._candidates = this._candidates.filter((c) => c._id !== id)
    }

    getCandidateByOfferId(offerId) {
        return this._candidates.find((c) => c.offerId === offerId)
    }

    getRelatedItems(role, type, userId) {
        if (role === 'employer') {
            switch (type) {
                case 'develop':
                    return this._items.filter((item) => item.startAt !== 0 && item.employerId === userId && !item.rejected)
                case 'pending':
                    return this._items.filter((item) => item.startAt === 0 && item.employerId === userId)
                case 'paused':
                    return this._items.filter((item) => item.rejected && item.employerId === userId)
            }
        } else if (role === 'employee') {
            switch (type) {
                case 'develop':
                    return this._items.filter((item) => item.employeeId === userId && !item.rejected)
                case 'pending':
                    const candidates = this._candidates.filter((c) => c.userId === userId).map((c) => c.offerId)
                    return this._items.filter((item) => candidates.includes(item._id))
                case 'paused':
                    return this._items.filter((item) => item.employeeId === userId && item.rejected)
            }
        } else return []
    }

    getOfferFunctionalCode(offerId, role, userId) {
        if (!userId) return 6
        const offer = this._items.find((item) => item._id === offerId)
        if (!offer) return 0
        if (role === 'employer') {
            if (offer.employerId === userId) {
                return !!offer.startAt ? (offer.rejected ? 0 : 2) : 1
            } else return 0
        } else if (role === 'employee') {
            const isCandidate = !!this._candidates.find((item) => item.userId === userId && item.offerId === offerId)
            if (offer.employeeId === userId || isCandidate) {
                return !!offer.startAt ? 5 : 4
            } else {
                return 3
            }
        }
    }

    searchOffers(query) {
        const lowerQuery = query.toLowerCase()
        return this.items.filter((item) => {
            const isTitle = item.title.toLowerCase().includes(lowerQuery)
            const isDescription = item.description.toLowerCase().includes(lowerQuery)
            const isKeywords = item.keywords.some((keyword) => keyword.replace('#', '').replaceAll('_', ' ').includes(lowerQuery))
            return isTitle || isDescription || isKeywords
        }).sort((a, b) => b.price - a.price)
    }

    isRelated(userId, role) {
        if (role === 'employer') {
            return this._items.some((item) => item.employerId === userId)
        } else if (role === 'employee') {
            return this._items.some((item) => item.employeeId === userId) || this._candidates.some((candidate) => candidate.userId === userId)
        } else return false
        
    }

}