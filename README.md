# Freelance exchange
This program repeats the functionality of the freelance exchange, payment in the program is made using blockchain technologies. You can:
* Register and log in
* Choose the role of employee or employer
* Сreate/execute offers
* Communicate through messenger
* Pay/get paid via blockchain

### Technologies:
* React.js
* Docker
* MobX
* REST API
* Axios
* Wagmi
* Scss
* Socket.IO
* Node.js
* MongoDB
* Express

## Start the program:
* Add DropboxAPI and Nodemailer configurations to `docker-compose.yml` file, to the `environment` field.
* Execute the `docker-compose up -d` command in root directory.